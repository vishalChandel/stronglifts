package com.stronglifts.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class ActionStatusModel(
    var action_status_id:String,
    var ac_id:String,
    var ac_status_date:String
): Parcelable