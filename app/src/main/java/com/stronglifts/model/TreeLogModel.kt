package com.stronglifts.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.io.Serializable

@Parcelize
class TreeLogModel(
    var tl_id:String,
    var plant_id:String,
    var log_date: String,
    var log_desc:String
):Parcelable