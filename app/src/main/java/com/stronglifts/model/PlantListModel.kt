package com.stronglifts.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.io.Serializable

@Parcelize
class PlantListModel(
    var item_id :String?,
    var plant_id : String?,
    var plant_type : String?,
    var plant_alt_name :String,
    var plant_stage :String?,
    var plant_env :String?,
    var creation_date :String?
):Parcelable