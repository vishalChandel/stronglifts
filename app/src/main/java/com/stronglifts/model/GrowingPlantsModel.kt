package com.stronglifts.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.io.Serializable

@Parcelize
class GrowingPlantsModel(
    var plant_id:String?,
    var name: String?,
    var thumbnailImage: String?,
    var item_id:String?,
    var plant_alt_name:String?,
    var plant_stage:String?,
    var plant_env:String?,
    var creation_date:String?
):Parcelable