package com.stronglifts.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.io.Serializable

@Parcelize
class EnvLogModel(
    var ev_id:String,
    var env_name:String,
    var env_log_date: String,
    var env_log_desc:String
):Parcelable