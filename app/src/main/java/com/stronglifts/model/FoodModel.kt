package com.stronglifts.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.io.Serializable

@Parcelize
class FoodModel(
    var food_id:String,
    var nutrient:String,
    var amount: String,
    var solvent:String
):Parcelable