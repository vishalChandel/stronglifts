package com.stronglifts.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.io.Serializable

@Parcelize
class PlantsModel(
    var id:String,
    var name: String,
    var category:String,
    var description:String,
    var thumbnailImage: String,
    var seeddepth:String,
    var germinationsoiltemp:String,
    var daystogermination:String,
    var sowindoors:String,
    var sooutdoors:String,
    var phrange:String,
    var growingsoiltemp:String,
    var spacinginbeds:String,
    var watering:String,
    var light:String,
    var goodcomapnions:String,
    var badcomapnions:String,
    var sowigdescription:String,
    var growingdescription:String,
    var harvestingdescription:String,
    var item_id:String
):Parcelable