package com.stronglifts.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class ActionTypeModel(
    var actionImage:Int,
    var actionName:String
): Parcelable