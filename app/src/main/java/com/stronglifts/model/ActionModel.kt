package com.stronglifts.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class ActionModel(
    var ac_id:String,
    var ac_plant_id:String,
    var ac_type:String,
    var ac_date:String,
    var ac_desc:String,
    var ac_type_method:String,
    var ac_type_water_quantity:String,
    var ac_type_water_amount:String,
    var ac_type_nutrients_list:String,
    var ac_repeat:String,
    var ac_repeat_type:String,
    var ac_repeat_till:String,
    var ac_repeat_days:String,
    var ac_repeat_status:String,
): Parcelable