package com.goserve.utils

import com.stronglifts.R

// - - Base Server Url
const val BASE_URL = ""

// - - Link Constants
const val LINK_TYPE = "link_type"
const val LINK_ABOUT = "link_about"
const val LINK_PP = "link_pp"
const val LINK_TERMS = "link_terms"

// - - Link Urls
const val ABOUT_WEB_LINK = ""
const val PP_WEB_LINK = ""
const val TERMS_WEB_LINK = ""

// - - Constant Values
const val SPLASH_TIME_OUT = 6000
const val DEVICE_TYPE = "2"

//Static arrays:
val GENDER_TYPES = arrayOf("Male", "Female", "Other")


// - - Plant Images Array
 val PLANT_IMAGES= arrayOf(R.drawable.artichoke,R.drawable.sunflower,R.drawable.arugula,R.drawable.asparagus,R.drawable.avocado,R.drawable.sweetbasil,R.drawable.beet,R.drawable.blackberry,R.drawable.blueberry,R.drawable.brocalli,R.drawable.brusselssprouts,R.drawable.bushbeans,R.drawable.cabbage,R.drawable.carrots,R.drawable.cauliflower,R.drawable.celery,R.drawable.chard,R.drawable.chives,R.drawable.greencoriander,R.drawable.corn,R.drawable.cucumber,R.drawable.dill,R.drawable.eggplant,R.drawable.fennel,R.drawable.garlic,R.drawable.greenbeans,R.drawable.greenonions,R.drawable.pepper,R.drawable.jalapeno,R.drawable.kale,R.drawable.leeks,R.drawable.lemon,R.drawable.lemongrass,R.drawable.lettuce,R.drawable.lime,R.drawable.marijuana,R.drawable.marjoram,R.drawable.melons,R.drawable.mint,R.drawable.mustardgreen,R.drawable.okra,R.drawable.onions,R.drawable.orach,R.drawable.oregano,R.drawable.orange,R.drawable.parsley,R.drawable.parsnips,R.drawable.polebeans,R.drawable.potato,R.drawable.pumpkin,R.drawable.redcabbage,R.drawable.radish,R.drawable.raspberry,R.drawable.redonion,R.drawable.rosemary,R.drawable.sage,R.drawable.savory,R.drawable.siamqueen,R.drawable.spinach,R.drawable.strawberry,R.drawable.summersquash,R.drawable.sweetpepper,R.drawable.sweetpotato,R.drawable.thyme,R.drawable.tomato,R.drawable.turnip,R.drawable.winterpeas,R.drawable.wintersquash,R.drawable.zucchini)

// - - Shared Preference Keys
const val IS_LOGIN = "is_login"
const val IS_CROSSING_TAB="is_crossing_tab"
const val FOOD_ID="food_id"