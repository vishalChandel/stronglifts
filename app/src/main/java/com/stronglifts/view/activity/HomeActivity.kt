package com.stronglifts.view.activity

import android.os.Bundle
import com.google.android.material.navigation.NavigationBarView
import com.goserve.utils.AppPreference
import com.goserve.utils.IS_CROSSING_TAB
import com.stronglifts.R
import com.stronglifts.databinding.ActivityHomeBinding
import com.stronglifts.view.fragment.HomeFragment1
import com.stronglifts.view.fragment.HomeFragment2
import com.stronglifts.view.fragment.HomeFragment3
import com.stronglifts.view.fragment.HomeFragment4

class HomeActivity : BaseActivity() {

    // - - Initialize Objects
    private lateinit var binding: ActivityHomeBinding
    private var isHomeFragment1Clicked = false
    private var isHomeFragment2Clicked = false
    private var isHomeFragment3Clicked = false
    private var isHomeFragment4Clicked = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityHomeBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initView()
    }

    private fun initView() {
        switchFragment(HomeFragment1(), "", false, null)
        binding.bottomNavigationView.setOnItemSelectedListener(NavigationBarView.OnItemSelectedListener {
            when (it.itemId) {
                R.id.actionFragment1 -> {
                    if(!isHomeFragment1Clicked) {
                        switchFragment(HomeFragment1(), "", false, null)
                    }
                    isHomeFragment1Clicked = true
                    isHomeFragment2Clicked = false
                    isHomeFragment3Clicked = false
                    isHomeFragment4Clicked = false
                }
                R.id.actionFragment2 -> {
                    if(!isHomeFragment2Clicked) {
                        switchFragment(HomeFragment2(), "", false, null)
                    }
                    isHomeFragment1Clicked = false
                    isHomeFragment2Clicked = true
                    isHomeFragment3Clicked = false
                    isHomeFragment4Clicked = false
                }
                R.id.actionFragment3 -> {
                    if(!isHomeFragment3Clicked) {
                        switchFragment(HomeFragment3(), "", false, null)
                    }
                    isHomeFragment1Clicked = false
                    isHomeFragment2Clicked = false
                    isHomeFragment3Clicked = true
                    isHomeFragment4Clicked = false
                }
                R.id.actionFragment4 -> {
                    if(!isHomeFragment4Clicked) {
                        switchFragment(HomeFragment4(), "", false, null)
                    }
                    isHomeFragment1Clicked = false
                    isHomeFragment2Clicked = false
                    isHomeFragment3Clicked = false
                    isHomeFragment4Clicked = true
                }
            }
            true

        })

    }

    override fun onBackPressed() {
        finish()
        AppPreference().writeBoolean(mActivity, IS_CROSSING_TAB, false)
    }

    override fun finish() {
        super.finish()
        AppPreference().writeBoolean(mActivity, IS_CROSSING_TAB, false)
    }
}