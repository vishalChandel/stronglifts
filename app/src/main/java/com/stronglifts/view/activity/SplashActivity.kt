package com.stronglifts.view.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.bumptech.glide.Glide
import com.goserve.utils.SPLASH_TIME_OUT
import com.stronglifts.R
import com.stronglifts.databinding.ActivityHomeBinding
import com.stronglifts.databinding.ActivitySplashBinding

class SplashActivity : BaseActivity() {

    // - - Initialize Objects
    private lateinit var binding: ActivitySplashBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySplashBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initView()
    }

    private fun initView() {
        // Adding the gif here using glide library
        Glide.with(this).load(R.drawable.ic_splash_gif).into(binding.imgSplashGifIV)
        setUpSplash()
    }

    private fun setUpSplash() {
        val mThread = object : Thread() {
            override fun run() {
                sleep(SPLASH_TIME_OUT.toLong())
                startActivity(Intent(mActivity, WelcomeActivity::class.java))
                finish()
            }
        }
        mThread.start()
    }
}