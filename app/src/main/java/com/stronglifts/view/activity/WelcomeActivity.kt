package com.stronglifts.view.activity

import android.content.Intent
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.util.Log
import android.view.ViewGroup
import android.widget.Button
import com.goserve.utils.AppPreference
import com.goserve.utils.IS_CROSSING_TAB
import com.goserve.utils.IS_LOGIN
import com.stronglifts.database.DBHelper
import com.stronglifts.database.DBQuery
import com.stronglifts.databinding.ActivityWelcomeBinding


class WelcomeActivity : BaseActivity() {

    // - - Initialize Objects
    private lateinit var binding: ActivityWelcomeBinding
    private var dbHandler: DBHelper? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityWelcomeBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initView()
        performClick()
    }

    private fun initView() {
        AppPreference().writeBoolean(mActivity, IS_CROSSING_TAB, false)
        if (!isLogin()) {
            AppPreference().writeBoolean(mActivity, IS_LOGIN, true)
            val db = DBQuery(this)
            db.createDatabase()
            db.close()
            dbHandler = DBHelper(this)
            dbHandler!!.createTables()
        } else {
            val db = DBQuery(this)
            db.open()
        }
    }

    private fun performClick() {
        binding.imgGetStartedIV.setOnClickListener {
            val intent = Intent(mActivity, HomeActivity::class.java)
            startActivity(intent)
            finish()
        }
    }
}