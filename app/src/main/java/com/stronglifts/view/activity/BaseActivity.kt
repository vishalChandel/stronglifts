package com.stronglifts.view.activity

import android.app.Activity
import android.os.Bundle
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.goserve.utils.AppPreference
import com.goserve.utils.IS_LOGIN
import com.stronglifts.R
import kotlinx.android.synthetic.main.item_growing_harvest.*

open class BaseActivity: AppCompatActivity() {

    // - - Get Class Name
    var TAG = this@BaseActivity.javaClass.simpleName

    // - - Initialize Activity
    var mActivity: Activity = this@BaseActivity

    // - - To Check Whether User is logged_in or not
    fun isLogin(): Boolean {
        return AppPreference().readBoolean(mActivity, IS_LOGIN, false)
    }

    // - - Switch Between Fragments
    fun switchFragment(
        fragment: Fragment?,
        Tag: String?,
        addToStack: Boolean,
        bundle: Bundle?
    ) {
        val fragmentManager = supportFragmentManager
        if (fragment != null) {
            val fragmentTransaction = fragmentManager.beginTransaction()
            fragmentTransaction.replace(R.id.frameLayout, fragment, Tag)
            if (addToStack) fragmentTransaction.addToBackStack(Tag)
            if (bundle != null) fragment.arguments = bundle
            fragmentTransaction.commit()
            fragmentManager.executePendingTransactions()
        }
    }
}