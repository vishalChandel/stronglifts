package com.stronglifts.view.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.stronglifts.R
import com.stronglifts.databinding.FragmentHome4Binding
import com.stronglifts.databinding.FragmentVideoDetailBinding

class VideoDetailFragment : BaseFragment() {

    // - - Initialize Objects
    private lateinit var binding: FragmentVideoDetailBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentVideoDetailBinding.inflate(inflater, container, false)
        container!!.removeAllViews()
        performClick()
        return binding.root
    }

    private fun performClick() {
        binding.backRL.setOnClickListener {
            switchFragment(HomeFragment4(),"",false,null)
        }

        binding!!.txtGetUrlTV.setOnClickListener {

        }
    }

}