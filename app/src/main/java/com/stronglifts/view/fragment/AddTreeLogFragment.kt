package com.stronglifts.view.fragment

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.TextView
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.ViewModelProvider
import com.stronglifts.R
import com.stronglifts.database.DBHelper
import com.stronglifts.database.DBQuery
import com.stronglifts.databinding.FragmentAddTreeLogBinding
import com.stronglifts.model.GrowingPlantsModel
import com.stronglifts.model.TreeLogModel
import com.stronglifts.sharedViewModel.GrowingPlantDetailSharedViewModel
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

class AddTreeLogFragment : BaseFragment() {

    // - - Initialize Objects
    private lateinit var binding: FragmentAddTreeLogBinding
    private lateinit var model: GrowingPlantDetailSharedViewModel
    private var dbHandler: DBHelper? = null
    private var mTreeLogArraylist: ArrayList<TreeLogModel>? = null
    private var mGrowingPlantId = ""
    private var mTreeLogId = ""
    private var mFragmentTag = ""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentAddTreeLogBinding.inflate(inflater, container, false)
        model = ViewModelProvider(requireActivity()).get(GrowingPlantDetailSharedViewModel::class.java)
        initView()
        onViewClicked()
        return binding.root
    }

    private fun initView() {
        showGrowingPlantsSpinner()
        getBundleData()
    }

    private fun onViewClicked() {

        binding.backRL.setOnClickListener {
            if (mFragmentTag == "GrowingPlantDetailFragment") {
                val fm: FragmentManager? = fragmentManager
                fm!!.popBackStack()
                hideSoftKeyboard(requireActivity())
            } else {
                val fm: FragmentManager? = fragmentManager
                fm!!.popBackStack()
            }
        }

        binding.txtSelectDateTV.setOnClickListener {
            datePicker(binding.txtSelectDateTV)
        }


        binding.txtSaveTV.setOnClickListener {
            if (isValidate()) {
                hideSoftKeyboard(requireActivity())
                if (mTreeLogId.isEmpty()) {
                    dbHandler = DBHelper(requireActivity())
                    dbHandler!!.addTreeLog(
                        mGrowingPlantId,
                        convertDateInTimestamp(binding.txtSelectDateTV.text.toString()).toString(),
                        binding.edtNoteET.text.toString()
                    )
                    val mSelectedDate = SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()).format(Date())

                    val db = DBQuery(requireContext())
                    db.open()
                    mTreeLogArraylist = db.getTreeLogListByGrowingPlant(convertDateInTimestamp(mSelectedDate).toString(), mGrowingPlantId)
                    db.close()
                    model.setSelected(mTreeLogArraylist!!)
                    if (mFragmentTag == "GrowingPlantDetailFragment") {
                        val fm: FragmentManager? = fragmentManager
                        fm!!.popBackStack()
                        hideSoftKeyboard(requireActivity())
                    } else {
                        val fm: FragmentManager? = fragmentManager
                        fm!!.popBackStack()
                    }
                } else {
                    dbHandler = DBHelper(requireActivity())
                    dbHandler!!.updateTreeLog(
                        mTreeLogId,
                        mGrowingPlantId,
                        convertDateInTimestamp(binding.txtSelectDateTV.text.toString()).toString(),
                        binding.edtNoteET.text.toString()
                    )
                    val mSelectedDate = SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()).format(Date())
                    val db = DBQuery(requireContext())
                    db.open()
                    mTreeLogArraylist = db.getTreeLogListByGrowingPlant(convertDateInTimestamp(mSelectedDate).toString(), mGrowingPlantId)
                    db.close()
                    model.setSelected(mTreeLogArraylist!!)
                    if (mFragmentTag == "GrowingPlantDetailFragment") {
                        val fm: FragmentManager? = fragmentManager
                        fm!!.popBackStack()
                        hideSoftKeyboard(requireActivity())
                    } else {
                        val fm: FragmentManager? = fragmentManager
                        fm!!.popBackStack()
                    }
                }

            }
        }
    }

    private fun getBundleData() {
        if (arguments != null) {
            val bundle = arguments
            if (bundle!!.getString("mTag").isNullOrEmpty()) {
                mTreeLogId = bundle!!.getString("mTreeLogId").toString()
                val mPlantId = bundle!!.getString("mPlantId").toString()
                val mDate = bundle!!.getString("mDate").toString()
                val mDesc = bundle!!.getString("mDesc").toString()
                val db = DBQuery(requireContext())
                db.open()
                val mGrowingPlantsList = db.getGrowingPlantsList()
                db.close()
                if (mPlantId.isNotEmpty()) {
                    for (i in 0 until mGrowingPlantsList!!.size) {
                        if (mGrowingPlantsList[i].item_id == mPlantId) {
                            binding.chooseTreeSpinner.setSelection(i)
                        }
                    }
                }
                if (mDate.isNotEmpty()) {
                    binding.txtSelectDateTV.text = convertTimestampInDate(mDate)
                }
                if (mDesc.isNotEmpty()) {
                    binding.edtNoteET.setText(mDesc)
                }
            }
            mFragmentTag = bundle!!.getString("FragmentTag").toString()
            if (!bundle!!.getString("FragmentTag").toString().isNullOrEmpty()) {
                if (bundle!!.getString("FragmentTag").toString() == "GrowingPlantDetailFragment") {
                    val mPlantId = bundle!!.getString("mPlantId").toString()
                    val db = DBQuery(requireContext())
                    db.open()
                    val mGrowingPlantsList = db.getGrowingPlantsList()
                    db.close()
                    if (mPlantId.isNotEmpty()) {
                        for (i in 0 until mGrowingPlantsList!!.size) {
                            if (mGrowingPlantsList[i].item_id == mPlantId) {
                                binding.chooseTreeSpinner.setSelection(i)
                            }
                        }
                    }
                    // disable user interaction
                    binding.chooseTreeSpinner.setOnTouchListener { _, _ -> false }
                    binding.chooseTreeRL.setOnTouchListener { _, _ -> false }
                    binding.chooseTreeSpinner.isEnabled = false
                    binding.chooseTreeRL.isEnabled = false
                    binding.chooseTreeSpinner.isClickable = false
                    binding.chooseTreeRL.isClickable = false
                }
            }

        }
    }

    // - - Validations for input fields
    private fun isValidate(): Boolean {
        var flag = true
        when {
            binding.txtSelectDateTV?.text.toString().trim() == "" -> {
                showAlertDialog(requireActivity(), getString(R.string.please_select_date))
                flag = false
            }
            binding.edtNoteET?.text.toString().trim() == "" -> {
                showAlertDialog(requireActivity(), getString(R.string.please_enter_note))
                flag = false
            }
        }
        return flag
    }


    private fun showGrowingPlantsSpinner() {
        val db = DBQuery(requireContext())
        db.open()
        val mGrowingPlantsList = db.getGrowingPlantsList()
        db.close()
        val mFoodAdapter: ArrayAdapter<GrowingPlantsModel> = object :
            ArrayAdapter<GrowingPlantsModel>(
                requireActivity(), android.R.layout.simple_spinner_item, mGrowingPlantsList!!
            ) {
            override fun getDropDownView(
                position: Int,
                convertView: View?,
                parent: ViewGroup
            ): View {
                var v = convertView
                if (v == null) {
                    val mContext = this.context
                    if (mContext != null) {
                        val vi =
                            (mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater)
                        v = vi.inflate(R.layout.layout_spinner, null)
                    }
                }
                val itemTextView = v?.findViewById<TextView>(R.id.itemTV)
                val mModel: GrowingPlantsModel? = mGrowingPlantsList?.get(position)
                if (mModel?.plant_alt_name.isNullOrEmpty()) {
                    itemTextView?.text = mModel?.name
                } else {
                    itemTextView?.text = mModel?.name + " (" + mModel?.plant_alt_name + ")"
                }
                return v!!
            }
        }
        binding.chooseTreeSpinner.adapter = mFoodAdapter

        /* Tree Spinner Click Listener */
        binding.chooseTreeSpinner.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View,
                    position: Int,
                    id: Long
                ) {
                    (view as TextView).setTextColor(resources.getColor(R.color.colorBlack))
                    val mModel: GrowingPlantsModel? = mGrowingPlantsList?.get(position)
                    if (mModel?.plant_alt_name.isNullOrEmpty()) {
                        view?.text = mModel?.name
                    } else {
                        view?.text = mModel?.name + " (" + mModel?.plant_alt_name + ")"
                    }
                    mGrowingPlantId = mModel?.item_id.toString()
                    view.setPadding(30, 0, 0, 0)
                    view.textSize = 15f
                    binding.chooseTreeSpinner.setSelection(position)
                }

                override fun onNothingSelected(parent: AdapterView<*>?) {}
            }
    }
}