package com.stronglifts.view.fragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.goserve.utils.AppPreference
import com.goserve.utils.FOOD_ID
import com.shrikanthravi.collapsiblecalendarview.data.Day
import com.shrikanthravi.collapsiblecalendarview.view.OnSwipeTouchListener
import com.shrikanthravi.collapsiblecalendarview.widget.CollapsibleCalendar
import com.stronglifts.Interface.*
import com.stronglifts.R
import com.stronglifts.adapter.ActionAdapter
import com.stronglifts.adapter.EnvLogAdapter
import com.stronglifts.adapter.TreeLogAdapter
import com.stronglifts.database.DBHelper
import com.stronglifts.database.DBQuery
import com.stronglifts.databinding.FragmentHome3Binding
import com.stronglifts.model.ActionModel
import com.stronglifts.model.EnvLogModel
import com.stronglifts.model.TreeLogModel
import com.stronglifts.sharedViewModel.EditGrowingPlantActionSharedViewModel
import com.stronglifts.sharedViewModel.EnvLogSharedViewModel
import com.stronglifts.sharedViewModel.GrowingPlantDetailSharedViewModel
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*


class HomeFragment3 : BaseFragment() {
    // - - Initialize Objects
    private lateinit var binding: FragmentHome3Binding
    private lateinit var mActionAdapter: ActionAdapter
    private lateinit var mTreeLogAdapter: TreeLogAdapter
    private lateinit var mEnvLogAdapter: EnvLogAdapter
    private var mActionArraylist: ArrayList<ActionModel>? = null
    private var mTreeLogArraylist: ArrayList<TreeLogModel>? = null
    private var mEnvLogArraylist: ArrayList<EnvLogModel>? = null
    private var dbHandler: DBHelper? = null
    private var mSelectedDate = ""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentHome3Binding.inflate(inflater, container, false)

        val mEditGrowingPlantActionSharedViewModel = ViewModelProvider(requireActivity()).get(EditGrowingPlantActionSharedViewModel::class.java)
        mEditGrowingPlantActionSharedViewModel.getSelected().observe(viewLifecycleOwner) {
            mActionArraylist?.clear()
            val db = DBQuery(requireContext())
            db.open()
            mActionArraylist = db.getActionList(convertDateInTimestamp(mSelectedDate).toString())

            if (!db.getActionListByRepeatType().isNullOrEmpty()) {
                for (i in 0 until db.getActionListByRepeatType()!!.size) {
                    val time1 = SimpleDateFormat("dd/MM/yyyy").parse(mSelectedDate)
                    val time2 = SimpleDateFormat("dd/MM/yyyy").parse(convertTimestampInDate(db.getActionListByRepeatType()!![i].ac_date))

                    if (time1 >= time2) {
                        if (!mActionArraylist!!.contains(db.getActionListByRepeatType()!![i])) {
                            mActionArraylist!!.add(db.getActionListByRepeatType()!![i])
                        }
                    }
                }
            }

            for (i in 0 until db.getActionListByRepeatTypeUntilDate()!!.size) {
                if (!db.getActionListByRepeatTypeUntilDate()!![i].ac_repeat_till.isNullOrEmpty()) {
                    val time1 = SimpleDateFormat("dd/MM/yyyy").parse(mSelectedDate)
                    val time2 = SimpleDateFormat("dd/MM/yyyy").parse(convertTimestampInDate(db.getActionListByRepeatTypeUntilDate()!![i].ac_date))
                    val time3 = SimpleDateFormat("dd/MM/yyyy").parse(convertTimestampInDate(db.getActionListByRepeatTypeUntilDate()!![i].ac_repeat_till))

                    if (time1 in time2..time3) {
                        if (!mActionArraylist!!.contains(db.getActionListByRepeatTypeUntilDate()!![i])) {
                            mActionArraylist!!.add(db.getActionListByRepeatTypeUntilDate()!![i])
                        }

                    }
                }
            }

            val mActionAllList = db.getActionListAll()

            for (i in 0 until mActionAllList!!.size) {
                if ( mActionAllList[i].ac_repeat_days != "1" && mActionAllList!![i].ac_repeat_till != "") {
                    var dateInString = convertTimestampInDate(mActionAllList!![i].ac_date)
                    for (j in 0 until 365) {
                        val sdf12 = SimpleDateFormat("dd/MM/yyyy")
                        val time1 = SimpleDateFormat("dd/MM/yyyy").parse(mSelectedDate)
                        val time2 = SimpleDateFormat("dd/MM/yyyy").parse(dateInString)
                        val time3 =
                            SimpleDateFormat("dd/MM/yyyy").parse(convertTimestampInDate(mActionAllList[i].ac_repeat_till))
                        val c = Calendar.getInstance()
                        c.time = sdf12.parse(dateInString)
                        c.add(Calendar.DATE, mActionAllList[i].ac_repeat_days.toInt())
                        val resultDate = Date(c.timeInMillis)
                        dateInString = sdf12.format(resultDate)
                        if (time1 == time2) {
                            if (!mActionArraylist!!.contains(mActionAllList!![i])) {
                                mActionArraylist!!.add(mActionAllList!![i])
                            }
                            break
                        }
                        if (time1 > time3) {
                            break
                        }
                    }
                }

            }

            for (i in 0 until mActionAllList!!.size) {
                if (mActionAllList!![i].ac_repeat_days != "1" && mActionAllList!![i].ac_repeat_type == "0") {
                    var dateInString = convertTimestampInDate(mActionAllList!![i].ac_date)
                    for (j in 0 until 365) {
                        val sdf12 = SimpleDateFormat("dd/MM/yyyy")
                        val time1 = SimpleDateFormat("dd/MM/yyyy").parse(mSelectedDate)
                        val time2 = SimpleDateFormat("dd/MM/yyyy").parse(dateInString)
                        val c = Calendar.getInstance()
                        c.time = sdf12.parse(dateInString)
                        if (mActionAllList[i].ac_repeat_days == "") {
                            c.add(Calendar.DATE, 0)
                        } else {
                            c.add(Calendar.DATE, mActionAllList[i].ac_repeat_days.toInt())
                        }
                        val resultDate = Date(c.timeInMillis)
                        dateInString = sdf12.format(resultDate)
                        if (time1 == time2) {
                            if (!mActionArraylist!!.contains(mActionAllList!![i])) {
                                mActionArraylist!!.add(mActionAllList!![i])
                            }
                            break
                        }
                    }
                }

            }

            mTreeLogArraylist = db.getTreeLogList(convertDateInTimestamp(mSelectedDate).toString())
            mEnvLogArraylist = db.getEnvLogList(convertDateInTimestamp(mSelectedDate).toString())
            db.close()
            initActionRecyclerView()
            initTreeLogRecyclerView()
            initEnvLogRecyclerView()
            try {
                val sdf = SimpleDateFormat("dd/MM/yyyy")
                val d = sdf.parse(mSelectedDate)
                val formatterOut = SimpleDateFormat("EEE, dd MMM")
                try {
                    val nextDate = formatterOut.format(d!!)

                    if (mSelectedDate == SimpleDateFormat(
                            "dd/MM/yyyy",
                            Locale.getDefault()
                        ).format(Date())
                    ) {
                        if (mActionArraylist.isNullOrEmpty()) {
                            binding.actionsDateTV.text = "No Actions for today $nextDate"
                        } else {
                            binding.actionsDateTV.text = "Actions for today $nextDate"
                        }
                    } else {
                        if (mActionArraylist.isNullOrEmpty()) {
                            binding.actionsDateTV.text = "No Actions for $nextDate"
                        } else {
                            binding.actionsDateTV.text = "Actions for $nextDate"
                        }
                    }
                } catch (e: ParseException) {
                    e.printStackTrace()
                }
            } catch (ex: ParseException) {
            }
        }

        val mEnvLogSharedViewModel = ViewModelProvider(requireActivity()).get(EnvLogSharedViewModel::class.java)
        mEnvLogSharedViewModel.getSelected().observe(viewLifecycleOwner) {
            mActionArraylist?.clear()
            val db = DBQuery(requireContext())
            db.open()
            mActionArraylist = db.getActionList(convertDateInTimestamp(mSelectedDate).toString())
            if (!db.getActionListByRepeatType().isNullOrEmpty()) {
                for (i in 0 until db.getActionListByRepeatType()!!.size) {
                    val time1 = SimpleDateFormat("dd/MM/yyyy").parse(mSelectedDate)

                    val time2 = SimpleDateFormat("dd/MM/yyyy").parse(convertTimestampInDate(db.getActionListByRepeatType()!![i].ac_date))

                    if (time1 >= time2) {
                        if (!mActionArraylist!!.contains(db.getActionListByRepeatType()!![i])) {
                            mActionArraylist!!.add(db.getActionListByRepeatType()!![i])
                        }
                    }
                }
            }

            for (i in 0 until db.getActionListByRepeatTypeUntilDate()!!.size) {
                if (!db.getActionListByRepeatTypeUntilDate()!![i].ac_repeat_till.isNullOrEmpty()) {
                    val time1 = SimpleDateFormat("dd/MM/yyyy").parse(mSelectedDate)
                    val time2 = SimpleDateFormat("dd/MM/yyyy").parse(convertTimestampInDate(db.getActionListByRepeatTypeUntilDate()!![i].ac_date))
                    val time3 = SimpleDateFormat("dd/MM/yyyy").parse(convertTimestampInDate(db.getActionListByRepeatTypeUntilDate()!![i].ac_repeat_till))

                    if (time1 in time2..time3) {
                        if (!mActionArraylist!!.contains(db.getActionListByRepeatTypeUntilDate()!![i])) {
                            mActionArraylist!!.add(db.getActionListByRepeatTypeUntilDate()!![i])
                        }

                    }
                }
            }

            val mActionAllList = db.getActionListAll()

            for (i in 0 until mActionAllList!!.size) {
                if ( mActionAllList[i].ac_repeat_days != "1" && mActionAllList!![i].ac_repeat_till != "") {
                    var dateInString = convertTimestampInDate(mActionAllList!![i].ac_date)
                    for (j in 0 until 365) {
                        val sdf12 = SimpleDateFormat("dd/MM/yyyy")
                        val time1 = SimpleDateFormat("dd/MM/yyyy").parse(mSelectedDate)
                        val time2 = SimpleDateFormat("dd/MM/yyyy").parse(dateInString)
                        val time3 =
                            SimpleDateFormat("dd/MM/yyyy").parse(convertTimestampInDate(mActionAllList[i].ac_repeat_till))
                        val c = Calendar.getInstance()
                        c.time = sdf12.parse(dateInString)
                        c.add(Calendar.DATE, mActionAllList[i].ac_repeat_days.toInt())
                        val resultDate = Date(c.timeInMillis)
                        dateInString = sdf12.format(resultDate)
                        if (time1 == time2) {
                            if (!mActionArraylist!!.contains(mActionAllList!![i])) {
                                mActionArraylist!!.add(mActionAllList!![i])
                            }
                            break
                        }
                        if (time1 > time3) {
                            break
                        }
                    }
                }

            }

            for (i in 0 until mActionAllList!!.size) {
                if (mActionAllList!![i].ac_repeat_days != "1" && mActionAllList!![i].ac_repeat_type == "0") {
                    var dateInString =convertTimestampInDate(mActionAllList!![i].ac_date)
                    for (j in 0 until 365) {
                        val sdf12 = SimpleDateFormat("dd/MM/yyyy")
                        val time1 = SimpleDateFormat("dd/MM/yyyy").parse(mSelectedDate)
                        val time2 = SimpleDateFormat("dd/MM/yyyy").parse(dateInString)
                        val c = Calendar.getInstance()
                        c.time = sdf12.parse(dateInString)
                        if (mActionAllList[i].ac_repeat_days == "") {
                            c.add(Calendar.DATE, 0)
                        } else {
                            c.add(Calendar.DATE, mActionAllList[i].ac_repeat_days.toInt())
                        }
                        val resultDate = Date(c.timeInMillis)
                        dateInString = sdf12.format(resultDate)
                        if (time1 == time2) {
                            if (!mActionArraylist!!.contains(mActionAllList!![i])) {
                                mActionArraylist!!.add(mActionAllList!![i])
                            }
                            break
                        }
                    }
                }

            }

            mTreeLogArraylist = db.getTreeLogList(convertDateInTimestamp(mSelectedDate).toString())
            mEnvLogArraylist = db.getEnvLogList(convertDateInTimestamp(mSelectedDate).toString())
            db.close()
            initActionRecyclerView()
            initTreeLogRecyclerView()
            initEnvLogRecyclerView()
            try {
                val sdf = SimpleDateFormat("dd/MM/yyyy")
                val d = sdf.parse(mSelectedDate)
                val formatterOut = SimpleDateFormat("EEE, dd MMM")
                try {
                    val nextDate = formatterOut.format(d!!)

                    if (mSelectedDate == SimpleDateFormat(
                            "dd/MM/yyyy",
                            Locale.getDefault()
                        ).format(Date())
                    ) {
                        if (mActionArraylist.isNullOrEmpty()) {
                            binding.actionsDateTV.text = "No Actions for today $nextDate"
                        } else {
                            binding.actionsDateTV.text = "Actions for today $nextDate"
                        }
                    } else {
                        if (mActionArraylist.isNullOrEmpty()) {
                            binding.actionsDateTV.text = "No Actions for $nextDate"
                        } else {
                            binding.actionsDateTV.text = "Actions for $nextDate"
                        }
                    }
                } catch (e: ParseException) {
                    e.printStackTrace()
                }
            } catch (ex: ParseException) {
            }
        }

        val mGrowingPlantDetailSharedViewModel = ViewModelProvider(requireActivity()).get(GrowingPlantDetailSharedViewModel::class.java)
        mGrowingPlantDetailSharedViewModel.getSelected().observe(viewLifecycleOwner) {
            mActionArraylist?.clear()
            val db = DBQuery(requireContext())
            db.open()
            mActionArraylist = db.getActionList(convertDateInTimestamp(mSelectedDate).toString())
            if (!db.getActionListByRepeatType().isNullOrEmpty()) {
                for (i in 0 until db.getActionListByRepeatType()!!.size) {
                    val time1 = SimpleDateFormat("dd/MM/yyyy").parse(mSelectedDate)

                    val time2 = SimpleDateFormat("dd/MM/yyyy").parse(convertTimestampInDate(db.getActionListByRepeatType()!![i].ac_date))

                    if (time1 >= time2) {
                        if (!mActionArraylist!!.contains(db.getActionListByRepeatType()!![i])) {
                            mActionArraylist!!.add(db.getActionListByRepeatType()!![i])
                        }
                    }
                }
            }

            for (i in 0 until db.getActionListByRepeatTypeUntilDate()!!.size) {
                if (!db.getActionListByRepeatTypeUntilDate()!![i].ac_repeat_till.isNullOrEmpty()) {
                    val time1 = SimpleDateFormat("dd/MM/yyyy").parse(mSelectedDate)
                    val time2 = SimpleDateFormat("dd/MM/yyyy").parse(convertTimestampInDate(db.getActionListByRepeatTypeUntilDate()!![i].ac_date))
                    val time3 = SimpleDateFormat("dd/MM/yyyy").parse(convertTimestampInDate(db.getActionListByRepeatTypeUntilDate()!![i].ac_repeat_till))

                    if (time1 in time2..time3) {
                        if (!mActionArraylist!!.contains(db.getActionListByRepeatTypeUntilDate()!![i])) {
                            mActionArraylist!!.add(db.getActionListByRepeatTypeUntilDate()!![i])
                        }

                    }
                }
            }

            val mActionAllList = db.getActionListAll()

            for (i in 0 until mActionAllList!!.size) {
                if ( mActionAllList[i].ac_repeat_days != "1" && mActionAllList!![i].ac_repeat_till != "") {
                    var dateInString = convertTimestampInDate(mActionAllList!![i].ac_date)
                    for (j in 0 until 365) {
                        val sdf12 = SimpleDateFormat("dd/MM/yyyy")
                        val time1 = SimpleDateFormat("dd/MM/yyyy").parse(mSelectedDate)
                        val time2 = SimpleDateFormat("dd/MM/yyyy").parse(dateInString)
                        val time3 =
                            SimpleDateFormat("dd/MM/yyyy").parse(convertTimestampInDate(mActionAllList[i].ac_repeat_till))
                        val c = Calendar.getInstance()
                        c.time = sdf12.parse(dateInString)
                        c.add(Calendar.DATE, mActionAllList[i].ac_repeat_days.toInt())
                        val resultDate = Date(c.timeInMillis)
                        dateInString = sdf12.format(resultDate)
                        if (time1 == time2) {
                            if (!mActionArraylist!!.contains(mActionAllList!![i])) {
                                mActionArraylist!!.add(mActionAllList!![i])
                            }
                            break
                        }
                        if (time1 > time3) {
                            break
                        }
                    }
                }

            }

            for (i in 0 until mActionAllList!!.size) {
                if (mActionAllList!![i].ac_repeat_days != "1" && mActionAllList!![i].ac_repeat_type == "0") {
                    var dateInString = convertTimestampInDate(mActionAllList!![i].ac_date)
                    for (j in 0 until 365) {
                        val sdf12 = SimpleDateFormat("dd/MM/yyyy")
                        val time1 = SimpleDateFormat("dd/MM/yyyy").parse(mSelectedDate)
                        val time2 = SimpleDateFormat("dd/MM/yyyy").parse(dateInString)
                        val c = Calendar.getInstance()
                        c.time = sdf12.parse(dateInString)
                        if (mActionAllList[i].ac_repeat_days == "") {
                            c.add(Calendar.DATE, 0)
                        } else {
                            c.add(Calendar.DATE, mActionAllList[i].ac_repeat_days.toInt())
                        }
                        val resultDate = Date(c.timeInMillis)
                        dateInString = sdf12.format(resultDate)
                        if (time1 == time2) {
                            if (!mActionArraylist!!.contains(mActionAllList!![i])) {
                                mActionArraylist!!.add(mActionAllList!![i])
                            }
                            break
                        }
                    }
                }

            }

            mTreeLogArraylist = db.getTreeLogList(convertDateInTimestamp(mSelectedDate).toString())
            mEnvLogArraylist = db.getEnvLogList(convertDateInTimestamp(mSelectedDate).toString())
            db.close()
            initActionRecyclerView()
            initTreeLogRecyclerView()
            initEnvLogRecyclerView()
            try {
                val sdf = SimpleDateFormat("dd/MM/yyyy")
                val d = sdf.parse(mSelectedDate)
                val formatterOut = SimpleDateFormat("EEE, dd MMM")
                try {
                    val nextDate = formatterOut.format(d!!)

                    if (mSelectedDate == SimpleDateFormat(
                            "dd/MM/yyyy",
                            Locale.getDefault()
                        ).format(Date())
                    ) {
                        if (mActionArraylist.isNullOrEmpty()) {
                            binding.actionsDateTV.text = "No Actions for today $nextDate"
                        } else {
                            binding.actionsDateTV.text = "Actions for today $nextDate"
                        }
                    } else {
                        if (mActionArraylist.isNullOrEmpty()) {
                            binding.actionsDateTV.text = "No Actions for $nextDate"
                        } else {
                            binding.actionsDateTV.text = "Actions for $nextDate"
                        }
                    }
                } catch (e: ParseException) {
                    e.printStackTrace()
                }
            } catch (ex: ParseException) {
            }
        }

        initView()
        performClick()
        return binding.root
    }

    private fun initView() {
        hideSoftKeyboard(requireActivity())
        binding.calenderView.setOnTouchListener(object : OnSwipeTouchListener(requireActivity()) {
            override fun onSwipeRight() {
                binding.collapsibleCalendarView.nextMonth()
            }

            override fun onSwipeLeft() {
                binding.collapsibleCalendarView.prevMonth()
            }

            override fun onSwipeTop() {
                if (binding.collapsibleCalendarView.expanded) {
                    binding.collapsibleCalendarView.collapse(400)
                }
            }

            override fun onSwipeBottom() {
                if (!binding.collapsibleCalendarView.expanded) {
                    binding.collapsibleCalendarView.expand(400)
                }
            }
        })
        //To hide or show expand icon
        binding.collapsibleCalendarView.setExpandIconVisible(true)
        val today = GregorianCalendar()
        binding.collapsibleCalendarView.selectedDay = Day(
            today.get(Calendar.YEAR),
            today.get(Calendar.MONTH),
            today.get(Calendar.DAY_OF_MONTH)
        )
        binding.collapsibleCalendarView.setCalendarListener(object :
            CollapsibleCalendar.CalendarListener {

            override fun onDayChanged() {

            }

            override fun onClickListener() {
                if (binding.collapsibleCalendarView.expanded) {
                    binding.collapsibleCalendarView.collapse(400)
                } else {
                    binding.collapsibleCalendarView.expand(400)
                }
            }

            override fun onDaySelect() {
                mActionArraylist?.clear()
                val mSelectedMonth = binding.collapsibleCalendarView?.selectedDay!!.month + 1
                mSelectedDate =
                    getFormatedString("" + getFormatedString("" + binding.collapsibleCalendarView?.selectedDay!!.day.toString())).toString() + "/" + getFormatedString(
                        "" + getFormatedString("" + mSelectedMonth.toString())
                    ).toString() + "/" + binding.collapsibleCalendarView?.selectedDay!!.year.toString()
                val db = DBQuery(requireContext())
                db.open()
                mActionArraylist = db.getActionList(convertDateInTimestamp(mSelectedDate).toString())
                if (!db.getActionListByRepeatType().isNullOrEmpty()) {
                    for (i in 0 until db.getActionListByRepeatType()!!.size) {
                        val time1 = SimpleDateFormat("dd/MM/yyyy").parse(mSelectedDate)

                        val time2 = SimpleDateFormat("dd/MM/yyyy").parse(convertTimestampInDate(db.getActionListByRepeatType()!![i].ac_date))

                        if (time1 >= time2) {
                            if (!mActionArraylist!!.contains(db.getActionListByRepeatType()!![i])) {
                                mActionArraylist!!.add(db.getActionListByRepeatType()!![i])
                            }
                        }
                    }
                }

                for (i in 0 until db.getActionListByRepeatTypeUntilDate()!!.size) {
                    if (!db.getActionListByRepeatTypeUntilDate()!![i].ac_repeat_till.isNullOrEmpty()) {
                        val time1 = SimpleDateFormat("dd/MM/yyyy").parse(mSelectedDate)
                        val time2 = SimpleDateFormat("dd/MM/yyyy").parse(convertTimestampInDate(db.getActionListByRepeatTypeUntilDate()!![i].ac_date))
                        val time3 = SimpleDateFormat("dd/MM/yyyy").parse(convertTimestampInDate(db.getActionListByRepeatTypeUntilDate()!![i].ac_repeat_till))

                        if (time1 in time2..time3) {
                            if (!mActionArraylist!!.contains(db.getActionListByRepeatTypeUntilDate()!![i])) {
                                mActionArraylist!!.add(db.getActionListByRepeatTypeUntilDate()!![i])
                            }
                        }
                    }
                }

                val mActionAllList = db.getActionListAll()

                for (i in 0 until mActionAllList!!.size) {
                    if ( mActionAllList[i].ac_repeat_days != "1" && mActionAllList!![i].ac_repeat_till != "") {
                        var dateInString = convertTimestampInDate(mActionAllList!![i].ac_date)
                        for (j in 0 until 365) {
                            val sdf12 = SimpleDateFormat("dd/MM/yyyy")
                            val time1 = SimpleDateFormat("dd/MM/yyyy").parse(mSelectedDate)
                            val time2 = SimpleDateFormat("dd/MM/yyyy").parse(dateInString)
                            val time3 = SimpleDateFormat("dd/MM/yyyy").parse(convertTimestampInDate(mActionAllList[i].ac_repeat_till))
                            val c = Calendar.getInstance()
                            c.time = sdf12.parse(dateInString)
                            c.add(Calendar.DATE, mActionAllList[i].ac_repeat_days.toInt())
                            val resultDate = Date(c.timeInMillis)
                            dateInString = sdf12.format(resultDate)
                            if (time1 == time2) {
                                if (!mActionArraylist!!.contains(mActionAllList!![i])) {
                                    mActionArraylist!!.add(mActionAllList!![i])
                                }
                                break
                            }
                            if (time1 > time3) {
                                break
                            }
                        }
                    }

                }

                for (i in 0 until mActionAllList!!.size) {
                    if (mActionAllList!![i].ac_repeat_days != "1" && mActionAllList!![i].ac_repeat_type == "0") {
                        var dateInString = convertTimestampInDate(mActionAllList!![i].ac_date)
                        for (j in 0 until 365) {
                            val sdf12 = SimpleDateFormat("dd/MM/yyyy")
                            val time1 = SimpleDateFormat("dd/MM/yyyy").parse(mSelectedDate)
                            val time2 = SimpleDateFormat("dd/MM/yyyy").parse(dateInString)
                            val c = Calendar.getInstance()
                            c.time = sdf12.parse(dateInString)
                            if (mActionAllList[i].ac_repeat_days == "") {
                                c.add(Calendar.DATE, 0)
                            } else {
                                c.add(Calendar.DATE, mActionAllList[i].ac_repeat_days.toInt())
                            }
                            val resultDate = Date(c.timeInMillis)
                            dateInString = sdf12.format(resultDate)
                            if (time1 == time2) {
                                if (!mActionArraylist!!.contains(mActionAllList!![i])) {
                                    mActionArraylist!!.add(mActionAllList!![i])
                                }
                                break
                            }
                        }
                    }

                }

                mTreeLogArraylist = db.getTreeLogList(convertDateInTimestamp(mSelectedDate).toString())
                mEnvLogArraylist = db.getEnvLogList(convertDateInTimestamp(mSelectedDate).toString())
                db.close()
                initActionRecyclerView()
                initTreeLogRecyclerView()
                initEnvLogRecyclerView()
                try {
                    val sdf = SimpleDateFormat("dd/MM/yyyy")
                    val d = sdf.parse(mSelectedDate)
                    val formatterOut = SimpleDateFormat("EEE, dd MMM")
                    try {
                        val nextDate = formatterOut.format(d!!)

                        if (mSelectedDate == SimpleDateFormat(
                                "dd/MM/yyyy",
                                Locale.getDefault()
                            ).format(Date())
                        ) {
                            if (mActionArraylist.isNullOrEmpty()) {
                                binding.actionsDateTV.text = "No Actions for today $nextDate"
                            } else {
                                binding.actionsDateTV.text = "Actions for today $nextDate"
                            }
                        } else {
                            if (mActionArraylist.isNullOrEmpty()) {
                                binding.actionsDateTV.text = "No Actions for $nextDate"
                            } else {
                                binding.actionsDateTV.text = "Actions for $nextDate"
                            }
                        }
                    } catch (e: ParseException) {
                        e.printStackTrace()
                    }
                } catch (ex: ParseException) {
                }
            }

            override fun onItemClick(v: View) {

            }

            override fun onDataUpdate() {

            }

            override fun onMonthChange() {

            }

            override fun onWeekChange(position: Int) {

            }
        })

        mSelectedDate = SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()).format(Date())
        mActionArraylist?.clear()
        val db = DBQuery(requireContext())
        db.open()
        mActionArraylist = db.getActionList(convertDateInTimestamp(mSelectedDate).toString())
        if (!db.getActionListByRepeatType().isNullOrEmpty()) {
            for (i in 0 until db.getActionListByRepeatType()!!.size) {
                val time1 = SimpleDateFormat("dd/MM/yyyy").parse(mSelectedDate)

                val time2 = SimpleDateFormat("dd/MM/yyyy").parse(convertTimestampInDate(db.getActionListByRepeatType()!![i].ac_date))

                if (time1 >= time2) {
                    if (!mActionArraylist!!.contains(db.getActionListByRepeatType()!![i])) {
                        mActionArraylist!!.add(db.getActionListByRepeatType()!![i])
                    }
                }
            }
        }
        for (i in 0 until db.getActionListByRepeatTypeUntilDate()!!.size) {
            if (!db.getActionListByRepeatTypeUntilDate()!![i].ac_repeat_till.isNullOrEmpty()) {
                val time1 = SimpleDateFormat("dd/MM/yyyy").parse(mSelectedDate)
                val time2 = SimpleDateFormat("dd/MM/yyyy").parse(convertTimestampInDate(db.getActionListByRepeatTypeUntilDate()!![i].ac_date))
                val time3 = SimpleDateFormat("dd/MM/yyyy").parse(convertTimestampInDate(db.getActionListByRepeatTypeUntilDate()!![i].ac_repeat_till))

                if (time1 in time2..time3) {
                    if (!mActionArraylist!!.contains(db.getActionListByRepeatTypeUntilDate()!![i])) {
                        mActionArraylist!!.add(db.getActionListByRepeatTypeUntilDate()!![i])
                    }

                }
            }
        }
        val mActionAllList = db.getActionListAll()
        for (i in 0 until mActionAllList!!.size) {
            if ( mActionAllList[i].ac_repeat_days != "1" && mActionAllList!![i].ac_repeat_till != "") {
                var dateInString = convertTimestampInDate(mActionAllList!![i].ac_date)
                for (j in 0 until 365) {
                    val sdf12 = SimpleDateFormat("dd/MM/yyyy")
                    val time1 = SimpleDateFormat("dd/MM/yyyy").parse(mSelectedDate)
                    val time2 = SimpleDateFormat("dd/MM/yyyy").parse(dateInString)
                    val time3 =
                        SimpleDateFormat("dd/MM/yyyy").parse(convertTimestampInDate(mActionAllList[i].ac_repeat_till))
                    val c = Calendar.getInstance()
                    c.time = sdf12.parse(dateInString)
                    c.add(Calendar.DATE, mActionAllList[i].ac_repeat_days.toInt())
                    val resultDate = Date(c.timeInMillis)
                    dateInString = sdf12.format(resultDate)
                    if (time1 == time2) {
                        if (!mActionArraylist!!.contains(mActionAllList!![i])) {
                            mActionArraylist!!.add(mActionAllList!![i])
                        }
                        break
                    }
                    if (time1 > time3) {
                        break
                    }
                }
            }
        }
        for (i in 0 until mActionAllList!!.size) {
            if (mActionAllList!![i].ac_repeat_days != "1" && mActionAllList!![i].ac_repeat_type == "0") {
                var dateInString = convertTimestampInDate(mActionAllList!![i].ac_date)
                for (j in 0 until 365) {
                    val sdf12 = SimpleDateFormat("dd/MM/yyyy")
                    val time1 = SimpleDateFormat("dd/MM/yyyy").parse(mSelectedDate)
                    val time2 = SimpleDateFormat("dd/MM/yyyy").parse(dateInString)
                    val c = Calendar.getInstance()
                    c.time = sdf12.parse(dateInString)
                    if (mActionAllList[i].ac_repeat_days == "") {
                        c.add(Calendar.DATE, 0)
                    } else {
                        c.add(Calendar.DATE, mActionAllList[i].ac_repeat_days.toInt())
                    }
                    val resultDate = Date(c.timeInMillis)
                    dateInString = sdf12.format(resultDate)
                    if (time1 == time2) {
                        if (!mActionArraylist!!.contains(mActionAllList!![i])) {
                            mActionArraylist!!.add(mActionAllList!![i])
                        }
                        break
                    }
                }
            }

        }
        mTreeLogArraylist = db.getTreeLogList(convertDateInTimestamp(mSelectedDate).toString())
        mEnvLogArraylist = db.getEnvLogList(convertDateInTimestamp(mSelectedDate).toString())
        db.close()
        initActionRecyclerView()
        initTreeLogRecyclerView()
        initEnvLogRecyclerView()
        val sdf = SimpleDateFormat("EEE, dd MMM")
        val d = Date()
        val dayOfTheWeek = sdf.format(d)
        if (mActionArraylist.isNullOrEmpty()) {
            binding.actionsDateTV.text = "No Actions for today $dayOfTheWeek"
        } else {
            binding.actionsDateTV.text = "Actions for today $dayOfTheWeek"
        }
    }

    private fun performClick() {
        binding.foodLL.setOnClickListener {
            switchFragment(FoodListFragment(), "", false, null)
        }

        binding.actionLL.setOnClickListener {
            val mFragment = AddActionFragment()
            val fragmentManager = fragmentManager
            val fragmentTransaction = fragmentManager!!.beginTransaction()
            fragmentTransaction.add(R.id.mainRL, mFragment, "")
            fragmentTransaction.addToBackStack(null)
            fragmentTransaction.commit()
            fragmentManager.executePendingTransactions()
        }

        binding.TreeLogLL.setOnClickListener {
            val mFragment = AddTreeLogFragment()
            val fragmentManager = fragmentManager
            val fragmentTransaction = fragmentManager!!.beginTransaction()
            fragmentTransaction.add(R.id.mainRL, mFragment, "")
            fragmentTransaction.addToBackStack(null)
            fragmentTransaction.commit()
            fragmentManager.executePendingTransactions()
        }

        binding.envLogLL.setOnClickListener {
            val mFragment = AddEnvironmentLogFragment()
            val fragmentManager = fragmentManager
            val fragmentTransaction = fragmentManager!!.beginTransaction()
            fragmentTransaction.add(R.id.mainRL, mFragment, "")
            fragmentTransaction.addToBackStack(null)
            fragmentTransaction.commit()
            fragmentManager.executePendingTransactions()
        }
    }

    private fun initActionRecyclerView() {
        val layoutManager: RecyclerView.LayoutManager =
            LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        binding.actionRV.layoutManager = layoutManager
        mActionArraylist!!.toSet().toList()
        mActionAdapter = ActionAdapter(
            activity,
            mActionArraylist,
            mEditActionClickListenerInterface,
            mDoneActionClickListenerInterface,
            mSelectedDate
        )
        binding.actionRV.adapter = mActionAdapter
    }

    private fun initTreeLogRecyclerView() {
        val layoutManager: RecyclerView.LayoutManager =
            LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        binding.treeLogRV.layoutManager = layoutManager
        mTreeLogAdapter = TreeLogAdapter(
            activity,
            mTreeLogArraylist,
            mEditTreeLogClickListenerInterface,
            mDeleteTreeLogClickListenerInterface
        )
        binding.treeLogRV.adapter = mTreeLogAdapter
    }

    private fun initEnvLogRecyclerView() {
        val layoutManager: RecyclerView.LayoutManager =
            LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        binding.envLogRV.layoutManager = layoutManager
        mEnvLogAdapter = EnvLogAdapter(
            activity,
            mEnvLogArraylist,
            mEditEnvLogClickListenerInterface,
            mDeleteEnvLogClickListenerInterface
        )
        binding.envLogRV.adapter = mEnvLogAdapter
    }

    private var mEditActionClickListenerInterface = object : EditActionClickListenerInterface {
        override fun mClickListenerInterface(mModel: ActionModel) {
            val bundle = Bundle()
            bundle.putString("mActionTag", "Edit")
            bundle.putParcelable("actionModel", mModel)
            val mFragment = AddActionFragment()
            mFragment.arguments = bundle
            val fragmentManager = childFragmentManager
            val fragmentTransaction = fragmentManager.beginTransaction()
            fragmentTransaction.add(R.id.mainRL, mFragment, "")
            fragmentTransaction.addToBackStack(null)
            fragmentTransaction.commit()
            fragmentManager.executePendingTransactions()
        }
    }

    private var mDoneActionClickListenerInterface = object : DoneActionClickListenerInterface {
        override fun mClickListenerInterface(mModel: ActionModel) {
            val db = DBQuery(requireActivity())
            db.open()
            val mActionStatusList = db.getActionStatus(mModel.ac_id, mSelectedDate)
            db.close()

            if (!mActionStatusList.isNullOrEmpty()) {
                for (i in 0 until mActionStatusList!!.size) {
                    if (mActionStatusList!![i].ac_id == mModel.ac_id && mActionStatusList!![i].ac_status_date == mSelectedDate) {
                        dbHandler = DBHelper(requireActivity()!!)
                        dbHandler!!.deleteActionStatus(mModel.ac_id, mSelectedDate)
                    } else {
                        dbHandler = DBHelper(requireActivity()!!)
                        dbHandler!!.addActionStatus(mModel.ac_id, mSelectedDate)
                    }
                }
            } else {
                dbHandler = DBHelper(requireActivity()!!)
                dbHandler!!.addActionStatus(mModel.ac_id, mSelectedDate)
            }
            initActionRecyclerView()
        }
    }

    private var mEditTreeLogClickListenerInterface =
        object : EditTreeLogClickListenerInterface {
            override fun mClickListenerInterface(mModel: TreeLogModel) {
                val bundle = Bundle()
                bundle.putString("mTreeLogId", mModel.tl_id)
                bundle.putString("mPlantId", mModel.plant_id)
                bundle.putString("mDate", mModel.log_date)
                bundle.putString("mDesc", mModel.log_desc)
                val mFragment = AddTreeLogFragment()
                mFragment.arguments = bundle
                val fragmentManager = childFragmentManager
                val fragmentTransaction = fragmentManager.beginTransaction()
                fragmentTransaction.add(R.id.mainRL, mFragment, "")
                fragmentTransaction.addToBackStack(null)
                fragmentTransaction.commit()
                fragmentManager.executePendingTransactions()
            }
        }

    private
    var mEditEnvLogClickListenerInterface =
        object : EditEnvLogClickListenerInterface {
            override fun mClickListenerInterface(mModel: EnvLogModel) {
                val bundle = Bundle()
                bundle.putString("mEnvLogId", mModel.ev_id)
                bundle.putString("mEnv", mModel.env_name)
                bundle.putString("mDate", mModel.env_log_date)
                bundle.putString("mDesc", mModel.env_log_desc)
                val mFragment = AddEnvironmentLogFragment()
                mFragment.arguments = bundle
                val fragmentManager = childFragmentManager
                val fragmentTransaction =
                    fragmentManager.beginTransaction()
                fragmentTransaction.add(R.id.mainRL, mFragment, "")
                fragmentTransaction.addToBackStack(null)
                fragmentTransaction.commit()
                fragmentManager.executePendingTransactions()
            }
        }

    private var mDeleteTreeLogClickListenerInterface =
        object : DeleteTreeLogClickListenerInterface {
            override fun mClickListenerInterface(mModel: TreeLogModel) {
                dbHandler = DBHelper(requireActivity()!!)
                dbHandler!!.deleteTreeLog(mModel.tl_id)
                val db = DBQuery(requireContext())
                db.open()
                mTreeLogArraylist = db.getTreeLogList(convertDateInTimestamp(mSelectedDate).toString())
                mEnvLogArraylist = db.getEnvLogList(convertDateInTimestamp(mSelectedDate).toString())
                db.close()
                initTreeLogRecyclerView()
                initEnvLogRecyclerView()
            }
        }

    private var mDeleteEnvLogClickListenerInterface =
        object :
            DeleteEnvLogClickListenerInterface {
            override fun mClickListenerInterface(
                mModel: EnvLogModel
            ) {
                dbHandler = DBHelper(requireActivity()!!)
                dbHandler!!.deleteEnvLog(mModel.ev_id)
                val db = DBQuery(requireContext())
                db.open()
                mTreeLogArraylist = db.getTreeLogList(convertDateInTimestamp(mSelectedDate).toString())
                mEnvLogArraylist = db.getEnvLogList(convertDateInTimestamp(mSelectedDate).toString())
                db.close()
                initTreeLogRecyclerView()
                initEnvLogRecyclerView()
            }
        }
}