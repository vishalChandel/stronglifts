package com.stronglifts.view.fragment

import android.os.Bundle

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.stronglifts.databinding.FragmentChartBinding


class ChartFragment : BaseFragment() {
    // - - Initialize Objects
    private lateinit var binding: FragmentChartBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentChartBinding.inflate(inflater, container, false)
        container!!.removeAllViews()
        onViewClicked()
        return binding.root
    }

    private fun onViewClicked() {
        binding.backRL.setOnClickListener {
            switchFragment(HomeFragment4(), "", false, null)
        }
    }


}