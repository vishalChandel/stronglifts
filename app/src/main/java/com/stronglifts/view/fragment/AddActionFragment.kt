package com.stronglifts.view.fragment

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.*
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.goserve.utils.AppPreference
import com.goserve.utils.FOOD_ID
import com.stronglifts.R
import com.stronglifts.adapter.ActionTypeSpinnerAdapter
import com.stronglifts.adapter.FoodListAdapter
import com.stronglifts.adapter.NutrientsListAdapter
import com.stronglifts.database.DBHelper
import com.stronglifts.database.DBQuery
import com.stronglifts.databinding.FragmentAddActionBinding
import com.stronglifts.model.ActionModel
import com.stronglifts.model.ActionTypeModel
import com.stronglifts.model.FoodModel
import com.stronglifts.model.GrowingPlantsModel
import com.stronglifts.sharedViewModel.EditGrowingPlantActionSharedViewModel
import com.stronglifts.sharedViewModel.FoodListSharedViewModel
import com.stronglifts.sharedViewModel.GrowingPlantDetailSharedViewModel
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class AddActionFragment : BaseFragment() {
    // - - Initialize Objects
    private lateinit var binding: FragmentAddActionBinding
    private var mActionTypeList: ArrayList<ActionTypeModel> = ArrayList()
    private var mNutrientsList: ArrayList<FoodModel> = ArrayList()
    private lateinit var model: EditGrowingPlantActionSharedViewModel
    private lateinit var mNutrientsListAdapter: NutrientsListAdapter
    private var dbHandler: DBHelper? = null
    private var mActionTag = ""
    private var mFragmentTag = ""
    private var mActionId = ""
    private var mGrowingPlantId = ""
    private var mActionType = ""
    private var mActionDate = ""
    private var mActionDescription = ""
    private var mActionTypeMethod = ""
    private var mActionTypeQuantity = ""
    private var mActionTypeWaterAmount = ""
    private var mActionTypeNutrientList = ""
    private var mActionRepeat = ""
    private var mActionRepeatType = ""
    private var mActionRepeatTill = ""
    private var mActionRepeatDays = ""
    private var mActionRepeatStatus = "No repeat"
    private var mNoOfDays = 1
    private var mWaterArray = arrayOf("ml", "cc", "m3", "in3", "ft3", "gal", "mm3", "l", "cups", "ft. oz", "pint", "quart", "tbsp")
    private var mNutrientArray = arrayOf("Liquid", "Spray", "Solid")
    private var mRepellentArray = arrayOf("Liquid", "Spray", "Solid")
    private var mTrainingArray = arrayOf("FIM", "LST(Low stress training)", "SCROG", "Super cropping", "Topping")
    private var mTrimArray = arrayOf("Topping", "FIM", "Defoliation", "Lollipopping", "Lower branches")
    private var mRepeatArray = arrayOf("No Repeat", "Repeat Daily")
    private var mRepeatTimeArray = arrayOf("Forever", "Until a date")
    private var mStateArray = arrayOf("Flowering", "Germination", "Seedling", "Vegetative")


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentAddActionBinding.inflate(inflater, container, false)
        val model = ViewModelProvider(requireActivity()).get(FoodListSharedViewModel::class.java)
        model.getSelected().observe(viewLifecycleOwner) {
            val db = DBQuery(requireContext())
            db.open()
            val mFoodList = db.getFoodListByFoodId(foodId())
            db.close()
            if (!mFoodList.isNullOrEmpty()) {
                if(!mNutrientsList.contains(mFoodList[0])) {
                    mNutrientsList.add(0, mFoodList[0])
                }
            }
            initNutrientsRecyclerView()
        }
        initView()
        onViewClicked()
        return binding.root
    }

    private fun initView() {
        model = ViewModelProvider(requireActivity()).get(EditGrowingPlantActionSharedViewModel::class.java)
        showGrowingPlantsSpinner()
        addActionTypeData()
        showActionTypeSpinner()
        mWaterSpinner()
        nutrientsSpinner()
        repellentSpinner()
        trainingSpinner()
        trimSpinner()
        stateSpinner()
        getBundleData()
    }

    private fun onViewClicked() {
        binding.backRL.setOnClickListener {
            if (mFragmentTag == "GrowingPlantDetailFragment" || mFragmentTag == "GrowingPlantDetailFragmentEdit") {
                val fm: FragmentManager? = fragmentManager
                fm!!.popBackStack()
                hideSoftKeyboard(requireActivity())
                requireActivity()?.let { AppPreference().writeString(it, FOOD_ID, "") }
            } else {
                requireActivity()?.let { AppPreference().writeString(it, FOOD_ID, "") }
                val fm: FragmentManager? = fragmentManager
                fm!!.popBackStack()
            }
        }

        binding.txtSelectDateTV.setOnClickListener {
            datePicker(binding.txtSelectDateTV)
        }

        binding.repeatLL.setOnClickListener {
            showRepeatAlertDialog(requireActivity())
        }

        binding.addNutrientMixTV.setOnClickListener {
            val bundle = Bundle()
            bundle.putString("FragmentTag", "AddAction")
            bundle.putParcelableArrayList("nutrientList",mNutrientsList)
            val mFragment = FoodListFragment()
            mFragment.arguments = bundle
            val fragmentManager = childFragmentManager
            val fragmentTransaction = fragmentManager.beginTransaction()
            fragmentTransaction.add(R.id.mainRL, mFragment, "")
            fragmentTransaction.addToBackStack(null)
            fragmentTransaction.commit()
            fragmentManager.executePendingTransactions()
        }

        binding.txtSaveTV.setOnClickListener {
            if (isValidate()) {
                if (mActionTag == "Edit") {
                    mActionTypeNutrientList = mNutrientsList.joinToString { it.food_id }
                    mActionDescription = binding.edtNoteET.text.toString()
                    mActionTypeWaterAmount = binding.edtWaterAmountET.text.toString()
                    mActionDate = binding.txtSelectDateTV.text.toString()
                    dbHandler = DBHelper(requireActivity())
                    if(mActionRepeatTill.isEmpty()){
                        dbHandler!!.updateAction(
                            mActionId,
                            mGrowingPlantId,
                            mActionType,
                            convertDateInTimestamp(mActionDate).toString(),
                            mActionDescription,
                            mActionTypeMethod,
                            mActionTypeQuantity,
                            mActionTypeWaterAmount,
                            mActionTypeNutrientList,
                            mActionRepeat,
                            mActionRepeatType,
                            mActionRepeatTill,
                            mActionRepeatDays,
                            mActionRepeatStatus
                        )
                    }else {
                        dbHandler!!.updateAction(
                            mActionId,
                            mGrowingPlantId,
                            mActionType,
                            convertDateInTimestamp(mActionDate).toString(),
                            mActionDescription,
                            mActionTypeMethod,
                            mActionTypeQuantity,
                            mActionTypeWaterAmount,
                            mActionTypeNutrientList,
                            mActionRepeat,
                            mActionRepeatType,
                            convertDateInTimestamp(mActionRepeatTill).toString(),
                            mActionRepeatDays,
                            mActionRepeatStatus
                        )
                    }
                    val mSelectedDate = SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()).format(Date())
                    val db = DBQuery(requireContext())
                    db.open()
                    val mActionArrayList = db.getActionList(convertDateInTimestamp(mSelectedDate).toString())
                    db.close()
                    model.setSelected(mActionArrayList!!)
                } else {
                    mActionTypeNutrientList = mNutrientsList.joinToString { it -> "${it.food_id}" }
                    mActionDescription = binding.edtNoteET.text.toString()
                    mActionTypeWaterAmount = binding.edtWaterAmountET.text.toString()
                    mActionDate = binding.txtSelectDateTV.text.toString()
                    dbHandler = DBHelper(requireActivity())
//                    if (mGrowingPlantId == "All") {
//                        val db = DBQuery(requireContext())
//                        db.open()
//                        val mAllGrowingPlantsList = db.getGrowingPlantsList()
//                        db.close()
//                        for (i in 0 until mAllGrowingPlantsList!!.size) {
//                            dbHandler!!.addAction(
//                                mAllGrowingPlantsList!![i].item_id.toString(),
//                                mActionType,
//                                mActionDate,
//                                mActionDescription,
//                                mActionTypeMethod,
//                                mActionTypeQuantity,
//                                mActionTypeWaterAmount,
//                                mActionTypeNutrientList,
//                                mActionRepeat,
//                                mActionRepeatType,
//                                mActionRepeatTill,
//                                mActionRepeatDays,
//                                mActionRepeatStatus
//                            )
//                        }
//                    } else {
//                        dbHandler!!.addAction(
//                            mGrowingPlantId,
//                            mActionType,
//                            mActionDate,
//                            mActionDescription,
//                            mActionTypeMethod,
//                            mActionTypeQuantity,
//                            mActionTypeWaterAmount,
//                            mActionTypeNutrientList,
//                            mActionRepeat,
//                            mActionRepeatType,
//                            mActionRepeatTill,
//                            mActionRepeatDays,
//                            mActionRepeatStatus
//                        )
//                    }
                    if(mActionRepeatTill.isEmpty()){
                        dbHandler!!.addAction(
                            mGrowingPlantId,
                            mActionType,
                            convertDateInTimestamp(mActionDate).toString(),
                            mActionDescription,
                            mActionTypeMethod,
                            mActionTypeQuantity,
                            mActionTypeWaterAmount,
                            mActionTypeNutrientList,
                            mActionRepeat,
                            mActionRepeatType,
                            mActionRepeatTill,
                            mActionRepeatDays,
                            mActionRepeatStatus
                        )
                    }else{
                        dbHandler!!.addAction(
                            mGrowingPlantId,
                            mActionType,
                            convertDateInTimestamp(mActionDate).toString(),
                            mActionDescription,
                            mActionTypeMethod,
                            mActionTypeQuantity,
                            mActionTypeWaterAmount,
                            mActionTypeNutrientList,
                            mActionRepeat,
                            mActionRepeatType,
                            convertDateInTimestamp(mActionRepeatTill).toString(),
                            mActionRepeatDays,
                            mActionRepeatStatus
                        )
                    }

                    val mSelectedDate = SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()).format(Date())
                    val db = DBQuery(requireContext())
                    db.open()
                    val mActionArrayList = db.getActionList(convertDateInTimestamp(mSelectedDate).toString())
                    db.close()
                    model.setSelected(mActionArrayList!!)
                }
                if (mFragmentTag == "GrowingPlantDetailFragment" || mFragmentTag == "GrowingPlantDetailFragmentEdit") {
                    val fm: FragmentManager? = fragmentManager
                    fm!!.popBackStack()
                    hideSoftKeyboard(requireActivity())
                } else {
                    val fm: FragmentManager? = fragmentManager
                    fm!!.popBackStack()
                }
            }

        }
    }

    // - - Validations for input fields
    private fun isValidate(): Boolean {
        var flag = true
        when {
            mActionType == "" || mActionType=="Action Type"-> {
                showAlertDialog(requireActivity(), getString(R.string.please_enter_action))
                flag = false
            }

            binding.txtSelectDateTV.text.toString().trim() == "" -> {
                showAlertDialog(requireActivity(), getString(R.string.please_select_date))
                flag = false
            }
            binding.edtNoteET.text.toString().trim() == "" -> {
                showAlertDialog(
                    requireActivity(),
                    getString(R.string.please_enter_description)
                )
                flag = false
            }

            mGrowingPlantId == "" -> {
                showAlertDialog(requireActivity(), getString(R.string.please_choose_tree))
                flag = false
            }

            mActionType == "Water" -> {
                if (binding.edtWaterAmountET.text.toString().trim() == "") {
                    showAlertDialog(
                        requireActivity(),
                        getString(R.string.please_enter_water_amount)
                    )
                    flag = false
                }
            }
        }
        return flag
    }

    private fun getBundleData() {
        requireActivity().let { AppPreference().writeString(it, FOOD_ID, "") }
        if (arguments != null) {
            val bundle = arguments
            mActionTag = bundle!!.getString("mActionTag").toString()
            mFragmentTag = bundle!!.getString("FragmentTag").toString()
            if (bundle.getString("FragmentTag").toString() != "GrowingPlantDetailFragment") {
                val mActionModel: ActionModel = bundle.getParcelable("actionModel")!!
                setDataOnWidgets(mActionModel)
                if(mActionModel.ac_repeat=="0"){
                    mActionRepeat= "0"
                }else if(mActionModel.ac_repeat=="1"){
                    mActionRepeat="1"
                }
                if(mActionModel.ac_repeat_type=="0"){
                    mActionRepeatType="0"
                }else if(mActionModel.ac_repeat_type=="1"){
                    mActionRepeatType="1"
                }
                mActionRepeatDays=mActionModel.ac_repeat_days


               if(mActionModel.ac_repeat_till.isEmpty()){
                   mActionRepeatTill =  mActionModel.ac_repeat_till
                }else {
                   mActionRepeatTill =  convertTimestampInDate(mActionModel.ac_repeat_till)
                }
                mActionRepeatStatus=mActionModel.ac_repeat_status
            } else {
                if (!bundle!!.getString("FragmentTag").toString().isNullOrEmpty()) {
                    if (bundle!!.getString("FragmentTag").toString() == "GrowingPlantDetailFragment"
                    ) {
                        val mPlantId = bundle!!.getString("mPlantId").toString()
                        val db = DBQuery(requireContext())
                        db.open()
                        val mGrowingPlantsList = db.getGrowingPlantsList()
                        db.close()
                        if (mPlantId.isNotEmpty()) {
                            for (i in 0 until mGrowingPlantsList!!.size) {
                                if (mGrowingPlantsList[i].item_id == mPlantId) {
                                    binding.plantEnvSpinner.setSelection(i)
                                }
                            }
                        }
                        // disable user interaction
                        binding.plantEnvSpinner.setOnTouchListener { _, _ -> false }
                        binding.plantEnvRL.setOnTouchListener { _, _ -> false }
                        binding.plantEnvSpinner.isEnabled = false
                        binding.plantEnvRL.isEnabled = false
                        binding.plantEnvSpinner.isClickable = false
                        binding.plantEnvRL.isClickable = false
                    }
                }
            }
        }
    }

    private fun setDataOnWidgets(mActionModel: ActionModel) {
        mActionId = mActionModel.ac_id
        binding.txtSelectDateTV.text = convertTimestampInDate(mActionModel.ac_date)
        mActionDescription=mActionModel.ac_desc
        binding.edtNoteET.setText(mActionModel.ac_desc)
        when (mActionModel.ac_type) {
            "Water" -> {
                binding.waterLL.visibility = View.VISIBLE
                binding.nutrientsLL.visibility = View.GONE
                binding.repellentLL.visibility = View.GONE
                binding.trimLL.visibility = View.GONE
                binding.trainingLL.visibility = View.GONE
                binding.changeStateLL.visibility = View.GONE
                binding.recurrenceLL.visibility = View.VISIBLE
                binding.actionTypeSpinner.setSelection(1)
                binding.edtWaterAmountET.setText(mActionModel.ac_type_water_amount)
                binding.waterSpinner.setSelection(mActionModel.ac_type_water_quantity.toInt())
            }
            "Nutrients" -> {
                binding.waterLL.visibility = View.GONE
                binding.nutrientsLL.visibility = View.VISIBLE
                binding.repellentLL.visibility = View.GONE
                binding.trimLL.visibility = View.GONE
                binding.trainingLL.visibility = View.GONE
                binding.changeStateLL.visibility = View.GONE
                binding.actionTypeSpinner.setSelection(2)
                binding.recurrenceLL.visibility = View.VISIBLE
                binding.applicationMethodSpinner.setSelection(mActionModel.ac_type_method.toInt())
                binding.nutrientsRV.visibility = View.VISIBLE
                val mFoodIdList = mActionModel.ac_type_nutrients_list.split(",")
                Log.i(TAG, "setDataOnWidgets:" + mNutrientsList!!.size)
                val db1 = DBQuery(requireActivity())
                db1.open()
                mNutrientsList.clear()
                initNutrientsRecyclerView()
                for (i in 0 until mFoodIdList!!.size) {
                    val mList = db1.getFoodListByFoodId(mFoodIdList[i])
                    if (!mList.isNullOrEmpty()) {
                        if(!mNutrientsList.contains(mList[0])) {
                            mNutrientsList.add(0, mList[0])
                        }
                    }
                }
                Log.i(TAG, "setDataOnWidgets:" + mNutrientsList!!.size)
                db1.close()
                mNutrientsListAdapter.notifyDataSetChanged()
            }
            "Repellent" -> {
                binding.waterLL.visibility = View.GONE
                binding.nutrientsLL.visibility = View.GONE
                binding.repellentLL.visibility = View.VISIBLE
                binding.trimLL.visibility = View.GONE
                binding.trainingLL.visibility = View.GONE
                binding.recurrenceLL.visibility = View.VISIBLE
                binding.actionTypeSpinner.setSelection(3)
                binding.recurrenceLL.visibility = View.VISIBLE
                binding.changeStateLL.visibility = View.GONE
                binding.repellentSpinner.setSelection(mActionModel.ac_type_method.toInt())
            }
            "Transplant" -> {
                binding.waterLL.visibility = View.GONE
                binding.nutrientsLL.visibility = View.GONE
                binding.repellentLL.visibility = View.GONE
                binding.trimLL.visibility = View.GONE
                binding.trainingLL.visibility = View.GONE
                binding.recurrenceLL.visibility = View.VISIBLE
                binding.recurrenceLL.visibility = View.VISIBLE
                binding.changeStateLL.visibility = View.GONE
                binding.actionTypeSpinner.setSelection(4)
            }
            "Trim" -> {
                binding.waterLL.visibility = View.GONE
                binding.nutrientsLL.visibility = View.GONE
                binding.repellentLL.visibility = View.GONE
                binding.trimLL.visibility = View.VISIBLE
                binding.recurrenceLL.visibility = View.VISIBLE
                binding.trainingLL.visibility = View.GONE
                binding.recurrenceLL.visibility = View.VISIBLE
                binding.changeStateLL.visibility = View.GONE
                binding.actionTypeSpinner.setSelection(5)
                binding.trimSpinner.setSelection(mActionModel.ac_type_method.toInt())
            }
            "Training" -> {
                binding.waterLL.visibility = View.GONE
                binding.nutrientsLL.visibility = View.GONE
                binding.repellentLL.visibility = View.GONE
                binding.trimLL.visibility = View.GONE
                binding.changeStateLL.visibility = View.GONE
                binding.recurrenceLL.visibility = View.VISIBLE
                binding.recurrenceLL.visibility = View.VISIBLE
                binding.trainingLL.visibility = View.VISIBLE
                binding.actionTypeSpinner.setSelection(6)
                binding.trainingSpinner.setSelection(mActionModel.ac_type_method.toInt())
            }
            "Change Environment" -> {
                binding.waterLL.visibility = View.GONE
                binding.nutrientsLL.visibility = View.GONE
                binding.repellentLL.visibility = View.GONE
                binding.trimLL.visibility = View.GONE
                binding.recurrenceLL.visibility = View.VISIBLE
                binding.recurrenceLL.visibility = View.VISIBLE
                binding.trainingLL.visibility = View.GONE
                binding.changeStateLL.visibility = View.GONE
                binding.actionTypeSpinner.setSelection(7)
            }
            "Flush" -> {
                binding.waterLL.visibility = View.GONE
                binding.nutrientsLL.visibility = View.GONE
                binding.repellentLL.visibility = View.GONE
                binding.trimLL.visibility = View.GONE
                binding.trainingLL.visibility = View.GONE
                binding.recurrenceLL.visibility = View.VISIBLE
                binding.recurrenceLL.visibility = View.VISIBLE
                binding.changeStateLL.visibility = View.GONE
                binding.actionTypeSpinner.setSelection(8)
            }
            "Harvest" -> {
                binding.waterLL.visibility = View.GONE
                binding.nutrientsLL.visibility = View.GONE
                binding.repellentLL.visibility = View.GONE
                binding.trimLL.visibility = View.GONE
                binding.recurrenceLL.visibility = View.VISIBLE
                binding.recurrenceLL.visibility = View.VISIBLE
                binding.trainingLL.visibility = View.GONE
                binding.changeStateLL.visibility = View.GONE
                binding.actionTypeSpinner.setSelection(9)
            }
            "Declare Death" -> {
                binding.waterLL.visibility = View.GONE
                binding.nutrientsLL.visibility = View.GONE
                binding.repellentLL.visibility = View.GONE
                binding.trimLL.visibility = View.GONE
                binding.trainingLL.visibility = View.GONE
                binding.recurrenceLL.visibility = View.VISIBLE
                binding.recurrenceLL.visibility = View.VISIBLE
                binding.changeStateLL.visibility = View.GONE
                binding.recurrenceLL.visibility=View.GONE
                binding.actionTypeSpinner.setSelection(10)
            }
            "Change State" -> {
                binding.waterLL.visibility = View.GONE
                binding.nutrientsLL.visibility = View.GONE
                binding.repellentLL.visibility = View.GONE
                binding.trimLL.visibility = View.GONE
                binding.recurrenceLL.visibility = View.GONE
                binding.trainingLL.visibility = View.GONE
                binding.changeStateLL.visibility = View.VISIBLE
                binding.recurrenceLL.visibility = View.GONE
                binding.actionTypeSpinner.setSelection(11)
                binding.recurrenceLL.visibility=View.GONE
                binding.changeStateSpinner.setSelection(mActionModel.ac_type_method.toInt())
            }
            "Other" -> {
                binding.waterLL.visibility = View.GONE
                binding.nutrientsLL.visibility = View.GONE
                binding.repellentLL.visibility = View.GONE
                binding.trimLL.visibility = View.GONE
                binding.recurrenceLL.visibility = View.VISIBLE
                binding.trainingLL.visibility = View.GONE
                binding.recurrenceLL.visibility = View.VISIBLE
                binding.changeStateLL.visibility = View.GONE
                binding.actionTypeSpinner.setSelection(12)
            }
        }
        binding.repeatStatusTV.text = mActionModel.ac_repeat_status
    }

    private fun addActionTypeData() {
        mActionTypeList?.add(ActionTypeModel(R.drawable.ic_action, "Action Type"))
        mActionTypeList?.add(ActionTypeModel(R.drawable.ic_water, "Water"))
        mActionTypeList?.add(ActionTypeModel(R.drawable.ic_nutrients, "Nutrients"))
        mActionTypeList?.add(ActionTypeModel(R.drawable.ic_repellent, "Repellent"))
        mActionTypeList?.add(ActionTypeModel(R.drawable.ic_transplant, "Transplant"))
        mActionTypeList?.add(ActionTypeModel(R.drawable.ic_trim, "Trim"))
        mActionTypeList?.add(ActionTypeModel(R.drawable.ic_training, "Training"))
        mActionTypeList?.add(ActionTypeModel(R.drawable.ic_change_env, "Change Environment"))
        mActionTypeList?.add(ActionTypeModel(R.drawable.ic_flush, "Flush"))
        mActionTypeList?.add(ActionTypeModel(R.drawable.ic_harvest, "Harvest"))
        mActionTypeList?.add(ActionTypeModel(R.drawable.ic_death, "Declare Death"))
        mActionTypeList?.add(ActionTypeModel(R.drawable.ic_change_state, "Change State"))
        mActionTypeList?.add(ActionTypeModel(R.drawable.ic_other, "Other"))
    }

    private fun showActionTypeSpinner() {
        val customDropDownAdapter = ActionTypeSpinnerAdapter(requireActivity(), mActionTypeList)
        binding.actionTypeSpinner.adapter = customDropDownAdapter
        binding.actionTypeSpinner.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {

                override fun onItemSelected(
                    parent: AdapterView<*>,
                    view: View, position: Int, id: Long
                ) {
                    val clickedItem: ActionTypeModel = parent.getItemAtPosition(position) as ActionTypeModel

                    mActionType = clickedItem.actionName
                    when (clickedItem.actionName) {
                        "Water" -> {
                            binding.waterLL.visibility = View.VISIBLE
                            binding.nutrientsLL.visibility = View.GONE
                            binding.repellentLL.visibility = View.GONE
                            binding.trimLL.visibility = View.GONE
                            binding.trainingLL.visibility = View.GONE
                            binding.changeStateLL.visibility = View.GONE
                        }
                        "Nutrients" -> {
                            binding.waterLL.visibility = View.GONE
                            binding.nutrientsLL.visibility = View.VISIBLE
                            binding.repellentLL.visibility = View.GONE
                            binding.trimLL.visibility = View.GONE
                            binding.trainingLL.visibility = View.GONE
                            binding.changeStateLL.visibility = View.GONE
                            initNutrientsRecyclerView()
                        }
                        "Repellent" -> {
                            binding.waterLL.visibility = View.GONE
                            binding.nutrientsLL.visibility = View.GONE
                            binding.repellentLL.visibility = View.VISIBLE
                            binding.trimLL.visibility = View.GONE
                            binding.trainingLL.visibility = View.GONE
                            binding.changeStateLL.visibility = View.GONE
                        }
                        "Trim" -> {
                            binding.waterLL.visibility = View.GONE
                            binding.nutrientsLL.visibility = View.GONE
                            binding.repellentLL.visibility = View.GONE
                            binding.trimLL.visibility = View.VISIBLE
                            binding.trainingLL.visibility = View.GONE
                            binding.changeStateLL.visibility = View.GONE
                        }
                        "Training" -> {
                            binding.waterLL.visibility = View.GONE
                            binding.nutrientsLL.visibility = View.GONE
                            binding.repellentLL.visibility = View.GONE
                            binding.trimLL.visibility = View.GONE
                            binding.trainingLL.visibility = View.VISIBLE
                            binding.changeStateLL.visibility = View.GONE
                        }
                        "Change State" -> {
                            binding.waterLL.visibility = View.GONE
                            binding.nutrientsLL.visibility = View.GONE
                            binding.repellentLL.visibility = View.GONE
                            binding.trimLL.visibility = View.GONE
                            binding.trainingLL.visibility = View.GONE
                            binding.recurrenceLL.visibility=View.GONE
                            binding.changeStateLL.visibility = View.VISIBLE
                        }
                        "Declare Death" -> {
                            binding.waterLL.visibility = View.GONE
                            binding.nutrientsLL.visibility = View.GONE
                            binding.repellentLL.visibility = View.GONE
                            binding.trimLL.visibility = View.GONE
                            binding.trainingLL.visibility = View.GONE
                            binding.recurrenceLL.visibility=View.GONE
                            binding.changeStateLL.visibility = View.GONE
                        }
                        else -> {
                            binding.waterLL.visibility = View.GONE
                            binding.nutrientsLL.visibility = View.GONE
                            binding.repellentLL.visibility = View.GONE
                            binding.trimLL.visibility = View.GONE
                            binding.trainingLL.visibility = View.GONE
                            binding.changeStateLL.visibility = View.GONE
                        }

                    }

                }

                override fun onNothingSelected(parent: AdapterView<*>?) {
                }
            }
    }

    private fun mWaterSpinner() {
        val adapter = ArrayAdapter(requireActivity(), R.layout.layout_simple_spinner, mWaterArray)
        binding.waterSpinner.adapter = adapter

        binding.waterSpinner.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View, position: Int, id: Long
            ) {
                mActionTypeQuantity = position.toString()
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
            }
        }
    }

    private fun nutrientsSpinner() {
        val adapter =
            ArrayAdapter(requireActivity(), R.layout.layout_simple_spinner, mNutrientArray)
        binding.applicationMethodSpinner.adapter = adapter

        binding.applicationMethodSpinner.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View, position: Int, id: Long
            ) {
                mActionTypeMethod = position.toString()
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
            }
        }
    }

    private fun repellentSpinner() {
        val adapter =
            ArrayAdapter(requireActivity(), R.layout.layout_simple_spinner, mRepellentArray)
        binding.repellentSpinner.adapter = adapter

        binding.repellentSpinner.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View, position: Int, id: Long
            ) {
                mActionTypeMethod = position.toString()
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
            }
        }
    }

    private fun stateSpinner() {
        val adapter =
            ArrayAdapter(requireActivity(), R.layout.layout_simple_spinner, mStateArray)
        binding.changeStateSpinner.adapter = adapter

        binding.changeStateSpinner.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View, position: Int, id: Long
            ) {
                mActionTypeMethod = position.toString()
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
            }
        }
    }

    private fun trimSpinner() {
        val adapter = ArrayAdapter(requireActivity(), R.layout.layout_simple_spinner, mTrimArray)
        binding.trimSpinner.adapter = adapter

        binding.trimSpinner.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View, position: Int, id: Long
            ) {
                mActionTypeMethod = position.toString()
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
            }
        }
    }

    private fun trainingSpinner() {
        val adapter =
            ArrayAdapter(requireActivity(), R.layout.layout_simple_spinner, mTrainingArray)
        binding.trainingSpinner.adapter = adapter

        binding.trainingSpinner.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View, position: Int, id: Long
            ) {
                mActionTypeMethod = position.toString()
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
            }
        }
    }

    private fun repeatSpinner(
        repeatSpinner: Spinner,
        repeatDateTV: TextView,
        repeatTimeRL: RelativeLayout,
        repeatDaysLL: LinearLayout
    ) {
        val adapter = ArrayAdapter(requireActivity(), R.layout.layout_simple_spinner, mRepeatArray)
        repeatSpinner.adapter = adapter

        repeatSpinner.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View, position: Int, id: Long
            ) {
                if (position == 0) {
                    repeatDateTV.visibility = View.GONE
                    repeatTimeRL.visibility = View.GONE
                    repeatDaysLL.visibility = View.GONE
                    mActionRepeat = "0"
                } else {
                    repeatDateTV.visibility = View.GONE
                    repeatTimeRL.visibility = View.VISIBLE
                    repeatDaysLL.visibility = View.VISIBLE
                    mActionRepeat = "1"
                }

            }

            override fun onNothingSelected(parent: AdapterView<*>) {
            }
        }
    }

    private fun repeatTimeSpinner(timeIntervalSpinner: Spinner, repeatDateTV: TextView) {
        val adapter =
            ArrayAdapter(requireActivity(), R.layout.layout_simple_spinner, mRepeatTimeArray)
        timeIntervalSpinner.adapter = adapter

        timeIntervalSpinner.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View, position: Int, id: Long
            ) {
                if (position == 0) {
                    repeatDateTV.visibility = View.GONE
                    mActionRepeatType = "0"
                } else {
                    repeatDateTV.visibility = View.VISIBLE
                    mActionRepeatType = "1"
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
            }
        }
    }

    // - - To Show Repeat Alert Dialog
    private fun showRepeatAlertDialog(mActivity: Activity?) {
        val alertDialog = Dialog(mActivity!!)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.dialog_repeat)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set the custom dialog components - text, image and button
        val txtSaveTV = alertDialog.findViewById<TextView>(R.id.txtSaveTV)
        val repeatSpinner = alertDialog.findViewById<Spinner>(R.id.repeatSpinner)
        val timeIntervalSpinner = alertDialog.findViewById<Spinner>(R.id.timeIntervalSpinner)
        val repeatDateTV = alertDialog.findViewById<TextView>(R.id.repeatDateTV)
        val txtRepeatDaysTV = alertDialog.findViewById<TextView>(R.id.txtRepeatDaysTV)
        val repeatDaysLL = alertDialog.findViewById<LinearLayout>(R.id.repeatDaysLL)
        val repeatTimeRL = alertDialog.findViewById<RelativeLayout>(R.id.repeatTimeRL)
        val plusIV = alertDialog.findViewById<ImageView>(R.id.plusIV)
        val minusIV = alertDialog.findViewById<ImageView>(R.id.minusIV)
        repeatSpinner(repeatSpinner, repeatDateTV, repeatTimeRL, repeatDaysLL)
        repeatTimeSpinner(timeIntervalSpinner, repeatDateTV)

        if (arguments != null) {
            val bundle = arguments
            if (bundle!!.getString("FragmentTag").toString() != "GrowingPlantDetailFragment") {
                val mActionModel: ActionModel = bundle.getParcelable("actionModel")!!
                if(mActionModel.ac_repeat_days.isNotEmpty()) {
                    mNoOfDays = mActionModel.ac_repeat_days.toInt()
                }
                if (mNoOfDays == 1) {
                    txtRepeatDaysTV.text = "Repeat every $mNoOfDays day"
                } else {
                    txtRepeatDaysTV.text = "Repeat every $mNoOfDays days"
                }

                when (mActionModel.ac_repeat) {
                    "0" -> {
                        repeatSpinner.setSelection(0)
                    }
                    "1" -> {
                        repeatSpinner.setSelection(1)
                    }
                }
                when (mActionModel.ac_repeat_type) {
                    "0" -> {
                        timeIntervalSpinner.setSelection(0)
                    }
                    "1" -> {
                        timeIntervalSpinner.setSelection(1)
                          if(mActionRepeatTill.isEmpty()){
                              repeatDateTV.text=  mActionModel.ac_repeat_till
                        }else {
                              repeatDateTV.text=  convertTimestampInDate(mActionModel.ac_repeat_till)
                              mActionRepeatTill=  convertTimestampInDate(mActionModel.ac_repeat_till)
                        }
                    }
                }
            }
        }
        repeatDateTV.setOnClickListener {
            datePicker(repeatDateTV)
        }

        minusIV.setOnClickListener {
            if (mNoOfDays == 1) {
                mNoOfDays = 1
                txtRepeatDaysTV.text = "Repeat every $mNoOfDays day"
            } else {
                mNoOfDays -= 1
                if (mNoOfDays == 1) {
                    txtRepeatDaysTV.text = "Repeat every $mNoOfDays day"
                } else {
                    txtRepeatDaysTV.text = "Repeat every $mNoOfDays days"
                }
            }
        }

        plusIV.setOnClickListener {
            mNoOfDays += 1
            mActionRepeatDays = mNoOfDays.toString()

            txtRepeatDaysTV.text = "Repeat every $mNoOfDays days"

        }

        txtSaveTV.setOnClickListener {
            when (mActionRepeat) {
                "1" -> {
                    mActionRepeatDays = mNoOfDays.toString()
                }
                else -> {
                    mActionRepeatDays = "0"
                }
            }
            mActionRepeatTill = repeatDateTV.text.toString()
            if (mActionRepeat == "0") {
                binding.repeatStatusTV.text = "No repeat"
                mActionRepeatStatus = "No repeat"
            } else if (mActionRepeat == "1") {
                if (mActionRepeatType == "0") {
                    if (mNoOfDays == 1) {
                        binding.repeatStatusTV.text = "Repeat every day"
                        mActionRepeatStatus = "Repeat every day"
                    } else {
                        binding.repeatStatusTV.text = "Repeat every $mNoOfDays days"
                        mActionRepeatStatus = "Repeat every $mNoOfDays days"
                    }

                } else if (mActionRepeatType == "1") {
                    if (mNoOfDays == 1) {
                        binding.repeatStatusTV.text =
                            "Repeat every $mNoOfDays day until $mActionRepeatTill"
                        mActionRepeatStatus =
                            "Repeat every $mNoOfDays day until $mActionRepeatTill"
                    } else {
                        binding.repeatStatusTV.text =
                            "Repeat every $mNoOfDays days until $mActionRepeatTill"
                        mActionRepeatStatus =
                            "Repeat every $mNoOfDays days until $mActionRepeatTill"
                    }

                }
            }

            alertDialog.dismiss()
        }
        alertDialog.show()
    }

    private fun showGrowingPlantsSpinner() {

        val db = DBQuery(requireContext())
        db.open()
        val mGrowingPlantsList = db.getGrowingPlantsList()
        db.close()
        if (!mGrowingPlantsList.isNullOrEmpty()) {
            binding.hintTV.visibility = View.GONE
//            if (arguments == null) {
//                mGrowingPlantsList.add(0, GrowingPlantsModel(" ", "All", "", "All", "", "", "", ""))
//            }
        } else {
            binding.hintTV.visibility = View.VISIBLE
        }
        val mFoodAdapter: ArrayAdapter<GrowingPlantsModel> = object :
            ArrayAdapter<GrowingPlantsModel>(
                requireActivity(), android.R.layout.simple_spinner_item, mGrowingPlantsList!!
            ) {
            override fun getDropDownView(
                position: Int,
                convertView: View?,
                parent: ViewGroup
            ): View {
                var v = convertView
                if (v == null) {
                    val mContext = this.context
                    if (mContext != null) {
                        val vi =
                            (mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater)
                        v = vi.inflate(R.layout.layout_spinner, null)
                    }
                }
                val itemTextView = v?.findViewById<TextView>(R.id.itemTV)
                val mModel: GrowingPlantsModel? = mGrowingPlantsList?.get(position)
                if (mModel?.plant_alt_name.isNullOrEmpty()) {
                    itemTextView?.text = mModel?.name
                } else {
                    itemTextView?.text = mModel?.name + " (" + mModel?.plant_alt_name + ")"
                }
                return v!!
            }
        }
        binding.plantEnvSpinner.adapter = mFoodAdapter

        /* Tree Spinner Click Listener */
        binding.plantEnvSpinner.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View,
                    position: Int,
                    id: Long
                ) {
                    (view as TextView).setTextColor(resources.getColor(R.color.colorBlack))
                    val mModel: GrowingPlantsModel? = mGrowingPlantsList?.get(position)
                    if (mModel?.plant_alt_name.isNullOrEmpty()) {
                        view?.text = mModel?.name
                    } else {
                        view?.text = mModel?.name + " (" + mModel?.plant_alt_name + ")"
                    }
                    mGrowingPlantId = mModel?.item_id.toString()
                    view.setPadding(30, 0, 0, 0)
                    view.textSize = 15f
                    binding.plantEnvSpinner.setSelection(position)
                }

                override fun onNothingSelected(parent: AdapterView<*>?) {}
            }
    }

    private fun initNutrientsRecyclerView() {
        mNutrientsListAdapter = NutrientsListAdapter(activity, mNutrientsList)
        binding.nutrientsRV.adapter = mNutrientsListAdapter
    }
}