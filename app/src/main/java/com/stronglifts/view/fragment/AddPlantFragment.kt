package com.stronglifts.view.fragment

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.stronglifts.Interface.ClickListenerInterface
import com.stronglifts.Interface.ClickListenerInterfacewithData
import com.stronglifts.Interface.FilterClickListenerInterface
import com.stronglifts.R
import com.stronglifts.adapter.AddPlantAdapter
import com.stronglifts.adapter.FilterAdapter
import com.stronglifts.database.DBQuery
import com.stronglifts.databinding.FragmentAddPlantBinding
import com.stronglifts.model.PlantsModel


class AddPlantFragment : BaseFragment() {

    // - - Initialize Objects
    private lateinit var binding: FragmentAddPlantBinding
    private lateinit var mAddPlantAdapter: AddPlantAdapter
    private lateinit var mFilterAdapter: FilterAdapter
    private var mPlantsList: ArrayList<PlantsModel>? = null
    private var mPlantsCategoryList: ArrayList<String>? = null
    private var mFragmentTag = ""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentAddPlantBinding.inflate(inflater, container, false)
        initView()
        performClick()
        return binding.root
    }

    private fun performClick() {
        binding.backRL.setOnClickListener {
            val fm: FragmentManager? = fragmentManager
            fm!!.popBackStack()
            hideSoftKeyboard(requireActivity())
            setEditTextFocused(binding.edtSearchET)
            binding.edtSearchET.setText("")
        }
    }

    private fun initView() {
        hideSoftKeyboard(requireActivity())
        setEditTextFocused(binding.edtSearchET)
        binding.edtSearchET.setText("")
        val bundle = arguments
        mFragmentTag = bundle!!.getString("FragmentTag").toString()
        setEditTextFocused(binding.edtSearchET)
        val db = DBQuery(requireContext())
        db.open()
        mPlantsList = db.getPlantsList()
        mPlantsCategoryList=db.getPlantsCategoryList()
        mPlantsCategoryList!!.add(0,"All")
        // Create a new LinkedHashSet
        val set: MutableSet<String> = LinkedHashSet()
        // Add the elements to set
        set.addAll(mPlantsCategoryList!!)
        mPlantsCategoryList!!.clear()
        mPlantsCategoryList!!.addAll(set)
        db.close()
        initRecyclerView()
        setFilterAdapter()
        binding.edtSearchET.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

            }

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun afterTextChanged(s: Editable) {
                filter(s.toString())
            }
        })
    }

    private fun initRecyclerView() {
        val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        binding.addPlantRV.layoutManager = layoutManager
        mAddPlantAdapter = AddPlantAdapter(activity,mFragmentTag, mPlantsList, mInterface,mHomeFragment2Interface)
        binding.addPlantRV.adapter = mAddPlantAdapter
    }

    private fun setFilterAdapter() {
        val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        binding.filterRV.layoutManager = layoutManager
        mFilterAdapter = FilterAdapter(activity, mPlantsCategoryList, mFilterClickListenerInterface, "AddPlantFragment")
        binding.filterRV.adapter = mFilterAdapter
    }

    private var mInterface = object : ClickListenerInterfacewithData {
        override fun mClickListenerInterface(
            mPos: Int, mTag: String, mPlantsList: ArrayList<PlantsModel>?
        ) {
            hideSoftKeyboard(requireActivity())
            setEditTextFocused(binding.edtSearchET)
            binding.edtSearchET.setText("")
            val bundle = Bundle()
            bundle.putParcelableArrayList("mPlantsList", mPlantsList)
            bundle.putInt("mPos", mPos)
            bundle.putString("mTag", mTag)
            bundle.putString("FragmentTag", mFragmentTag)
            val plantFrag = PlantDetailFragment()
            plantFrag.arguments = bundle
            val fragmentManager = childFragmentManager
            val fragmentTransaction = fragmentManager.beginTransaction()
            fragmentTransaction.add(R.id.mainRL, plantFrag, "")
            fragmentTransaction.addToBackStack(null)
            fragmentTransaction.commit()
            fragmentManager.executePendingTransactions()
        }
    }

    private var mFilterClickListenerInterface = object : FilterClickListenerInterface {
        override fun mClickListenerInterface(
            mCategory: String,
            mTag: String,
        ) {
            hideSoftKeyboard(requireActivity())
            setEditTextFocused(binding.edtSearchET)
            binding.edtSearchET.setText("")
            val db = DBQuery(requireContext())
            db.open()
            mPlantsList!!.clear()
            mPlantsList = when (mCategory) {
                "All" -> {
                    db.getPlantsList()
                }
                else -> {
                    db.getPlantsListByCategory(mCategory)
                }
            }
            db.close()
            initRecyclerView()
        }
    }

    fun filter(text: String?) {
        val temp: ArrayList<PlantsModel> = ArrayList()
        for (d in mPlantsList!!) {
            //or use .equal(text) with you want equal match
            //use .toLowerCase() for better matches
            if (d.name.toLowerCase().contains(text!!.toLowerCase().toString())) {
                temp.add(d)
            }
        }
        //update recyclerview
        mAddPlantAdapter.updateList(temp)
    }

    private var mHomeFragment2Interface = object : ClickListenerInterface {
        override fun mClickListenerInterface(mGpId: Int,mTag:String ) {
            val bundle = Bundle()
            bundle.putString("mGpId", mGpId.toString())
            val mPlantStageFragment = ChoosePlantStageFragment()
            mPlantStageFragment.arguments = bundle
            val fragmentManager = fragmentManager
            val fragmentTransaction = fragmentManager!!.beginTransaction()
            fragmentTransaction.add(R.id.mainRL, mPlantStageFragment, "")
            fragmentTransaction.addToBackStack(null)
            fragmentTransaction.commit()
            fragmentManager.executePendingTransactions()
        }
    }

}