package com.stronglifts.view.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.stronglifts.Interface.ClickListenerInterface
import com.stronglifts.R
import com.stronglifts.adapter.AddPlantAdapter
import com.stronglifts.adapter.HomeFragment4Adapter
import com.stronglifts.databinding.FragmentAddPlantBinding
import com.stronglifts.databinding.FragmentHome4Binding

class HomeFragment4 : BaseFragment() {

    // - - Initialize Objects
    private lateinit var binding: FragmentHome4Binding
    private lateinit var mHomeFragment4Adapter: HomeFragment4Adapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentHome4Binding.inflate(inflater, container, false)
        container!!.removeAllViews()
        initView()
        performClick()
        return binding.getRoot()
    }

    private fun initView() {
        hideSoftKeyboard(requireActivity())
        initRecyclerView()
    }

    private fun performClick() {
        binding.txtOtherResourcesTV.setOnClickListener {
            switchFragment(OtherResourcesFragment(), "", false, null)
        }

        binding.chartRL.setOnClickListener {
            switchFragment(ChartFragment(), "", false, null)
        }
    }

    private fun initRecyclerView() {
        val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        binding.homeFragment4RV.layoutManager = layoutManager
        mHomeFragment4Adapter = HomeFragment4Adapter(activity,mInterface)
        binding.homeFragment4RV.adapter = mHomeFragment4Adapter
    }

    private var mInterface = object : ClickListenerInterface {
        override fun mClickListenerInterface(mPos: Int,mTag:String ) {
            switchFragment(VideoDetailFragment(), "", false, null)
        }
    }
}