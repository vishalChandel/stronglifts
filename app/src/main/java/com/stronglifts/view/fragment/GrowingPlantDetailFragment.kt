package com.stronglifts.view.fragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.shrikanthravi.collapsiblecalendarview.data.Day
import com.shrikanthravi.collapsiblecalendarview.view.OnSwipeTouchListener
import com.shrikanthravi.collapsiblecalendarview.widget.CollapsibleCalendar
import com.stronglifts.Interface.*
import com.stronglifts.R
import com.stronglifts.adapter.ActionAdapter
import com.stronglifts.adapter.EnvLogAdapter
import com.stronglifts.adapter.TreeLogAdapter
import com.stronglifts.database.DBHelper
import com.stronglifts.database.DBQuery
import com.stronglifts.databinding.FragmentGrowingPlantDetailBinding
import com.stronglifts.model.ActionModel
import com.stronglifts.model.EnvLogModel
import com.stronglifts.model.TreeLogModel
import com.stronglifts.sharedViewModel.EditGrowingPlantActionSharedViewModel
import com.stronglifts.sharedViewModel.EditGrowingPlantSharedViewModel
import com.stronglifts.sharedViewModel.GrowingPlantDetailSharedViewModel
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class GrowingPlantDetailFragment : BaseFragment() {
    // - - Initialize Objects
    private lateinit var binding: FragmentGrowingPlantDetailBinding
    private lateinit var mActionAdapter: ActionAdapter
    private lateinit var mTreeLogAdapter: TreeLogAdapter
    private lateinit var mEnvLogAdapter: EnvLogAdapter
    private lateinit var mEditGrowingPlantSharedViewModel: EditGrowingPlantSharedViewModel
    private var mActionArraylist: ArrayList<ActionModel>? = null
    private var mTreeLogArraylist: ArrayList<TreeLogModel>? = null
    private var mEnvLogArraylist: ArrayList<EnvLogModel>? = null
    private var dbHandler: DBHelper? = null
    private var mSelectedDate = ""
    private var mGrowingPlantId = ""
    private var mGrowingPlantEnv = ""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentGrowingPlantDetailBinding.inflate(inflater, container, false)
        mEditGrowingPlantSharedViewModel = ViewModelProvider(requireActivity()).get(EditGrowingPlantSharedViewModel::class.java)
        val model = ViewModelProvider(requireActivity()).get(GrowingPlantDetailSharedViewModel::class.java)
        model.getSelected().observe(viewLifecycleOwner) {
            val db = DBQuery(requireContext())
            db.open()

            mActionArraylist = db.getActionListByPlant(convertDateInTimestamp(mSelectedDate).toString(), mGrowingPlantId)

            if (!db.getActionListByRepeatType2(mGrowingPlantId).isNullOrEmpty()) {
                for (i in 0 until db.getActionListByRepeatType2(mGrowingPlantId)!!.size) {
                    val time1 = SimpleDateFormat("dd/MM/yyyy").parse(mSelectedDate)

                    val time2 = SimpleDateFormat("dd/MM/yyyy").parse(convertTimestampInDate(db.getActionListByRepeatType2(mGrowingPlantId)!![i].ac_date))

                    if (time1 >= time2) {
                        if (!mActionArraylist!!.contains(
                                db.getActionListByRepeatType2(
                                    mGrowingPlantId
                                )!![i]
                            )
                        ) {
                            mActionArraylist!!.add(db.getActionListByRepeatType2(mGrowingPlantId)!![i])
                        }
                    }
                }
            }

            for (i in 0 until db.getActionListByRepeatTypeUntilDate2(mGrowingPlantId)!!.size) {
                if (!db.getActionListByRepeatTypeUntilDate2(mGrowingPlantId)!![i].ac_repeat_till.isNullOrEmpty()) {
                    val time1 = SimpleDateFormat("dd/MM/yyyy").parse(mSelectedDate)
                    val time2 = SimpleDateFormat("dd/MM/yyyy").parse(convertTimestampInDate(db.getActionListByRepeatTypeUntilDate2(mGrowingPlantId)!![i].ac_date))
                    val time3 = SimpleDateFormat("dd/MM/yyyy").parse(convertTimestampInDate(db.getActionListByRepeatTypeUntilDate2(mGrowingPlantId)!![i].ac_repeat_till))

                    if (time1 in time2..time3) {
                        if (!mActionArraylist!!.contains(
                                db.getActionListByRepeatTypeUntilDate2(
                                    mGrowingPlantId
                                )!![i]
                            )
                        ) {
                            mActionArraylist!!.add(
                                db.getActionListByRepeatTypeUntilDate2(
                                    mGrowingPlantId
                                )!![i]
                            )
                        }
                    }
                }
            }


            val mActionAllList = db.getActionListAll2(mGrowingPlantId)
            for (i in 0 until mActionAllList!!.size) {
                if (mActionAllList[i].ac_repeat_days != "1" && mActionAllList!![i].ac_repeat_till != "") {
                    var dateInString = convertTimestampInDate(mActionAllList!![i].ac_date)
                    for (j in 0 until 365) {
                        val sdf12 = SimpleDateFormat("dd/MM/yyyy")
                        val time1 = SimpleDateFormat("dd/MM/yyyy").parse(mSelectedDate)
                        val time2 = SimpleDateFormat("dd/MM/yyyy").parse(dateInString)
                        val time3 = SimpleDateFormat("dd/MM/yyyy").parse(convertTimestampInDate(mActionAllList!![i].ac_repeat_till))
                        val c = Calendar.getInstance()
                        c.time = sdf12.parse(dateInString)
                        c.add(Calendar.DATE, mActionAllList!![i].ac_repeat_days.toInt())
                        val resultDate = Date(c.timeInMillis)
                        dateInString = sdf12.format(resultDate)
                        if (time1 == time2) {
                            if (!mActionArraylist!!.contains(mActionAllList!![i])) {
                                mActionArraylist!!.add(mActionAllList!![i])
                            }
                            break
                        }
                        if (time1 > time3) {
                            break
                        }
                    }
                }

            }


            for (i in 0 until mActionAllList!!.size) {
                if (mActionAllList!![i].ac_repeat_days != "1" && mActionAllList!![i].ac_repeat_type == "0") {
                    var dateInString = convertTimestampInDate(mActionAllList!![i].ac_date)
                    for (j in 0 until 365) {
                        val sdf12 = SimpleDateFormat("dd/MM/yyyy")
                        val time1 = SimpleDateFormat("dd/MM/yyyy").parse(mSelectedDate)
                        val time2 = SimpleDateFormat("dd/MM/yyyy").parse(dateInString)
                        val c = Calendar.getInstance()
                        c.time = sdf12.parse(dateInString)
                        if (mActionAllList!![i].ac_repeat_days == "") {
                            c.add(Calendar.DATE, 0)
                        } else {
                            c.add(Calendar.DATE, mActionAllList!![i].ac_repeat_days.toInt())
                        }
                        val resultDate = Date(c.timeInMillis)
                        dateInString = sdf12.format(resultDate)
                        if (time1 == time2) {
                            if (!mActionArraylist!!.contains(mActionAllList!![i])) {
                                mActionArraylist!!.add(mActionAllList!![i])
                            }
                            break
                        }
                    }
                }

            }
            mTreeLogArraylist = db.getTreeLogListByGrowingPlant(convertDateInTimestamp(mSelectedDate).toString(), mGrowingPlantId)
            mEnvLogArraylist = db.getEnvLogListByEnvironment(convertDateInTimestamp(mSelectedDate).toString(), mGrowingPlantEnv)
            db.close()
            initActionRecyclerView()
            initTreeLogRecyclerView()
            initEnvLogRecyclerView()
            try {
                val sdf = SimpleDateFormat("dd/MM/yyyy")
                val d = sdf.parse(mSelectedDate)
                val formatterOut = SimpleDateFormat("EEE, dd MMM")
                try {
                    val nextDate = formatterOut.format(d!!)

                    if (mSelectedDate == SimpleDateFormat(
                            "dd/MM/yyyy",
                            Locale.getDefault()
                        ).format(Date())
                    ) {
                        if (mActionArraylist.isNullOrEmpty()) {
                            binding.actionsDateTV.text = "No Actions for today $nextDate"
                        } else {
                            binding.actionsDateTV.text = "Actions for today $nextDate"
                        }
                    } else {
                        if (mActionArraylist.isNullOrEmpty()) {
                            binding.actionsDateTV.text = "No Actions for $nextDate"
                        } else {
                            binding.actionsDateTV.text = "Actions for $nextDate"
                        }
                    }
                } catch (e: ParseException) {
                    e.printStackTrace()
                }
            } catch (ex: ParseException) {
            }
        }
        val model1 = ViewModelProvider(requireActivity()).get(EditGrowingPlantActionSharedViewModel::class.java)
        model1.getSelected().observe(viewLifecycleOwner) {

            val db = DBQuery(requireContext())
            db.open()

            mActionArraylist = db.getActionListByPlant(convertDateInTimestamp(mSelectedDate).toString(), mGrowingPlantId)

            if (!db.getActionListByRepeatType2(mGrowingPlantId).isNullOrEmpty()) {
                for (i in 0 until db.getActionListByRepeatType2(mGrowingPlantId)!!.size) {
                    val time1 = SimpleDateFormat("dd/MM/yyyy").parse(mSelectedDate)

                    val time2 = SimpleDateFormat("dd/MM/yyyy").parse(convertTimestampInDate(db.getActionListByRepeatType2(mGrowingPlantId)!![i].ac_date))

                    if (time1 >= time2) {
                        if (!mActionArraylist!!.contains(
                                db.getActionListByRepeatType2(
                                    mGrowingPlantId
                                )!![i]
                            )
                        ) {
                           mActionArraylist!!.add(db.getActionListByRepeatType2(mGrowingPlantId)!![i])
                        }
                    }
                }
            }

            for (i in 0 until db.getActionListByRepeatTypeUntilDate2(mGrowingPlantId)!!.size) {
                if (!db.getActionListByRepeatTypeUntilDate2(mGrowingPlantId)!![i].ac_repeat_till.isNullOrEmpty()) {
                    val time1 = SimpleDateFormat("dd/MM/yyyy").parse(mSelectedDate)
                    val time2 = SimpleDateFormat("dd/MM/yyyy").parse(convertTimestampInDate(db.getActionListByRepeatTypeUntilDate2(mGrowingPlantId)!![i].ac_date))
                    val time3 = SimpleDateFormat("dd/MM/yyyy").parse(convertTimestampInDate(db.getActionListByRepeatTypeUntilDate2(mGrowingPlantId)!![i].ac_repeat_till))

                    if (time1 in time2..time3) {
                        if (!mActionArraylist!!.contains(
                                db.getActionListByRepeatTypeUntilDate2(
                                    mGrowingPlantId
                                )!![i]
                            )
                        ) {
                            mActionArraylist!!.add(
                                db.getActionListByRepeatTypeUntilDate2(
                                    mGrowingPlantId
                                )!![i]
                            )
                        }
                    }
                }
            }


            val mActionAllList = db.getActionListAll2(mGrowingPlantId)
            for (i in 0 until mActionAllList!!.size) {
                if (mActionAllList[i].ac_repeat_days != "1" && mActionAllList!![i].ac_repeat_till != "") {
                    var dateInString = convertTimestampInDate(mActionAllList!![i].ac_date)
                    for (j in 0 until 365) {
                        val sdf12 = SimpleDateFormat("dd/MM/yyyy")
                        val time1 = SimpleDateFormat("dd/MM/yyyy").parse(mSelectedDate)
                        val time2 = SimpleDateFormat("dd/MM/yyyy").parse(dateInString)
                        val time3 = SimpleDateFormat("dd/MM/yyyy").parse(convertTimestampInDate(mActionAllList!![i].ac_repeat_till))
                        val c = Calendar.getInstance()
                        c.time = sdf12.parse(dateInString)
                        c.add(Calendar.DATE, mActionAllList!![i].ac_repeat_days.toInt())
                        val resultDate = Date(c.timeInMillis)
                        dateInString = sdf12.format(resultDate)
                        if (time1 == time2) {
                            if (!mActionArraylist!!.contains(mActionAllList!![i])) {
                                mActionArraylist!!.add(mActionAllList!![i])
                            }
                            break
                        }
                        if (time1 > time3) {
                            break
                        }
                    }
                }

            }


            for (i in 0 until mActionAllList!!.size) {
                if (mActionAllList!![i].ac_repeat_days != "1" && mActionAllList!![i].ac_repeat_type == "0") {
                    var dateInString = convertTimestampInDate(mActionAllList!![i].ac_date)
                    for (j in 0 until 365) {
                        val sdf12 = SimpleDateFormat("dd/MM/yyyy")
                        val time1 = SimpleDateFormat("dd/MM/yyyy").parse(mSelectedDate)
                        val time2 = SimpleDateFormat("dd/MM/yyyy").parse(dateInString)
                        val c = Calendar.getInstance()
                        c.time = sdf12.parse(dateInString)
                        if (mActionAllList!![i].ac_repeat_days == "") {
                            c.add(Calendar.DATE, 0)
                        } else {
                            c.add(Calendar.DATE, mActionAllList!![i].ac_repeat_days.toInt())
                        }
                        val resultDate = Date(c.timeInMillis)
                        dateInString = sdf12.format(resultDate)
                        if (time1 == time2) {
                            if (!mActionArraylist!!.contains(mActionAllList!![i])) {
                                mActionArraylist!!.add(mActionAllList!![i])
                            }
                            break
                        }
                    }
                }

            }
            mTreeLogArraylist = db.getTreeLogListByGrowingPlant(convertDateInTimestamp(mSelectedDate).toString(), mGrowingPlantId)
            mEnvLogArraylist = db.getEnvLogListByEnvironment(convertDateInTimestamp(mSelectedDate).toString(), mGrowingPlantEnv)
            db.close()
            initActionRecyclerView()
            initTreeLogRecyclerView()
            initEnvLogRecyclerView()
            try {
                val sdf = SimpleDateFormat("dd/MM/yyyy")
                val d = sdf.parse(mSelectedDate)
                val formatterOut = SimpleDateFormat("EEE, dd MMM")
                try {
                    val nextDate = formatterOut.format(d!!)

                    if (mSelectedDate == SimpleDateFormat(
                            "dd/MM/yyyy",
                            Locale.getDefault()
                        ).format(Date())
                    ) {
                        if (mActionArraylist.isNullOrEmpty()) {
                            binding.actionsDateTV.text = "No Actions for today $nextDate"
                        } else {
                            binding.actionsDateTV.text = "Actions for today $nextDate"
                        }
                    } else {
                        if (mActionArraylist.isNullOrEmpty()) {
                            binding.actionsDateTV.text = "No Actions for $nextDate"
                        } else {
                            binding.actionsDateTV.text = "Actions for $nextDate"
                        }
                    }
                } catch (e: ParseException) {
                    e.printStackTrace()
                }
            } catch (ex: ParseException) {
            }
        }
        initView()
        performClick()
        return binding.root
    }

    private fun initView() {
        hideSoftKeyboard(requireActivity())
        getBundleData()
        binding.calenderView.setOnTouchListener(object : OnSwipeTouchListener(requireContext()) {
            override fun onSwipeRight() {
                binding.collapsibleCalendarView.nextMonth()
            }

            override fun onSwipeLeft() {
                binding.collapsibleCalendarView.prevMonth()
            }

            override fun onSwipeTop() {
                if (binding.collapsibleCalendarView.expanded) {
                    binding.collapsibleCalendarView.collapse(400)
                }
            }

            override fun onSwipeBottom() {
                if (!binding.collapsibleCalendarView.expanded) {
                    binding.collapsibleCalendarView.expand(400)
                }
            }
        })

        //To hide or show expand icon
        binding.collapsibleCalendarView.setExpandIconVisible(true)
        val today = GregorianCalendar()
        binding.collapsibleCalendarView.selectedDay = Day(
            today.get(Calendar.YEAR),
            today.get(Calendar.MONTH),
            today.get(Calendar.DAY_OF_MONTH)
        )
        binding.collapsibleCalendarView.setCalendarListener(object :
            CollapsibleCalendar.CalendarListener {

            override fun onDayChanged() {

            }

            override fun onClickListener() {
                if (binding.collapsibleCalendarView.expanded) {
                    binding.collapsibleCalendarView.collapse(400)
                } else {
                    binding.collapsibleCalendarView.expand(400)
                }
            }

            override fun onDaySelect() {
                mActionArraylist!!.clear()
                val mSelectedMonth = binding.collapsibleCalendarView?.selectedDay!!.month + 1
                mSelectedDate =
                    getFormatedString("" + getFormatedString("" + binding.collapsibleCalendarView?.selectedDay!!.day.toString())).toString() + "/" + getFormatedString(
                        "" + getFormatedString("" + mSelectedMonth.toString())
                    ).toString() + "/" + binding.collapsibleCalendarView?.selectedDay!!.year.toString()

                val db = DBQuery(requireContext())
                db.open()

                mActionArraylist = db.getActionListByPlant(convertDateInTimestamp(mSelectedDate).toString(), mGrowingPlantId)

                if (!db.getActionListByRepeatType2(mGrowingPlantId).isNullOrEmpty()) {
                    for (i in 0 until db.getActionListByRepeatType2(mGrowingPlantId)!!.size) {
                        val time1 = SimpleDateFormat("dd/MM/yyyy").parse(mSelectedDate)

                        val time2 = SimpleDateFormat("dd/MM/yyyy").parse(convertTimestampInDate(db.getActionListByRepeatType2(mGrowingPlantId)!![i].ac_date))

                        if (time1 >= time2) {
                            if (!mActionArraylist!!.contains(
                                    db.getActionListByRepeatType2(
                                        mGrowingPlantId
                                    )!![i]
                                )
                            ) {
                               mActionArraylist!!.add(db.getActionListByRepeatType2(mGrowingPlantId)!![i])
                            }
                        }
                    }
                }

                for (i in 0 until db.getActionListByRepeatTypeUntilDate2(mGrowingPlantId)!!.size) {
                    if (!db.getActionListByRepeatTypeUntilDate2(mGrowingPlantId)!![i].ac_repeat_till.isNullOrEmpty()) {
                        val time1 = SimpleDateFormat("dd/MM/yyyy").parse(mSelectedDate)
                        val time2 = SimpleDateFormat("dd/MM/yyyy").parse(convertTimestampInDate(db.getActionListByRepeatTypeUntilDate2(mGrowingPlantId)!![i].ac_date))
                        val time3 = SimpleDateFormat("dd/MM/yyyy").parse(convertTimestampInDate(db.getActionListByRepeatTypeUntilDate2(mGrowingPlantId)!![i].ac_repeat_till))

                        if (time1 in time2..time3) {
                            if (!mActionArraylist!!.contains(
                                    db.getActionListByRepeatTypeUntilDate2(
                                        mGrowingPlantId
                                    )!![i]
                                )
                            ) {
                                mActionArraylist!!.add(
                                    db.getActionListByRepeatTypeUntilDate2(
                                        mGrowingPlantId
                                    )!![i]
                                )
                            }
                        }
                    }
                }


                val mActionAllList = db.getActionListAll2(mGrowingPlantId)
                for (i in 0 until mActionAllList!!.size) {
                    if (mActionAllList[i].ac_repeat_days != "1" && mActionAllList!![i].ac_repeat_till != "") {
                        var dateInString = convertTimestampInDate(mActionAllList!![i].ac_date)
                        for (j in 0 until 365) {
                            val sdf12 = SimpleDateFormat("dd/MM/yyyy")
                            val time1 = SimpleDateFormat("dd/MM/yyyy").parse(mSelectedDate)
                            val time2 = SimpleDateFormat("dd/MM/yyyy").parse(dateInString)
                            val time3 = SimpleDateFormat("dd/MM/yyyy").parse(convertTimestampInDate(mActionAllList!![i].ac_repeat_till))
                            val c = Calendar.getInstance()
                            c.time = sdf12.parse(dateInString)
                            c.add(Calendar.DATE, mActionAllList!![i].ac_repeat_days.toInt())
                            val resultDate = Date(c.timeInMillis)
                            dateInString = sdf12.format(resultDate)
                            if (time1 == time2) {
                                if (!mActionArraylist!!.contains(mActionAllList!![i])) {
                                    mActionArraylist!!.add(mActionAllList!![i])
                                }
                                break
                            }
                            if (time1 > time3) {
                                break
                            }
                        }
                    }

                }


                for (i in 0 until mActionAllList!!.size) {
                    if (mActionAllList!![i].ac_repeat_days != "1" && mActionAllList!![i].ac_repeat_type == "0") {
                        var dateInString = convertTimestampInDate(mActionAllList!![i].ac_date)
                        for (j in 0 until 365) {
                            val sdf12 = SimpleDateFormat("dd/MM/yyyy")
                            val time1 = SimpleDateFormat("dd/MM/yyyy").parse(mSelectedDate)
                            val time2 = SimpleDateFormat("dd/MM/yyyy").parse(dateInString)
                            val c = Calendar.getInstance()
                            c.time = sdf12.parse(dateInString)
                            if (mActionAllList!![i].ac_repeat_days == "") {
                                c.add(Calendar.DATE, 0)
                            } else {
                                c.add(Calendar.DATE, mActionAllList!![i].ac_repeat_days.toInt())
                            }
                            val resultDate = Date(c.timeInMillis)
                            dateInString = sdf12.format(resultDate)
                            if (time1 == time2) {
                                if (!mActionArraylist!!.contains(mActionAllList!![i])) {
                                    mActionArraylist!!.add(mActionAllList!![i])
                                }
                                break
                            }
                        }
                    }

                }
                mTreeLogArraylist = db.getTreeLogListByGrowingPlant(convertDateInTimestamp(mSelectedDate).toString(), mGrowingPlantId)
                mEnvLogArraylist = db.getEnvLogListByEnvironment(convertDateInTimestamp(mSelectedDate).toString(), mGrowingPlantEnv)
                db.close()
                initActionRecyclerView()
                initTreeLogRecyclerView()
                initEnvLogRecyclerView()
                try {
                    val sdf = SimpleDateFormat("dd/MM/yyyy")
                    val d = sdf.parse(mSelectedDate)
                    val formatterOut = SimpleDateFormat("EEE, dd MMM")
                    try {
                        val nextDate = formatterOut.format(d!!)

                        if (mSelectedDate == SimpleDateFormat(
                                "dd/MM/yyyy",
                                Locale.getDefault()
                            ).format(Date())
                        ) {
                            if (mActionArraylist.isNullOrEmpty()) {
                                binding.actionsDateTV.text = "No Actions for today $nextDate"
                            } else {
                                binding.actionsDateTV.text = "Actions for today $nextDate"
                            }
                        } else {
                            if (mActionArraylist.isNullOrEmpty()) {
                                binding.actionsDateTV.text = "No Actions for $nextDate"
                            } else {
                                binding.actionsDateTV.text = "Actions for $nextDate"
                            }
                        }
                    } catch (e: ParseException) {
                        e.printStackTrace()
                    }
                } catch (ex: ParseException) {
                }
            }

            override fun onItemClick(v: View) {

            }

            override fun onDataUpdate() {

            }

            override fun onMonthChange() {

            }

            override fun onWeekChange(position: Int) {

            }
        })
        mSelectedDate = SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()).format(Date())

        val db = DBQuery(requireContext())
        db.open()

        mActionArraylist = db.getActionListByPlant(convertDateInTimestamp(mSelectedDate).toString(), mGrowingPlantId)

        if (!db.getActionListByRepeatType2(mGrowingPlantId).isNullOrEmpty()) {
            for (i in 0 until db.getActionListByRepeatType2(mGrowingPlantId)!!.size) {
                val time1 = SimpleDateFormat("dd/MM/yyyy").parse(mSelectedDate)

                val time2 = SimpleDateFormat("dd/MM/yyyy").parse(convertTimestampInDate(db.getActionListByRepeatType2(mGrowingPlantId)!![i].ac_date))

                if (time1 >= time2) {
                    if (!mActionArraylist!!.contains(
                            db.getActionListByRepeatType2(
                                mGrowingPlantId
                            )!![i]
                        )
                    ) {
                       mActionArraylist!!.add(db.getActionListByRepeatType2(mGrowingPlantId)!![i])
                    }
                }
            }
        }

        for (i in 0 until db.getActionListByRepeatTypeUntilDate2(mGrowingPlantId)!!.size) {
            if (!db.getActionListByRepeatTypeUntilDate2(mGrowingPlantId)!![i].ac_repeat_till.isNullOrEmpty()) {
                val time1 = SimpleDateFormat("dd/MM/yyyy").parse(mSelectedDate)
                val time2 = SimpleDateFormat("dd/MM/yyyy").parse(convertTimestampInDate(db.getActionListByRepeatTypeUntilDate2(mGrowingPlantId)!![i].ac_date))
                val time3 = SimpleDateFormat("dd/MM/yyyy").parse(convertTimestampInDate(db.getActionListByRepeatTypeUntilDate2(mGrowingPlantId)!![i].ac_repeat_till))

                if (time1 in time2..time3) {
                    if (!mActionArraylist!!.contains(
                            db.getActionListByRepeatTypeUntilDate2(
                                mGrowingPlantId
                            )!![i]
                        )
                    ) {
                        mActionArraylist!!.add(
                            db.getActionListByRepeatTypeUntilDate2(
                                mGrowingPlantId
                            )!![i]
                        )
                    }
                }
            }
        }


        val mActionAllList = db.getActionListAll2(mGrowingPlantId)
        for (i in 0 until mActionAllList!!.size) {
            if (mActionAllList[i].ac_repeat_days != "1" && mActionAllList!![i].ac_repeat_till != "") {
                var dateInString = convertTimestampInDate(mActionAllList!![i].ac_date)
                for (j in 0 until 365) {
                    val sdf12 = SimpleDateFormat("dd/MM/yyyy")
                    val time1 = SimpleDateFormat("dd/MM/yyyy").parse(mSelectedDate)
                    val time2 = SimpleDateFormat("dd/MM/yyyy").parse(dateInString)
                    val time3 = SimpleDateFormat("dd/MM/yyyy").parse(convertTimestampInDate(mActionAllList!![i].ac_repeat_till))
                    val c = Calendar.getInstance()
                    c.time = sdf12.parse(dateInString)
                    c.add(Calendar.DATE, mActionAllList!![i].ac_repeat_days.toInt())
                    val resultDate = Date(c.timeInMillis)
                    dateInString = sdf12.format(resultDate)
                    if (time1 == time2) {
                        if (!mActionArraylist!!.contains(mActionAllList!![i])) {
                            mActionArraylist!!.add(mActionAllList!![i])
                        }
                        break
                    }
                    if (time1 > time3) {
                        break
                    }
                }
            }

        }


        for (i in 0 until mActionAllList!!.size) {
            if (mActionAllList!![i].ac_repeat_days != "1" && mActionAllList!![i].ac_repeat_type == "0") {
                var dateInString = convertTimestampInDate(mActionAllList!![i].ac_date)
                for (j in 0 until 365) {
                    val sdf12 = SimpleDateFormat("dd/MM/yyyy")
                    val time1 = SimpleDateFormat("dd/MM/yyyy").parse(mSelectedDate)
                    val time2 = SimpleDateFormat("dd/MM/yyyy").parse(dateInString)
                    val c = Calendar.getInstance()
                    c.time = sdf12.parse(dateInString)
                    if (mActionAllList!![i].ac_repeat_days == "") {
                        c.add(Calendar.DATE, 0)
                    } else {
                        c.add(Calendar.DATE, mActionAllList!![i].ac_repeat_days.toInt())
                    }
                    val resultDate = Date(c.timeInMillis)
                    dateInString = sdf12.format(resultDate)
                    if (time1 == time2) {
                        if (!mActionArraylist!!.contains(mActionAllList!![i])) {
                            mActionArraylist!!.add(mActionAllList!![i])
                        }
                        break
                    }
                }
            }

        }
        mTreeLogArraylist = db.getTreeLogListByGrowingPlant(convertDateInTimestamp(mSelectedDate).toString(), mGrowingPlantId)
        mEnvLogArraylist = db.getEnvLogListByEnvironment(convertDateInTimestamp(mSelectedDate).toString(), mGrowingPlantEnv)
        db.close()
        initActionRecyclerView()
        initTreeLogRecyclerView()
        initEnvLogRecyclerView()
        val sdf = SimpleDateFormat("EEE, dd MMM")
        val d = Date()
        val dayOfTheWeek = sdf.format(d)
        if (mActionArraylist.isNullOrEmpty()) {
            binding.actionsDateTV.text = "No Actions for today $dayOfTheWeek"
        } else {
            binding.actionsDateTV.text = "Actions for today $dayOfTheWeek"
        }
    }

    private fun getBundleData() {
        if (arguments != null) {
            val bundle = arguments
            if (bundle!!.getString("growingPlantAltName").toString()
                    .isNullOrEmpty() || bundle!!.getString("growingPlantAltName")
                    .toString() == "null"
            ) {
                binding.plantNameTV.text = bundle!!.getString("growingPlantName")
            } else {
                binding.plantNameTV.text =
                    bundle!!.getString("growingPlantName") + "(" + bundle!!.getString("growingPlantAltName") + ")"
            }
            mGrowingPlantId = bundle!!.getString("growingPlantId").toString()
            mGrowingPlantEnv = bundle!!.getString("growingPlantEnv").toString()
        }
    }

    private fun performClick() {

        binding.actionLL.setOnClickListener {
            val bundle = Bundle()
            bundle.putString("FragmentTag", "GrowingPlantDetailFragment")
            bundle.putString("mTag", "AddAction")
            bundle.putString("mPlantId", mGrowingPlantId)
            val mFragment = AddActionFragment()
            mFragment.arguments = bundle
            val fragmentManager = childFragmentManager
            val fragmentTransaction = fragmentManager.beginTransaction()
            fragmentTransaction.add(R.id.mainRL, mFragment, "")
            fragmentTransaction.addToBackStack(null)
            fragmentTransaction.commit()
            fragmentManager.executePendingTransactions()
        }

        binding.TreeLogLL.setOnClickListener {
            val bundle = Bundle()
            bundle.putString("FragmentTag", "GrowingPlantDetailFragment")
            bundle.putString("mTag", "AddTreeLog")
            bundle.putString("mPlantId", mGrowingPlantId)
            val mFragment = AddTreeLogFragment()
            mFragment.arguments = bundle
            val fragmentManager = childFragmentManager
            val fragmentTransaction = fragmentManager.beginTransaction()
            fragmentTransaction.add(R.id.mainRL, mFragment, "")
            fragmentTransaction.addToBackStack(null)
            fragmentTransaction.commit()
            fragmentManager.executePendingTransactions()
        }

        binding.backRL.setOnClickListener {
            dbHandler = DBHelper(requireActivity())
            mSelectedDate = SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()).format(Date())
            val db = DBQuery(requireContext())
            db.open()
            val mActionAllListByCurrentDate = db.getActionListAllByCurrentDate(mSelectedDate)
            if (mActionAllListByCurrentDate!!.isNotEmpty()) {
                for (i in 0 until mActionAllListByCurrentDate!!.size) {
                    dbHandler!!.updateHarvestingPlant(
                        mActionAllListByCurrentDate!![i].ac_plant_id, "2")
                }
            }
            val mGrowingPlants = db.getGrowingPlantsList()
            db.close()
            mEditGrowingPlantSharedViewModel.setSelected(mGrowingPlants!!)
            val fm: FragmentManager? = fragmentManager
            fm!!.popBackStack()
            hideSoftKeyboard(requireActivity())
        }
    }

    private fun initActionRecyclerView() {
        val layoutManager: RecyclerView.LayoutManager =
            LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        binding.actionRV.layoutManager = layoutManager
        mActionArraylist!!.toSet().toList()
        mActionAdapter = ActionAdapter(
            activity,
            mActionArraylist,
            mEditActionClickListenerInterface,
            mDoneActionClickListenerInterface,
            mSelectedDate
        )
        binding.actionRV.adapter = mActionAdapter
    }

    private fun initTreeLogRecyclerView() {
        val layoutManager: RecyclerView.LayoutManager =
            LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        binding.treeLogRV.layoutManager = layoutManager
        mTreeLogAdapter = TreeLogAdapter(
            activity,
            mTreeLogArraylist,
            mEditTreeLogClickListenerInterface,
            mDeleteTreeLogClickListenerInterface
        )
        binding.treeLogRV.adapter = mTreeLogAdapter
    }

    private fun initEnvLogRecyclerView() {
        val layoutManager: RecyclerView.LayoutManager =
            LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        binding.envLogRV.layoutManager = layoutManager
        mEnvLogAdapter = EnvLogAdapter(
            activity,
            mEnvLogArraylist,
            mEditEnvLogClickListenerInterface,
            mDeleteEnvLogClickListenerInterface
        )
        binding.envLogRV.adapter = mEnvLogAdapter
    }


    private var mEditTreeLogClickListenerInterface = object : EditTreeLogClickListenerInterface {
        override fun mClickListenerInterface(mModel: TreeLogModel) {
            val bundle = Bundle()
            bundle.putString("mTreeLogId", mModel.tl_id)
            bundle.putString("mPlantId", mModel.plant_id)
            bundle.putString("mDate", mModel.log_date)
            bundle.putString("mDesc", mModel.log_desc)
            bundle.putString("FragmentTag", "GrowingPlantDetailFragment")
            val mFragment = AddTreeLogFragment()
            mFragment.arguments = bundle
            val fragmentManager = childFragmentManager
            val fragmentTransaction = fragmentManager.beginTransaction()
            fragmentTransaction.add(R.id.mainRL, mFragment, "")
            fragmentTransaction.addToBackStack(null)
            fragmentTransaction.commit()
            fragmentManager.executePendingTransactions()
        }
    }

    private var mEditEnvLogClickListenerInterface = object : EditEnvLogClickListenerInterface {
        override fun mClickListenerInterface(mModel: EnvLogModel) {
            val bundle = Bundle()
            bundle.putString("mEnvLogId", mModel.ev_id)
            bundle.putString("mEnv", mModel.env_name)
            bundle.putString("mDate", mModel.env_log_date)
            bundle.putString("mDesc", mModel.env_log_desc)
            bundle.putString("FragmentTag", "GrowingPlantDetailFragment")
            val mFragment = AddEnvironmentLogFragment()
            mFragment.arguments = bundle
            val fragmentManager = childFragmentManager
            val fragmentTransaction = fragmentManager.beginTransaction()
            fragmentTransaction.add(R.id.mainRL, mFragment, "")
            fragmentTransaction.addToBackStack(null)
            fragmentTransaction.commit()
            fragmentManager.executePendingTransactions()
        }
    }

    private var mDeleteTreeLogClickListenerInterface =
        object : DeleteTreeLogClickListenerInterface {
            override fun mClickListenerInterface(mModel: TreeLogModel) {
                dbHandler = DBHelper(requireActivity()!!)
                dbHandler!!.deleteTreeLog(mModel.tl_id)
                val db = DBQuery(requireContext())
                db.open()
                mTreeLogArraylist = db.getTreeLogListByGrowingPlant(mSelectedDate, mGrowingPlantId)
                mEnvLogArraylist = db.getEnvLogListByEnvironment(mSelectedDate, mGrowingPlantEnv)
                db.close()
                initTreeLogRecyclerView()
                initEnvLogRecyclerView()
            }
        }

    private var mDeleteEnvLogClickListenerInterface = object : DeleteEnvLogClickListenerInterface {
        override fun mClickListenerInterface(mModel: EnvLogModel) {
            dbHandler = DBHelper(requireActivity()!!)
            dbHandler!!.deleteEnvLog(mModel.ev_id)
            val db = DBQuery(requireContext())
            db.open()
            mTreeLogArraylist = db.getTreeLogListByGrowingPlant(mSelectedDate, mGrowingPlantId)
            mEnvLogArraylist = db.getEnvLogListByEnvironment(mSelectedDate, mGrowingPlantEnv)
            db.close()
            initTreeLogRecyclerView()
            initEnvLogRecyclerView()
        }
    }

    private var mEditActionClickListenerInterface = object : EditActionClickListenerInterface {
        override fun mClickListenerInterface(mModel: ActionModel) {
            val bundle = Bundle()
            bundle.putString("FragmentTag", "GrowingPlantDetailFragmentEdit")
            bundle.putString("mActionTag", "Edit")
            bundle.putParcelable("actionModel", mModel)
            val mFragment = AddActionFragment()
            mFragment.arguments = bundle
            val fragmentManager = childFragmentManager
            val fragmentTransaction = fragmentManager.beginTransaction()
            fragmentTransaction.add(R.id.mainRL, mFragment, "")
            fragmentTransaction.addToBackStack(null)
            fragmentTransaction.commit()
            fragmentManager.executePendingTransactions()
        }
    }

    private var mDoneActionClickListenerInterface = object : DoneActionClickListenerInterface {
        override fun mClickListenerInterface(mModel: ActionModel) {
            val db = DBQuery(requireActivity())
            db.open()
            val mActionStatusList = db.getActionStatus(mModel.ac_id, mSelectedDate)
            db.close()

            if (!mActionStatusList.isNullOrEmpty()) {
                for (i in 0 until mActionStatusList!!.size) {
                    if (mActionStatusList!![i].ac_id == mModel.ac_id && mActionStatusList!![i].ac_status_date == mSelectedDate) {
                        dbHandler = DBHelper(requireActivity()!!)
                        dbHandler!!.deleteActionStatus(mModel.ac_id, mSelectedDate)
                    } else {
                        dbHandler = DBHelper(requireActivity()!!)
                        dbHandler!!.addActionStatus(mModel.ac_id, mSelectedDate)
                    }
                }
            } else {
                dbHandler = DBHelper(requireActivity()!!)
                dbHandler!!.addActionStatus(mModel.ac_id, mSelectedDate)
            }
            initActionRecyclerView()
        }
    }
}