package com.stronglifts.view.fragment

import android.app.Activity
import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.*
import androidx.fragment.app.FragmentManager
import com.stronglifts.R
import com.stronglifts.database.DBHelper
import com.stronglifts.databinding.FragmentChooseEnvironmentBinding
import java.text.SimpleDateFormat
import java.util.*

class ChooseEnvironmentFragment : BaseFragment() {

    // - - Initialize Objects
    private lateinit var binding: FragmentChooseEnvironmentBinding
    private var dbHandler: DBHelper? = null
    private var mGpId = ""
    private var mGpStage = ""
    private var mGpEnvironment = ""
    private var mCurrentDate = ""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentChooseEnvironmentBinding.inflate(inflater, container, false)
        initView()
        onViewClicked()
        return binding.root
    }

    private fun initView() {
        hideSoftKeyboard(requireActivity())
        dbHandler = DBHelper(requireContext())
        val bundle = arguments
        if (bundle != null) {
            mGpId = bundle.getString("mGpId").toString()
            mGpStage = bundle.getString("mGpStage").toString()
        }
    }

    private fun onViewClicked() {
        binding.backRL.setOnClickListener {
            val fm: FragmentManager? = fragmentManager
            fm!!.popBackStack()
        }

        binding.indoorRL.setOnClickListener {
            mGpEnvironment = "Indoor"
            showSelectDateAlertDialog(requireActivity())
        }

        binding.outdoorRL.setOnClickListener {
            mGpEnvironment = "Outdoor"
            showSelectDateAlertDialog(requireActivity())

        }

    }


    // - - To Show Select Date Dialog
    private fun showSelectDateAlertDialog(mActivity: Activity?) {
        val alertDialog = Dialog(mActivity!!)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.dialog_add_plant_creation_date)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set the custom dialog components - text, image and button
        val txtAddTV = alertDialog.findViewById<TextView>(R.id.txtAddTV)
        val selectDateTV = alertDialog.findViewById<TextView>(R.id.selectDateTV)

        selectDateTV.setOnClickListener {
            datePicker(selectDateTV)
        }

        txtAddTV.setOnClickListener {
           if(selectDateTV.text.trim().isEmpty()){
               showAlertDialog(mActivity,getString(R.string.please_select_date))
           }else {
               mCurrentDate = selectDateTV.text.toString()
               dbHandler!!.addGrowingPlant(mGpId, "1", mGpStage, mGpEnvironment, convertDateInTimestamp(mCurrentDate).toString())
               alertDialog.dismiss()
               switchFragment(HomeFragment2(), "", false, null)
           }
       }
        alertDialog.show()
    }
}