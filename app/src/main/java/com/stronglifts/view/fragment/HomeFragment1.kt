package com.stronglifts.view.fragment

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.TextView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.goserve.utils.AppPreference
import com.goserve.utils.IS_CROSSING_TAB
import com.stronglifts.Interface.ClickListenerInterfacewithData
import com.stronglifts.Interface.FilterClickListenerInterface
import com.stronglifts.Interface.LongClickListenerInterface
import com.stronglifts.R
import com.stronglifts.adapter.FilterAdapter
import com.stronglifts.adapter.HomeFragment1Adapter
import com.stronglifts.database.DBHelper
import com.stronglifts.database.DBQuery
import com.stronglifts.databinding.FragmentHome1Binding
import com.stronglifts.model.PlantsModel

class HomeFragment1 : BaseFragment() {

    // - - Initialize Objects
    private lateinit var binding: FragmentHome1Binding
    private lateinit var mHomeFragment1Adpater: HomeFragment1Adapter
    private lateinit var mFilterAdapter: FilterAdapter
    private var mPlantsList: ArrayList<PlantsModel>? = null
    private var mPlantsCategoryList: ArrayList<String>? = null
    private var dbHandler: DBHelper? = null
    private var isLongPressed = false
    private var mSelectedCategory="All"

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentHome1Binding.inflate(inflater, container, false)
        container!!.removeAllViews()
        performClick()
        return binding.root
    }

    private fun initView() {
        hideSoftKeyboard(requireActivity())
        AppPreference().writeBoolean(requireContext(), IS_CROSSING_TAB, false)
        setEditTextFocused(binding.edtSearchET)
        binding.edtSearchET.setText("")
        val db = DBQuery(requireContext())
        db.open()
        mPlantsList = db.getAddedPlantsList()
        if(mPlantsList.isNullOrEmpty()){
            binding.txtDoneTV.visibility=View.GONE
        }
        mPlantsCategoryList = db.getPlantsCategoryList()
        mPlantsCategoryList!!.add(0, "All")
        // Create a new LinkedHashSet
        val set: MutableSet<String> = LinkedHashSet()
        // Add the elements to set
        set.addAll(mPlantsCategoryList!!)
        mPlantsCategoryList!!.clear()
        mPlantsCategoryList!!.addAll(set)
        db.close()
        initRecyclerView()
        setFilterAdapter()
        binding.edtSearchET.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                // TODO Auto-generated method stub
            }

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
                // TODO Auto-generated method stub
            }

            override fun afterTextChanged(s: Editable) {
                // filter your list from your input
                filter(s.toString())
            }
        })
    }

    private fun performClick() {

        binding.addRL.setOnClickListener {
            val bundle = Bundle()
            bundle.putString("FragmentTag", "HomeFragment1")
            val mAddPlantFragment = AddPlantFragment()
            mAddPlantFragment.arguments = bundle
            val fragmentManager = childFragmentManager
            val fragmentTransaction = fragmentManager.beginTransaction()
            fragmentTransaction.add(R.id.mainRL, mAddPlantFragment, "")
            fragmentTransaction.addToBackStack(null)
            fragmentTransaction.commit()
            fragmentManager.executePendingTransactions()
        }

        binding.txtDoneTV.setOnClickListener {
            if(isLongPressed){
                isLongPressed=false
                binding.txtDoneTV.text="Edit"
            }else if(!isLongPressed) {
                binding.txtDoneTV.text="Done"
                isLongPressed = true
            }
            initRecyclerView()
        }

    }

    private fun initRecyclerView() {
        val layoutManager = GridLayoutManager(activity, 2)
        binding.homeFragment1RV.layoutManager = layoutManager
        mHomeFragment1Adpater = HomeFragment1Adapter(
            activity,
            mPlantsList,
            mInterface,
            mLongClickInterface,
            isLongPressed
        )
        binding.homeFragment1RV.adapter = mHomeFragment1Adpater
    }

    private fun setFilterAdapter() {
        val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        binding.filterRV.layoutManager = layoutManager
        mFilterAdapter = FilterAdapter(
            activity,
            mPlantsCategoryList,
            mFilterClickListenerInterface,
            "HomeFragment1")
        binding.filterRV.adapter = mFilterAdapter
    }

    fun filter(text: String?) {
        val temp: ArrayList<PlantsModel> = ArrayList()
        for (d in mPlantsList!!) {
            if (d.name.toLowerCase().contains(text!!.toLowerCase().toString())) {
                temp.add(d)
            }
        }
        //update recyclerview
        mHomeFragment1Adpater.updateList(temp)
    }

    private var mInterface = object : ClickListenerInterfacewithData {
        override fun mClickListenerInterface(
            mPos: Int,
            mTag: String,
            mPlantsList: ArrayList<PlantsModel>?
        ) {
            hideSoftKeyboard(requireActivity())
            setEditTextFocused(binding.edtSearchET)
            binding.edtSearchET.setText("")
            val bundle = Bundle()
            bundle.putParcelableArrayList("mPlantsList", mPlantsList)
            bundle.putInt("mPos", mPos)
            bundle.putString("mTag", mTag)
            val plantFrag = PlantDetailFragment()
            plantFrag.arguments = bundle
            val fragmentManager = childFragmentManager
            val fragmentTransaction = fragmentManager.beginTransaction()
            fragmentTransaction.add(R.id.mainRL, plantFrag, "")
            fragmentTransaction.addToBackStack(null)
            fragmentTransaction.commit()
            fragmentManager.executePendingTransactions()
        }
    }

    private var mLongClickInterface = object : LongClickListenerInterface {
        override fun mClickListenerInterface(
            mItemId: String,
            mTag: String,
        ) {
            hideSoftKeyboard(requireActivity())
            setEditTextFocused(binding.edtSearchET)
            binding.edtSearchET.setText("")
            showDeleteConfirmAlertDialog(mItemId)
        }
    }

    private var mFilterClickListenerInterface = object : FilterClickListenerInterface {
        override fun mClickListenerInterface(
            mCategory: String,
            mTag: String,
        ) {
            mSelectedCategory=mCategory
            hideSoftKeyboard(requireActivity())
            setEditTextFocused(binding.edtSearchET)
            binding.edtSearchET.setText("")
            val db = DBQuery(requireContext())
            db.open()
            mPlantsList!!.clear()
            mPlantsList = when (mCategory) {
                "All" -> {
                    db.getAddedPlantsList()
                }
                else -> {
                    db.getAddedPlantsListByCategory(mCategory)
                }
            }
            db.close()
            initRecyclerView()
        }
    }

    // - - Delete  Alert Dialog
    private fun showDeleteConfirmAlertDialog(mItemId: String) {
        val alertDialog = Dialog(requireActivity()!!)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.dialog_logout)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set the custom dialog components - text, image and button
        val btnYes = alertDialog.findViewById<TextView>(R.id.btnYes)
        val btnNo = alertDialog.findViewById<TextView>(R.id.btnNo)

        btnNo.setOnClickListener {
            alertDialog.dismiss()
        }

        btnYes.setOnClickListener {
            dbHandler = DBHelper(requireActivity()!!)
            dbHandler!!.deletePlantFromPlantList(mItemId, "0")
            val db = DBQuery(requireContext())
            db.open()
            mPlantsList!!.clear()
            mPlantsList = when (mSelectedCategory) {
                "All" -> {
                    db.getAddedPlantsList()
                }
                else -> {
                    db.getAddedPlantsListByCategory(mSelectedCategory)
                }
            }
            if(mPlantsList.isNullOrEmpty()){
                binding.txtDoneTV.visibility=View.GONE
            }
            db.close()
            initRecyclerView()
            alertDialog.dismiss()
        }
        alertDialog.show()
    }

    override fun onResume() {
        super.onResume()
        initView()
    }
}