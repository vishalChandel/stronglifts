package com.stronglifts.view.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.stronglifts.Interface.ClickListenerInterface
import com.stronglifts.Interface.ClickListenerInterfacewithGrowingPlantsData
import com.stronglifts.R
import com.stronglifts.adapter.GrowingPlantsParentAdapter
import com.stronglifts.database.DBHelper
import com.stronglifts.database.DBQuery
import com.stronglifts.databinding.FragmentGrowingPlantsBinding
import com.stronglifts.model.GrowingPlantsModel
import com.stronglifts.sharedViewModel.EditGrowingPlantSharedViewModel
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.LinkedHashSet

class GrowingPlantsFragment : BaseFragment() {

    // - - Initialize Objects
    private lateinit var binding: FragmentGrowingPlantsBinding
    private lateinit var mGrowingPlantsAdapter: GrowingPlantsParentAdapter
    private var mGrowingPlants: ArrayList<GrowingPlantsModel>? = null
    private val mGrowingPlantStages = ArrayList<String?>()
    private var dbHandler: DBHelper? = null
    private var mSelectedDate = ""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentGrowingPlantsBinding.inflate(inflater, container, false)
        val model = ViewModelProvider(requireActivity()).get(EditGrowingPlantSharedViewModel::class.java)
        model.getSelected().observe(viewLifecycleOwner, Observer {
            initView()
        })
        initView()
        return binding.root
    }

    private fun initView() {
        dbHandler = DBHelper(requireActivity())
        updateData()
        mSelectedDate = SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()).format(Date())
        hideSoftKeyboard(requireActivity())
        val db = DBQuery(requireContext())
        db.open()
        mGrowingPlants = db.getGrowingPlantsList()
        db.close()
        if (!mGrowingPlants.isNullOrEmpty()) {
            mGrowingPlantStages.clear()
            for (i in 0 until mGrowingPlants!!.size) {
                mGrowingPlantStages.add(mGrowingPlants!![i].plant_stage.toString())
            }
            val set: MutableSet<String?> = LinkedHashSet()
            set.addAll(mGrowingPlantStages)
            mGrowingPlantStages.clear()
            mGrowingPlantStages.addAll(set)
            binding.growingPlantsParentRV.visibility=View.VISIBLE
            initRecyclerView()
        }else{
            binding.growingPlantsParentRV.visibility=View.GONE
        }
    }

    private fun updateData() {
        dbHandler = DBHelper(requireActivity())
        mSelectedDate = SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()).format(Date())
        val db = DBQuery(requireContext())
        db.open()
        val mActionAllListByCurrentDate = db.getActionListAllByCurrentDate(convertDateInTimestamp(mSelectedDate).toString())
        if (mActionAllListByCurrentDate!!.isNotEmpty()) {
            for (i in 0 until mActionAllListByCurrentDate!!.size) {
                dbHandler!!.updateHarvestingPlant(mActionAllListByCurrentDate!![i].ac_plant_id, "2")
            }
        }
        val mChangeStateActionList = db.getChangeStateActionList(convertDateInTimestamp(mSelectedDate).toString())
        if (mChangeStateActionList!!.isNotEmpty()) {
            for (i in 0 until mChangeStateActionList.size) {
                var mUpdatedStage = ""
                when (mChangeStateActionList[i].ac_type_method) {
                    "0" -> {
                        mUpdatedStage = "Flowering"
                    }
                    "1" -> {
                        mUpdatedStage = "Germination"
                    }
                    "2" -> {
                        mUpdatedStage = "Seedling"
                    }
                    "3" -> {
                        mUpdatedStage = "Vegetative"
                    }
                }
                dbHandler!!.updateGrowingPlantState(mChangeStateActionList!![i].ac_plant_id, mUpdatedStage)
            }
        }
        db.close()
    }

    private fun initRecyclerView() {
        val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        binding.growingPlantsParentRV.layoutManager = layoutManager
        mGrowingPlantsAdapter = GrowingPlantsParentAdapter(
            activity,
            mGrowingPlantStages,
            mGrowingPlants,
            mInterface,
            mInterfaceWithData
        )
        binding.growingPlantsParentRV.adapter = mGrowingPlantsAdapter
    }

    private var mInterface = object : ClickListenerInterface {
        override fun mClickListenerInterface(mGpId: Int, mTag: String) {
            val bundle = Bundle()
            bundle.putString("mGpId", mGpId.toString())
            val mPlantStageFragment = ChoosePlantStageFragment()
            mPlantStageFragment.arguments = bundle
            val fragmentManager = fragmentManager
            val fragmentTransaction = fragmentManager!!.beginTransaction()
            fragmentTransaction.add(R.id.mainRL, mPlantStageFragment, "")
            fragmentTransaction.addToBackStack(null)
            fragmentTransaction.commit()
            fragmentManager.executePendingTransactions()
        }
    }

    private var mInterfaceWithData = object : ClickListenerInterfacewithGrowingPlantsData {
        override fun mClickListenerInterface(
            mPos: Int,
            mTag: String,
            mGrowingPlantsList: ArrayList<GrowingPlantsModel>?
        ) {
            if(mTag=="EditGrowingPlantFragment") {
                hideSoftKeyboard(requireActivity())
                val bundle = Bundle()
                bundle.putParcelableArrayList("mGrowingPlantsList", mGrowingPlantsList)
                bundle.putInt("mPos", mPos)
                bundle.putString("mTag", mTag)
                val plantFrag = EditGrowingPlantFragment()
                plantFrag.arguments = bundle
                val fragmentManager = fragmentManager
                val fragmentTransaction = fragmentManager!!.beginTransaction()
                fragmentTransaction.add(R.id.secondaryRL, plantFrag, "")
                fragmentTransaction.addToBackStack(null)
                fragmentTransaction.commit()
                fragmentManager.executePendingTransactions()
            }else if(mTag=="GrowingPlantsDetailFragment"){
                hideSoftKeyboard(requireActivity())
                val bundle = Bundle()
                bundle.putString("growingPlantName",mGrowingPlantsList!![mPos].name)
                bundle.putString("growingPlantAltName",mGrowingPlantsList!![mPos].plant_alt_name)
                bundle.putString("growingPlantId",mGrowingPlantsList!![mPos].item_id)
                bundle.putString("growingPlantEnv",mGrowingPlantsList!![mPos].plant_env)
                val mFragment = GrowingPlantDetailFragment()
                mFragment.arguments = bundle
                val fragmentManager = fragmentManager
                val fragmentTransaction = fragmentManager!!.beginTransaction()
                fragmentTransaction.add(R.id.secondaryRL, mFragment, "")
                fragmentTransaction.addToBackStack(null)
                fragmentTransaction.commit()
                fragmentManager.executePendingTransactions()
            }
        }
    }
}