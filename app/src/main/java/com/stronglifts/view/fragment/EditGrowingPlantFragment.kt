package com.stronglifts.view.fragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.ViewModelProvider
import com.stronglifts.R
import com.stronglifts.database.DBHelper
import com.stronglifts.databinding.FragmentEditGrowingPlantBinding
import com.stronglifts.model.GrowingPlantsModel
import com.stronglifts.sharedViewModel.EditGrowingPlantSharedViewModel


class EditGrowingPlantFragment : BaseFragment() {

    // - - Initialize Objects
    private lateinit var binding: FragmentEditGrowingPlantBinding
    private lateinit var model: EditGrowingPlantSharedViewModel
    private var dbHandler: DBHelper? = null
    private var mPlantStageArray = arrayOf("Flowering", "Germination", "Seedling", "Vegetative")
    private var mEnvArray = arrayOf("Indoor", "Outdoor")
    private var mGrowingPlantsList: ArrayList<GrowingPlantsModel>? = null
    private var mPlantStage = ""
    private var mEnvironment = ""
    private var mPos = 0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentEditGrowingPlantBinding.inflate(inflater, container, false)
        model = ViewModelProvider(requireActivity()).get(EditGrowingPlantSharedViewModel::class.java)
        initView()
        onViewClicked()
        return binding.root
    }

    private fun initView() {
        mPlantStageSpinner()
        mEnvSpinner()
        getBundleData()
    }

    private fun onViewClicked() {
        binding.backRL.setOnClickListener {
            val fm: FragmentManager? = fragmentManager
            fm!!.popBackStack()
            hideSoftKeyboard(requireActivity())
        }

        binding.txtSaveTV.setOnClickListener {
            hideSoftKeyboard(requireActivity())
            dbHandler = DBHelper(requireActivity())
            dbHandler!!.updateGrowingPlant(
                mGrowingPlantsList!![mPos].item_id.toString(),
                binding.edtPlantAltNameET.text.toString(),
                mPlantStage,
                mEnvironment
            )
            Log.i(TAG, "onViewClicked: " + mGrowingPlantsList!![mPos].item_id.toString() + binding.edtPlantAltNameET.text.toString() + mPlantStage + mEnvironment)
            model.setSelected(mGrowingPlantsList!!)
            val fm: FragmentManager? = fragmentManager
            fm!!.popBackStack()
        }
    }


    private fun mPlantStageSpinner() {
        hideSoftKeyboard(requireActivity())
        val adapter =
            ArrayAdapter(requireActivity(), R.layout.layout_simple_spinner, mPlantStageArray)

        binding.plantStageSpinner.adapter = adapter

        binding.plantStageSpinner.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View, position: Int, id: Long
            ) {
                mPlantStage = mPlantStageArray[position]
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
            }
        }
    }

    private fun mEnvSpinner() {
        hideSoftKeyboard(requireActivity())
        val adapter = ArrayAdapter(requireActivity(), R.layout.layout_simple_spinner, mEnvArray)

        binding.envSpinner.adapter = adapter

        binding.envSpinner.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View, position: Int, id: Long
            ) {
                mEnvironment = mEnvArray[position]
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
            }
        }
    }

    private fun getBundleData() {
        val bundle = arguments
        mGrowingPlantsList = bundle!!.getParcelableArrayList("mGrowingPlantsList")
        mPos = bundle!!.getInt("mPos")
        setDataOnWidgets(mPos, mGrowingPlantsList)
    }

    private fun setDataOnWidgets(mPos: Int, mGrowingPlantsList: ArrayList<GrowingPlantsModel>?) {
        when {
            mGrowingPlantsList!![mPos].plant_alt_name.isNullOrEmpty() -> {
            }
            else -> {
                binding.edtPlantAltNameET.setText(mGrowingPlantsList!![mPos].plant_alt_name)
            }
        }
        when (mGrowingPlantsList[mPos].plant_env) {
            "Outdoor" -> {
                binding.plantStageSpinner.setSelection(1)
            }
            "Indoor" -> {
                binding.plantStageSpinner.setSelection(0)
            }
        }

        when (mGrowingPlantsList[mPos].plant_stage) {
            "Flowering" -> {
                binding.plantStageSpinner.setSelection(0)
            }
            "Germination" -> {
                binding.plantStageSpinner.setSelection(1)
            }
            "Seedling" -> {
                binding.plantStageSpinner.setSelection(2)
            }
            "Vegetative" -> {
                binding.plantStageSpinner.setSelection(3)
            }
        }

    }

}