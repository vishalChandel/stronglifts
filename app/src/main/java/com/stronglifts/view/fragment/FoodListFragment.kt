package com.stronglifts.view.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.goserve.utils.AppPreference
import com.goserve.utils.FOOD_ID
import com.stronglifts.Interface.ClickListenerInterfaceFoodListwithData
import com.stronglifts.R
import com.stronglifts.adapter.FoodListAdapter
import com.stronglifts.database.DBQuery
import com.stronglifts.databinding.FragmentFoodListBinding
import com.stronglifts.model.FoodModel
import com.stronglifts.sharedViewModel.FoodListSharedViewModel
import com.stronglifts.sharedViewModel.FoodSharedViewModel
import com.stronglifts.sharedViewModel.GrowingPlantDetailSharedViewModel
import java.text.SimpleDateFormat

class FoodListFragment : BaseFragment() {

    // - - Initialize Objects
    private lateinit var binding: FragmentFoodListBinding
    private lateinit var mFoodListAdapter: FoodListAdapter
    private lateinit var model: FoodListSharedViewModel
    private var mFoodList: ArrayList<FoodModel>? = null
    private var mNutrientList: ArrayList<FoodModel>? = null
    private var isAlreadyAdded=false
    private var mFragmentTag = ""


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentFoodListBinding.inflate(inflater, container, false)
        model = ViewModelProvider(requireActivity()).get(FoodListSharedViewModel::class.java)
        val mFoodSharedViewModel =
            ViewModelProvider(requireActivity()).get(FoodSharedViewModel::class.java)
        mFoodSharedViewModel.getSelected().observe(viewLifecycleOwner) {
            initView()
        }
        initView()
        performClick()
        return binding.root
    }

    private fun getBundleData() {
        if (arguments != null) {
            val bundle = arguments
            mFragmentTag = bundle!!.getString("FragmentTag").toString()
            mNutrientList = bundle!!.getParcelableArrayList("nutrientList")
        }
    }

    private fun initView() {
        hideSoftKeyboard(requireActivity())
        getBundleData()
        val db = DBQuery(requireContext())
        db.open()
        mFoodList = db.getFoodList()
        db.close()
        initRecyclerView()
    }

    private fun performClick() {
        binding.backRL.setOnClickListener {
            if (mFragmentTag == "AddAction") {
                val fm: FragmentManager? = fragmentManager
                fm!!.popBackStack()
                hideSoftKeyboard(requireActivity())
            } else {
                switchFragment(HomeFragment3(), "", false, null)
            }
        }

        binding.addRL.setOnClickListener {
            val bundle = Bundle()
            val mFragment = AddFoodFragment()
            mFragment.arguments = bundle
            val fragmentManager = fragmentManager
            val fragmentTransaction = fragmentManager!!.beginTransaction()
            fragmentTransaction.add(R.id.mainRL, mFragment, "")
            fragmentTransaction.addToBackStack(null)
            fragmentTransaction.commit()
            fragmentManager.executePendingTransactions()
        }
    }

    private fun initRecyclerView() {
        val layoutManager: RecyclerView.LayoutManager =
            LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        binding.foodListRV.layoutManager = layoutManager
        mFoodListAdapter = FoodListAdapter(activity, mFoodList, mClickListenerInterface)
        binding.foodListRV.adapter = mFoodListAdapter
    }

    private var mClickListenerInterface = object : ClickListenerInterfaceFoodListwithData {
        override fun mClickListenerInterface(mModel: FoodModel) {
            isAlreadyAdded=false
            if (mFragmentTag.isNotEmpty()) {
                if (mNutrientList.isNullOrEmpty()) {
                    requireActivity().let { AppPreference().writeString(it, FOOD_ID, mModel.food_id) }
                    model.setSelected(mFoodList!!)
                    val fm: FragmentManager? = fragmentManager
                    fm!!.popBackStack()
                    isAlreadyAdded=false
                } else {
                    for (i in 0 until mNutrientList!!.size) {
                       if(mNutrientList!![i].food_id == mModel.food_id){
                            isAlreadyAdded=true
                            break
                        }
                    }
                    if(isAlreadyAdded){
                        showAlertDialog(requireActivity(), "Already added this nutrient.")
                    }else{
                        requireActivity().let { AppPreference().writeString(it, FOOD_ID, mModel.food_id) }
                        model.setSelected(mFoodList!!)
                        val fm: FragmentManager? = fragmentManager
                        fm!!.popBackStack()
                    }
                }
            }
        }
    }
}