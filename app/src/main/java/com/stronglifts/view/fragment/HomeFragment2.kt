package com.stronglifts.view.fragment

import android.graphics.Color
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentManager
import com.goserve.utils.AppPreference
import com.goserve.utils.IS_CROSSING_TAB
import com.stronglifts.R
import com.stronglifts.database.DBHelper
import com.stronglifts.database.DBQuery
import com.stronglifts.databinding.FragmentHome2Binding
import java.text.SimpleDateFormat
import java.util.*

class HomeFragment2 : BaseFragment() {

    // - - Initialize Objects
    private lateinit var binding: FragmentHome2Binding
    private var dbHandler: DBHelper? = null
    private var mSelectedDate = ""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentHome2Binding.inflate(inflater, container, false)
        initView()
        performClick()
        return binding.root
    }

    private fun initView() {
        hideSoftKeyboard(requireActivity())
        dbHandler = DBHelper(requireActivity())
        mSelectedDate = SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()).format(Date())
        val db = DBQuery(requireContext())
        db.open()
        val mActionAllListByCurrentDate = db.getActionListAllByCurrentDate(convertDateInTimestamp(mSelectedDate).toString())
        if (mActionAllListByCurrentDate!!.isNotEmpty()) {
            for (i in 0 until mActionAllListByCurrentDate!!.size) {
                dbHandler!!.updateHarvestingPlant(mActionAllListByCurrentDate!![i].ac_plant_id, "2")
            }
        }
        val mChangeStateActionList = db.getChangeStateActionList(convertDateInTimestamp(mSelectedDate).toString())
        if (mChangeStateActionList!!.isNotEmpty()) {
            for (i in 0 until mChangeStateActionList.size) {
                var mUpdatedStage = ""
                when (mChangeStateActionList[i].ac_type_method) {
                    "0" -> {
                        mUpdatedStage = "Flowering"
                    }
                    "1" -> {
                        mUpdatedStage = "Germination"
                    }
                    "2" -> {
                        mUpdatedStage = "Seedling"
                    }
                    "3" -> {
                        mUpdatedStage = "Vegetative"
                    }
                }
                dbHandler!!.updateGrowingPlantState(mChangeStateActionList!![i].ac_plant_id, mUpdatedStage)
            }
        }
        db.close()
        if (!isCrossingTab()) {
            tabSelection(0)
            binding.addRL.visibility = View.VISIBLE
        } else if (isCrossingTab()) {
            tabSelection(1)
            binding.addRL.visibility = View.GONE
        }
    }

    private fun performClick() {
        binding.txtGrowingHarvestTV.setOnClickListener(View.OnClickListener {
            tabSelection(0)
            binding.addRL.visibility = View.VISIBLE
        })

        binding.txtGrowingPlantsTV.setOnClickListener(View.OnClickListener {
            tabSelection(1)
            binding.addRL.visibility = View.GONE
        })

        binding.addRL.setOnClickListener {
            val bundle = Bundle()
            bundle.putString("FragmentTag", "HomeFragment2")
            val mAddPlantFragment = AddPlantFragment()
            mAddPlantFragment.arguments = bundle
            val fragmentManager = childFragmentManager
            val fragmentTransaction = fragmentManager.beginTransaction()
            fragmentTransaction.add(R.id.mainRL, mAddPlantFragment, "")
            fragmentTransaction.addToBackStack(null)
            fragmentTransaction.commit()
            fragmentManager.executePendingTransactions()
        }
    }

    private fun tabSelection(pos: Int) {
        when (pos) {
            0 -> {
                hideSoftKeyboard(requireActivity())
                AppPreference().writeBoolean(requireContext()!!, IS_CROSSING_TAB, false)
                binding.txtGrowingHarvestTV.setTextColor(Color.parseColor("#FFFFFF"))
                binding.txtGrowingPlantsTV.setTextColor(Color.parseColor("#C1C1C1"))
                binding.txtGrowingHarvestTV.setBackgroundResource(R.drawable.bg_link)
                binding.txtGrowingPlantsTV.setBackgroundResource(R.color.colorWhite)
                switchTabs(GrowingPlantsFragment(), "", false, null)
            }
            1 -> {
                hideSoftKeyboard(requireActivity())
                AppPreference().writeBoolean(requireContext()!!, IS_CROSSING_TAB, true)
                binding.txtGrowingHarvestTV.setTextColor(Color.parseColor("#C1C1C1"))
                binding.txtGrowingPlantsTV.setTextColor(Color.parseColor("#FFFFFF"))
                binding.txtGrowingPlantsTV.setBackgroundResource(R.drawable.bg_link)
                binding.txtGrowingHarvestTV.setBackgroundResource(R.color.colorWhite)
                switchTabs(HarvestingPlantsFragment(), "", false, null)
            }
        }
    }

    // Switch Between Tabs*
    private fun switchTabs(
        fragment: Fragment?,
        Tag: String?,
        addToStack: Boolean,
        bundle: Bundle?
    ) {
        val fragmentManager: FragmentManager = childFragmentManager
        if (fragment != null) {
            val fragmentTransaction = fragmentManager.beginTransaction()
            fragmentTransaction.replace(R.id.homeFragment2FL, fragment, Tag)
            if (addToStack) fragmentTransaction.addToBackStack(Tag)
            if (bundle != null) fragment.arguments = bundle
            fragmentTransaction.commit()
            fragmentManager.executePendingTransactions()

        }

    }
}