package com.stronglifts.view.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.ViewModelProvider
import com.stronglifts.R
import com.stronglifts.database.DBHelper
import com.stronglifts.database.DBQuery
import com.stronglifts.databinding.FragmentAddEnvironmentLogBinding
import com.stronglifts.databinding.FragmentChartBinding
import com.stronglifts.model.GrowingPlantsModel
import com.stronglifts.sharedViewModel.EnvLogSharedViewModel
import com.stronglifts.sharedViewModel.FoodSharedViewModel
import java.text.SimpleDateFormat
import java.util.*

class AddEnvironmentLogFragment : BaseFragment() {

    // - - Initialize Objects
    private lateinit var binding: FragmentAddEnvironmentLogBinding
    private lateinit var mEnvLogSharedViewModel: EnvLogSharedViewModel
    private var dbHandler: DBHelper? = null
    private var mEnvArray = arrayOf("Indoor", "Outdoor")
    private var mEnvironment = ""
    private var mEnvLogId = ""
    private var mFragmentTag = ""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentAddEnvironmentLogBinding.inflate(inflater, container, false)
        mEnvLogSharedViewModel = ViewModelProvider(requireActivity()).get(EnvLogSharedViewModel::class.java)
        initView()
        onViewClicked()
        return binding.root
    }

    private fun initView() {
        mEnvSpinner()
        getBundleData()
    }

    private fun onViewClicked() {
        binding.backRL.setOnClickListener {
            if (mFragmentTag == "GrowingPlantDetailFragment") {
                val fm: FragmentManager? = fragmentManager
                fm!!.popBackStack()
                hideSoftKeyboard(requireActivity())
            } else {
                val fm: FragmentManager? = fragmentManager
                fm!!.popBackStack()
            }
        }

        binding.txtSelectDateTV.setOnClickListener {
            datePicker(binding.txtSelectDateTV)
        }

        binding.txtSaveTV.setOnClickListener {
            if (isValidate()) {
                hideSoftKeyboard(requireActivity())
                if (mEnvLogId.isEmpty()) {
                    dbHandler = DBHelper(requireActivity())
                    dbHandler!!.addEnvironmentLog(mEnvironment, convertDateInTimestamp(binding.txtSelectDateTV.text.toString()).toString(), binding.edtNoteET.text.toString())
                    val mSelectedDate = SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()).format(Date())
                    val db = DBQuery(requireContext())
                    db.open()
                    val mEnvLogArrayList = db.getEnvLogList(mSelectedDate)
                    db.close()
                    mEnvLogSharedViewModel.setSelected(mEnvLogArrayList!!)
                    val fm: FragmentManager? = fragmentManager
                    fm!!.popBackStack()
                } else {
                    dbHandler = DBHelper(requireActivity())
                    dbHandler!!.updateEnvLog(mEnvLogId, mEnvironment, convertDateInTimestamp(binding.txtSelectDateTV.text.toString()).toString(), binding.edtNoteET.text.toString())
                    val mSelectedDate = SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()).format(Date())
                    val db = DBQuery(requireContext())
                    db.open()
                    val mEnvLogArrayList = db.getEnvLogList(mSelectedDate)
                    db.close()
                    mEnvLogSharedViewModel.setSelected(mEnvLogArrayList!!)
                    val fm: FragmentManager? = fragmentManager
                    fm!!.popBackStack()
                }

            }
        }
    }


    private fun getBundleData() {
        if (arguments != null) {
            val bundle = arguments
            mEnvLogId = bundle!!.getString("mEnvLogId").toString()
            val mEnv = bundle!!.getString("mEnv").toString()
            val mDate = bundle!!.getString("mDate").toString()
            val mDesc = bundle!!.getString("mDesc").toString()
            mFragmentTag = bundle!!.getString("FragmentTag").toString()
            if (!bundle!!.getString("FragmentTag").toString().isNullOrEmpty()) {
                if (bundle!!.getString("FragmentTag").toString() == "GrowingPlantDetailFragment") {
                    // disable user interaction
                    binding.envSpinner.setOnTouchListener { _, _ -> false }
                    binding.envRL.setOnTouchListener { _, _ -> false }
                    binding.envSpinner.isEnabled = false
                    binding.envRL.isEnabled = false
                    binding.envSpinner.isClickable = false
                    binding.envRL.isClickable = false
                }
            }
            if (mEnv.isNotEmpty()) {
                if (mEnv == "Indoor")
                    binding.envSpinner.setSelection(0)
                else if (mEnv == "Outdoor") {
                    binding.envSpinner.setSelection(1)
                }
            }
            if (mDate.isNotEmpty()) {
                binding.txtSelectDateTV.text = convertTimestampInDate(mDate)
            }
            if (mDesc.isNotEmpty()) {
                binding.edtNoteET.setText(mDesc)
            }
        }
    }


    private fun mEnvSpinner() {
        hideSoftKeyboard(requireActivity())
        val adapter = ArrayAdapter(requireActivity(), R.layout.layout_simple_spinner, mEnvArray)

        binding.envSpinner.adapter = adapter

        binding.envSpinner.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View, position: Int, id: Long
            ) {
                mEnvironment = mEnvArray[position]
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
            }
        }
    }

    // - - Validations for input fields
    private fun isValidate(): Boolean {
        var flag = true
        when {
            binding.txtSelectDateTV?.text.toString().trim() == "" -> {
                showAlertDialog(requireActivity(), getString(R.string.please_select_date))
                flag = false
            }
            binding.edtNoteET?.text.toString().trim() == "" -> {
                showAlertDialog(requireActivity(), getString(R.string.please_enter_note))
                flag = false
            }
        }
        return flag
    }
}