package com.stronglifts.view.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.OnTouchListener
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.FragmentManager
import com.goserve.utils.PLANT_IMAGES
import com.stronglifts.R
import com.stronglifts.database.DBHelper
import com.stronglifts.database.DBQuery
import com.stronglifts.databinding.FragmentPlantDetailBinding
import com.stronglifts.model.PlantsModel
import com.stronglifts.view.activity.HomeActivity


class PlantDetailFragment : BaseFragment() {

    // - - Initialize Objects
    private lateinit var binding: FragmentPlantDetailBinding
    private var mPlantDetailsList: ArrayList<PlantsModel>? = null
    private var mPlantsList: ArrayList<PlantsModel>? = null
    private var dbHandler: DBHelper? = null
    private var mPos = 0
    private var mPlantId = ""
    private var mPlantType = "0"
    private var mTag = ""
    private var mFragmentTag = ""
    private var isAlreadyAdded = ""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentPlantDetailBinding.inflate(inflater, container, false)
        initView()
        performClick()
        return binding.root
    }

    private fun initView() {
        hideSoftKeyboard(requireActivity())
        dbHandler = DBHelper(requireContext())
        getBundleData()
        val db = DBQuery(requireContext())
        db.open()
        mPlantsList = db.getAddedPlantsList()
        if (!mPlantsList.isNullOrEmpty()) {
            for (i in 0 until mPlantsList!!.size) {
                if (mPlantsList!![i].id.contains(mPlantId)) {
                    isAlreadyAdded = "true"
                    break
                } else {
                    isAlreadyAdded = "false"
                }
            }
        }
        //  Disable touch on SeekBar
        binding.lightSB.setOnTouchListener(OnTouchListener { _, _ -> true })
    }

    private fun getBundleData() {
        val bundle = arguments
        mPlantDetailsList = bundle!!.getParcelableArrayList("mPlantsList")
        mPos = bundle!!.getInt("mPos")
        mTag = bundle!!.getString("mTag").toString()
        mFragmentTag = bundle!!.getString("FragmentTag").toString()
        if (mTag == "HomeFragment1") {
            binding!!.addRL.visibility = View.GONE
        }
        if (mFragmentTag == "HomeFragment1") {
            mPlantType = "0"
        } else if (mFragmentTag == "HomeFragment2") {
            if (!isCrossingTab()) {
                mPlantType = "1"
            } else if (isCrossingTab()) {
                mPlantType = "1"
            }

        }
        setDataOnWidgets(mPos, mPlantDetailsList)
    }

    private fun performClick() {
        binding.addRL.setOnClickListener {
            if (isAlreadyAdded == "true") {
                showAlertDialog(requireActivity(), "Plant added already in favourites.")
            } else {
                dbHandler!!.addPlant(mPlantId, mPlantType)
                if (mFragmentTag == "HomeFragment1") {
                    switchFragment(HomeFragment1(), "", false, null)
                } else if (mFragmentTag == "HomeFragment2") {
                    switchFragment(HomeFragment2(), "", false, null)
                }
            }
        }


        binding.backRL.setOnClickListener {
            val fm: FragmentManager? = fragmentManager
            fm!!.popBackStack()
        }
    }

    private fun setDataOnWidgets(mPos: Int, mPlantDetailsList: ArrayList<PlantsModel>?) {
        mPlantId = mPlantDetailsList!![mPos]?.id
        binding.txtPlantNameTV.text = mPlantDetailsList!![mPos]?.name
        binding.txtCategoryNameTV.text = mPlantDetailsList[mPos].category
        binding.txtDetail1TV.text = mPlantDetailsList!![mPos]?.sooutdoors
        binding.txtDetail2TV.text = mPlantDetailsList!![mPos]?.light
        binding.txtDetail3TV.text = mPlantDetailsList!![mPos]?.seeddepth + " Seed depth"
        binding.txtDetail4TV.text =
            mPlantDetailsList!![mPos]?.daystogermination + " Days to germinate"
        binding.txtDetail5TV.text = mPlantDetailsList!![mPos]?.spacinginbeds + " Spacing"
        binding.txtDetail6TV.text = mPlantDetailsList!![mPos]?.watering
        binding.txtDescriptionTV.text = mPlantDetailsList!![mPos]?.description
        binding.txtGoodCompanionsTV.text = mPlantDetailsList!![mPos]?.goodcomapnions
        binding.txtBadCompanionsTV.text = mPlantDetailsList!![mPos]?.badcomapnions
        binding.txtPHRangeTV.text = mPlantDetailsList!![mPos]?.phrange
        binding.txtSowIndoorsTV.text = mPlantDetailsList!![mPos]?.sowindoors
        binding.txtSowOutdoorsTV.text = mPlantDetailsList!![mPos]?.sooutdoors
        binding.txtGerminationSoilTV.text = mPlantDetailsList!![mPos]?.germinationsoiltemp
        binding.txtGrowingSoilTempTV.text = mPlantDetailsList!![mPos]?.growingsoiltemp
        binding.txtSowingDescriptionTV.text = mPlantDetailsList!![mPos]?.sowigdescription
        binding.txtGrowingDescriptionTV.text = mPlantDetailsList!![mPos]?.growingdescription
        binding.txtHarvestingDescriptionTV.text = mPlantDetailsList!![mPos]?.harvestingdescription

        if (mPlantDetailsList!![mPos]?.light == "Full sun" || mPlantDetailsList!![mPos]?.light == "Full") {
            binding.lightSB.progress = 2
        } else {
            binding.lightSB.progress = 1
        }
        for (i in 0 until PLANT_IMAGES!!.size) {
            val mStaticImagesArray =
                requireActivity().resources.getResourceEntryName(PLANT_IMAGES[i]) + ".png"
            if (mPlantDetailsList!![mPos]!!.thumbnailImage == mStaticImagesArray) {
                binding.imgPlantIV.setImageResource(PLANT_IMAGES[i])
                break
            }
        }
    }
}