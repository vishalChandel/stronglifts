package com.stronglifts.view.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.stronglifts.R
import com.stronglifts.adapter.HomeFragment4Adapter
import com.stronglifts.adapter.OtherResourcesAdapter
import com.stronglifts.databinding.FragmentHome4Binding
import com.stronglifts.databinding.FragmentOtherResourcesBinding

class OtherResourcesFragment : BaseFragment() {

    // - - Initialize Objects
    private lateinit var binding: FragmentOtherResourcesBinding
    private lateinit var mOtherResourcesAdapter: OtherResourcesAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentOtherResourcesBinding.inflate(inflater, container, false)
        container!!.removeAllViews()
        initView()
        performClick()
        return binding.getRoot()
    }

    private fun initView() {
        hideSoftKeyboard(requireActivity())
        initRecylerView()
    }

    private fun performClick() {
        binding.backRL.setOnClickListener {
            switchFragment(HomeFragment4(),"",false,null)
        }
    }

    private fun initRecylerView() {
        val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        binding.otherResourcesRV.layoutManager = layoutManager
        mOtherResourcesAdapter = OtherResourcesAdapter(activity)
        binding.otherResourcesRV.adapter = mOtherResourcesAdapter
    }
}