package com.stronglifts.view.fragment

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.stronglifts.Interface.ClickListenerInterfaceHarvestModel
import com.stronglifts.Interface.DeleteClickListenerInterfaceHarvestModel
import com.stronglifts.R
import com.stronglifts.adapter.GrowingHarvestAdapter
import com.stronglifts.database.DBHelper
import com.stronglifts.database.DBQuery
import com.stronglifts.databinding.FragmentGrowingHarvestBinding
import com.stronglifts.model.GrowingPlantsModel
import kotlin.collections.ArrayList

class HarvestingPlantsFragment : BaseFragment() {

    // - - Initialize Objects
    private lateinit var binding: FragmentGrowingHarvestBinding
    private lateinit var mGrowingHarvestAdapter: GrowingHarvestAdapter
    private lateinit var db: DBQuery
    private var dbHandler: DBHelper? = null
    private var mGrowingHarvest: ArrayList<GrowingPlantsModel>? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentGrowingHarvestBinding.inflate(inflater, container, false)
        initView()
        performClick()
        return binding.root
    }

    fun initView() {
        db = DBQuery(requireContext())
        db.open()
        hideSoftKeyboard(requireActivity())
        mGrowingHarvest = db.getHarvestPlantsList()
        db.close()
        initRecyclerView()
    }

    private fun performClick() {
    }

    private fun initRecyclerView() {
        val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        binding.growingHarvestRV.layoutManager = layoutManager
        mGrowingHarvestAdapter = GrowingHarvestAdapter(activity, mGrowingHarvest, mClickListenerInterfaceHarvestModel,mDeleteClickListenerInterfaceHarvestModel)
        binding.growingHarvestRV.adapter = mGrowingHarvestAdapter
    }

    private var mClickListenerInterfaceHarvestModel = object : ClickListenerInterfaceHarvestModel {
        override fun mClickListenerInterface(mModel: GrowingPlantsModel) {
            hideSoftKeyboard(requireActivity())
            val bundle = Bundle()
            bundle.putParcelable("mModel",mModel)
            val mFragment = HarvestingPlantDetailFragment()
            mFragment.arguments = bundle
            val fragmentManager = fragmentManager
            val fragmentTransaction = fragmentManager!!.beginTransaction()
            fragmentTransaction.add(R.id.secondaryRL, mFragment, "")
            fragmentTransaction.addToBackStack(null)
            fragmentTransaction.commit()
            fragmentManager.executePendingTransactions()
        }
    }

    private var mDeleteClickListenerInterfaceHarvestModel = object : DeleteClickListenerInterfaceHarvestModel {
        override fun mClickListenerInterface(mModel: GrowingPlantsModel) {
            hideSoftKeyboard(requireActivity())
           showDeleteConfirmAlertDialog(mModel.item_id!!)
        }
    }

    // - - Delete  Alert Dialog
    private fun showDeleteConfirmAlertDialog(mItemId: String) {
        val alertDialog = Dialog(requireActivity())
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.dialog_logout)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set the custom dialog components - text, image and button
        val btnYes = alertDialog.findViewById<TextView>(R.id.btnYes)
        val btnNo = alertDialog.findViewById<TextView>(R.id.btnNo)

        btnNo.setOnClickListener {
            alertDialog.dismiss()
        }

        btnYes.setOnClickListener {
            dbHandler = DBHelper(requireActivity())
            dbHandler!!.deletePlantFromPlantList(mItemId, "2")
            db = DBQuery(requireContext())
            db.open()
            hideSoftKeyboard(requireActivity())
            mGrowingHarvest = db.getHarvestPlantsList()
            db.close()
            initRecyclerView()
            alertDialog.dismiss()
        }
        alertDialog.show()
    }
}