package com.stronglifts.view.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentManager
import com.stronglifts.R
import com.stronglifts.adapter.GrowingPlantsParentAdapter
import com.stronglifts.database.DBQuery
import com.stronglifts.databinding.FragmentChoosePlantStageBinding
import com.stronglifts.databinding.FragmentGrowingPlantsBinding
import com.stronglifts.model.PlantsModel

class ChoosePlantStageFragment : BaseFragment() {

    // - - Initialize Objects
    private lateinit var binding: FragmentChoosePlantStageBinding
    private var mGpId=""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentChoosePlantStageBinding.inflate(inflater, container, false)
        initView()
        onViewClicked()
        return binding.root
    }

    private fun initView() {
        val bundle = arguments
        if(bundle!=null) {
            mGpId = bundle.getString("mGpId").toString()
        }
    }

    private fun onViewClicked() {
        binding.backRL.setOnClickListener {
            val fm: FragmentManager? = fragmentManager
            fm!!.popBackStack()
        }

        binding.floweringRL.setOnClickListener {
            val bundle = Bundle()
            bundle.putString("mGpId", mGpId)
            bundle.putString("mGpStage", "Flowering")
            val mPlantStageFragment = ChooseEnvironmentFragment()
            mPlantStageFragment.arguments = bundle
            val fragmentManager = fragmentManager
            val fragmentTransaction = fragmentManager!!.beginTransaction()
            fragmentTransaction.add(R.id.mainRL, mPlantStageFragment, "")
            fragmentTransaction.addToBackStack(null)
            fragmentTransaction.commit()
            fragmentManager.executePendingTransactions()
        }

        binding.germinationRL.setOnClickListener {
            val bundle = Bundle()
            bundle.putString("mGpId", mGpId)
            bundle.putString("mGpStage", "Germination")
            val mPlantStageFragment = ChooseEnvironmentFragment()
            mPlantStageFragment.arguments = bundle
            val fragmentManager = fragmentManager
            val fragmentTransaction = fragmentManager!!.beginTransaction()
            fragmentTransaction.add(R.id.mainRL, mPlantStageFragment, "")
            fragmentTransaction.addToBackStack(null)
            fragmentTransaction.commit()
            fragmentManager.executePendingTransactions()
        }

        binding.seedlingRL.setOnClickListener {
            val bundle = Bundle()
            bundle.putString("mGpId", mGpId)
            bundle.putString("mGpStage", "Seedling")
            val mPlantStageFragment = ChooseEnvironmentFragment()
            mPlantStageFragment.arguments = bundle
            val fragmentManager = fragmentManager
            val fragmentTransaction = fragmentManager!!.beginTransaction()
            fragmentTransaction.add(R.id.mainRL, mPlantStageFragment, "")
            fragmentTransaction.addToBackStack(null)
            fragmentTransaction.commit()
            fragmentManager.executePendingTransactions()
        }

        binding.vegetativeRL.setOnClickListener {
            val bundle = Bundle()
            bundle.putString("mGpId", mGpId)
            bundle.putString("mGpStage", "Vegetative")
            val mPlantStageFragment = ChooseEnvironmentFragment()
            mPlantStageFragment.arguments = bundle
            val fragmentManager = fragmentManager
            val fragmentTransaction = fragmentManager!!.beginTransaction()
            fragmentTransaction.add(R.id.mainRL, mPlantStageFragment, "")
            fragmentTransaction.addToBackStack(null)
            fragmentTransaction.commit()
            fragmentManager.executePendingTransactions()
        }

    }
}