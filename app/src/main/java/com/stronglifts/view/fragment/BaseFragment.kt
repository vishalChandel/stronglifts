package com.stronglifts.view.fragment

import android.app.Activity
import android.app.DatePickerDialog
import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.Window
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.goserve.utils.AppPreference
import com.goserve.utils.FOOD_ID
import com.goserve.utils.IS_CROSSING_TAB
import com.stronglifts.R
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

open class BaseFragment : Fragment() {

    // - - Get Class Name
    var TAG = this@BaseFragment.javaClass.simpleName


    //
    fun isCrossingTab(): Boolean {
        return activity?.let { AppPreference().readBoolean(it, IS_CROSSING_TAB, false) }!!
    }

    //
    fun foodId(): String {
        return activity?.let { AppPreference().readString(it, FOOD_ID, "") }!!
    }

    // - - Switch Between Fragments
    fun switchFragment(
        fragment: Fragment?,
        Tag: String?,
        addToStack: Boolean,
        bundle: Bundle?
    ) {
        val fragmentManager = requireActivity()!!.supportFragmentManager
        if (fragment != null) {
            val fragmentTransaction = fragmentManager.beginTransaction()
            fragmentTransaction.replace(R.id.mainRL, fragment, Tag)
            if (bundle != null) fragment.arguments = bundle
            fragmentTransaction.commit()
            fragmentManager.executePendingTransactions()
        }
    }

    // - - To Show Toast Message
    fun showToast(mActivity: Activity?, strMessage: String?) {
        Toast.makeText(mActivity, strMessage, Toast.LENGTH_SHORT).show()
    }

    // - - To Remove Focus
    fun setEditTextFocused(mEditText: EditText) {
        mEditText.setOnEditorActionListener(TextView.OnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                mEditText.clearFocus()
                val imm =
                    v.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(v.windowToken, 0)
                return@OnEditorActionListener true
            }
            false
        })
    }

    // - - Date Picker
    open fun datePicker(mTextview: TextView) {
        // - - To Hide Keyboard
        val imm =
            requireActivity().getSystemService(AppCompatActivity.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(mTextview.windowToken, 0)
        val mYear: Int
        val mMonth: Int
        val mDay: Int
        var mHour: Int
        var mMinute: Int
        val mDayOfWeek: Int
        // Get Current Date
        val c = Calendar.getInstance()
        mYear = c[Calendar.YEAR]
        mMonth = c[Calendar.MONTH]
        mDay = c[Calendar.DAY_OF_MONTH]
        mDayOfWeek = c[Calendar.DAY_OF_WEEK]
        val datePickerDialog = DatePickerDialog(
            requireActivity(),
            { view, year, monthOfYear, dayOfMonth ->
                val intMonth = monthOfYear + 1
                val date: String =
                    getFormatedString("" + getFormatedString("" + dayOfMonth)).toString() + "/" + getFormatedString(
                        "" + getFormatedString("" + intMonth)
                    ).toString() + "/" + year
                mTextview.text = date
                mTextview.setTextColor(Color.BLACK)
            }, mYear, mMonth, mDay
        )
        datePickerDialog.show()
    }

    // - - To Get Formatted String
    open fun getFormatedString(strTemp: String): String? {
        var strActual = ""
        if (strTemp.length == 1) {
            strActual = "0$strTemp"
        } else if (strTemp.length == 2) {
            strActual = strTemp
        }
        return strActual
    }

    // - - To Hide Keyboard
    open fun hideSoftKeyboard(activity: Activity) {
        if (activity.currentFocus == null) {
            return
        }
        val inputMethodManager =
            activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(activity.currentFocus!!.windowToken, 0)
    }

    // - - To Show Alert Dialog
    open fun showAlertDialog(mActivity: Activity?, strMessage: String?) {
        val alertDialog = Dialog(mActivity!!)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.dialog_alert)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set the custom dialog components - text, image and button
        val txtMessageTV = alertDialog.findViewById<TextView>(R.id.txtMessageTV)
        val btnDismiss = alertDialog.findViewById<TextView>(R.id.btnDismiss)
        txtMessageTV.text = strMessage
        btnDismiss.setOnClickListener { alertDialog.dismiss() }
        alertDialog.show()
    }

    open fun convertDateInTimestamp(dateString: String): Long {
        val formatter: DateFormat = SimpleDateFormat("dd/MM/yyyy")
        val date = formatter.parse(dateString) as Date
        return date.time
    }

    open fun convertTimestampInDate(timestamp: String): String {
        val formatter: DateFormat = SimpleDateFormat("dd/MM/yyyy")
        val date = Date(timestamp.toLong())
        return formatter.format(date)
    }
}