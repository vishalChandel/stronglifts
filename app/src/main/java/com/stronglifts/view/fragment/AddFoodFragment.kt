package com.stronglifts.view.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.ViewModelProvider
import com.stronglifts.R
import com.stronglifts.database.DBHelper
import com.stronglifts.database.DBQuery
import com.stronglifts.databinding.FragmentAddFoodBinding
import com.stronglifts.sharedViewModel.FoodSharedViewModel
import com.stronglifts.sharedViewModel.GrowingPlantDetailSharedViewModel

class AddFoodFragment : BaseFragment() {

    // - - Initialize Objects
    private lateinit var binding: FragmentAddFoodBinding
    private lateinit var mFoodSharedViewModel: FoodSharedViewModel
    private var mQuantityArray = arrayOf(
        "ml",
        "cc",
        "m3",
        "in3",
        "ft3",
        "gal",
        "mm3",
        "l",
        "cups",
        "ft. oz",
        "pint",
        "quart",
        "tbsp"
    )

    private var dbHandler: DBHelper? = null
    private var mNutrient = ""
    private var mAmount = ""
    private var mSolvent = ""
    private var mAmountQuantity = ""
    private var mSolventQuantity = ""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentAddFoodBinding.inflate(inflater, container, false)
        mFoodSharedViewModel = ViewModelProvider(requireActivity()).get(FoodSharedViewModel::class.java)
        initView()
        performClick()
        return binding.root
    }

    private fun initView() {
        setEditTextFocused(binding.edtSolventET)
        mAmountSpinner()
        mWaterSpinner()
    }

    private fun performClick() {
        binding.backRL.setOnClickListener {
            hideSoftKeyboard(requireActivity())
            val fm: FragmentManager? = fragmentManager
            fm!!.popBackStack()
        }

        binding.txtSaveTV.setOnClickListener {
            hideSoftKeyboard(requireActivity())
            if (isValidate()) {
                mNutrient = binding.edtNutrientET.text.toString()
                mAmount = binding.edtAmountET.text.toString()
                mSolvent = binding.edtSolventET.text.toString()
                dbHandler = DBHelper(requireActivity())
                dbHandler!!.addFood(mNutrient, "$mAmount ($mAmountQuantity)","$mSolvent ($mSolventQuantity)")
                showToast(requireActivity(), "Food added successfully.")
                binding.edtNutrientET.setText("")
                binding.edtAmountET.setText("")
                binding.edtSolventET.setText("")
                val db = DBQuery(requireContext())
                db.open()
                val mFoodArrayList = db.getFoodList()
                db.close()
                mFoodSharedViewModel.setSelected(mFoodArrayList!!)
                val fm: FragmentManager? = fragmentManager
                fm!!.popBackStack()
            }
        }
    }

    private fun mAmountSpinner() {
        val adapter = ArrayAdapter(requireActivity(), R.layout.layout_simple_spinner, mQuantityArray)

        binding.amountSpinner.adapter = adapter

        binding.amountSpinner.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View, position: Int, id: Long
            ) {
                mAmountQuantity=mQuantityArray[position]
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
            }
        }
    }


    private fun mWaterSpinner() {
        val adapter =
            ArrayAdapter(requireActivity(), R.layout.layout_simple_spinner, mQuantityArray)
        binding.waterSpinner.adapter = adapter

        binding.waterSpinner.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View, position: Int, id: Long
            ) {
                mSolventQuantity=mQuantityArray[position]
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
            }
        }
    }


    // - - Validations for input fields
    private fun isValidate(): Boolean {
        var flag = true
        when {
            binding.edtNutrientET.text.toString().trim() == "" -> {
                showAlertDialog(requireActivity(), getString(R.string.please_enter_food_title))
                flag = false
            }
            binding.edtAmountET?.text.toString().trim() == "" -> {
                showAlertDialog(
                    requireActivity(),
                    getString(R.string.please_enter_potassium_quantity)
                )
                flag = false
            }
            binding.edtSolventET?.text.toString().trim() == "" -> {
                showAlertDialog(requireActivity(), getString(R.string.please_enter_total))
                flag = false
            }
        }
        return flag
    }
}