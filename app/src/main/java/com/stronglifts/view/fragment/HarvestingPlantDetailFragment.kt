package com.stronglifts.view.fragment

import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.fragment.app.FragmentManager
import com.goserve.utils.PLANT_IMAGES
import com.stronglifts.R
import com.stronglifts.adapter.HarvestingPlantsParentAdapter
import com.stronglifts.database.DBQuery
import com.stronglifts.databinding.FragmentHarvestingPlantDetailBinding
import com.stronglifts.model.ActionModel
import com.stronglifts.model.EnvLogModel
import com.stronglifts.model.GrowingPlantsModel
import com.stronglifts.model.TreeLogModel
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.*


class HarvestingPlantDetailFragment : BaseFragment() {

    // - - Initialize Objects
    private lateinit var binding: FragmentHarvestingPlantDetailBinding
    private lateinit var mHarvestingPlantsParentAdapter: HarvestingPlantsParentAdapter
    private val mHarvestingPlantDates = ArrayList<String?>()
    private var mHarvestingPlantActionList = ArrayList<ActionModel>()
    private var mHarvestingPlantUpdatedActionList = ArrayList<ActionModel>()
    private var mHarvestingPlantTreeLogList = ArrayList<TreeLogModel>()
    private var mHarvestingPlantEnvLogList = ArrayList<EnvLogModel>()
    private var mHarvestDate=""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentHarvestingPlantDetailBinding.inflate(inflater, container, false)
        initView()
        onViewClicked()
        return binding.root
    }

    private fun initView() {
        getBundleData()
    }


    private fun onViewClicked() {
        binding.backRL.setOnClickListener {
            val fm: FragmentManager? = fragmentManager
            fm!!.popBackStack()
        }
    }

    private fun getBundleData() {
        if (arguments != null) {
            val bundle = arguments
            val mHarvestPlantModel: GrowingPlantsModel = bundle!!.getParcelable("mModel")!!
            val db = DBQuery(requireContext())
            db.open()
            mHarvestingPlantActionList = db.getActionListForHarvestPlant(mHarvestPlantModel.item_id!!)!!
            for(i in 0 until mHarvestingPlantActionList!!.size){
                if(mHarvestingPlantActionList!![i].ac_type=="Harvest" || mHarvestingPlantActionList!![i].ac_type=="Declare Death"){
                    mHarvestDate= mHarvestingPlantActionList!![i].ac_date
                    break
                }
            }
            mHarvestingPlantUpdatedActionList=db.getActionListForHarvestPlantByHarvestDate(mHarvestPlantModel.item_id!!,mHarvestDate)!!
            mHarvestingPlantTreeLogList = db.getTreeLogListForHarvestPlant(mHarvestPlantModel.item_id!!,mHarvestDate)!!
            mHarvestingPlantEnvLogList = db.getEnvLogListForHarvestPlant(mHarvestPlantModel.plant_env!!,mHarvestDate)!!
            db.close()

            if (!mHarvestingPlantUpdatedActionList.isNullOrEmpty()) {
                for (i in 0 until mHarvestingPlantUpdatedActionList!!.size) {
                    if(!mHarvestingPlantDates.contains(mHarvestingPlantUpdatedActionList!![i].ac_date)) {
                        mHarvestingPlantDates.add(mHarvestingPlantUpdatedActionList!![i].ac_date)
                    }
                }
            }

            if (!mHarvestingPlantTreeLogList.isNullOrEmpty()) {
                for (i in 0 until mHarvestingPlantTreeLogList!!.size) {
                    if(!mHarvestingPlantDates.contains(mHarvestingPlantTreeLogList!![i].log_date)) {
                        mHarvestingPlantDates.add(mHarvestingPlantTreeLogList!![i].log_date)
                    }
                }
            }

            if (!mHarvestingPlantEnvLogList.isNullOrEmpty()) {
                for (i in 0 until mHarvestingPlantEnvLogList!!.size) {
                    if(!mHarvestingPlantDates.contains(mHarvestingPlantEnvLogList!![i].env_log_date)) {
                        mHarvestingPlantDates.add(mHarvestingPlantEnvLogList!![i].env_log_date)
                    }
                }
            }

            mHarvestingPlantDates.toSet().toList()
            mHarvestingPlantDates.sortWith(compareBy { it })
            mHarvestingPlantDates.reverse()
            initHarvestingPlantParentRecyclerView(mHarvestPlantModel.creation_date)
            setDataOnWidgets(mHarvestPlantModel)
        }
    }

    private fun initHarvestingPlantParentRecyclerView(creationDate: String?) {
        mHarvestingPlantsParentAdapter = HarvestingPlantsParentAdapter(activity, mHarvestingPlantDates, mHarvestingPlantUpdatedActionList,mHarvestingPlantTreeLogList,mHarvestingPlantEnvLogList,creationDate)
        binding.harvestDetailRV.adapter = mHarvestingPlantsParentAdapter
    }

    private fun setDataOnWidgets(mHarvestPlantModel: GrowingPlantsModel) {
        for (i in 0 until PLANT_IMAGES!!.size) {
            val mStaticImagesArray = requireActivity().resources.getResourceEntryName(PLANT_IMAGES[i]) + ".png"
            if (mHarvestPlantModel.thumbnailImage == mStaticImagesArray) {
                binding.imgPlantIV.setImageResource(PLANT_IMAGES[i])
                break
            }
        }
        binding.txtPlantNameTV.text = mHarvestPlantModel.name
        if (!mHarvestPlantModel.plant_alt_name.isNullOrEmpty()) {
            binding.altNameTV.visibility = View.VISIBLE
            binding.altNameTV.text = "(" + mHarvestPlantModel.plant_alt_name + ")"
        }

        when (mHarvestPlantModel.plant_env) {
            "Outdoor" -> {
                binding.envIV.setImageResource(R.drawable.ic_outdoor)
                binding.envTV.text = "Outdoor"
            }
            "Indoor" -> {
                binding.envIV.setImageResource(R.drawable.ic_indoor)
                binding.envTV.text = "Indoor"
            }
        }

        when (mHarvestPlantModel.plant_stage) {
            "Flowering" -> {
                binding.stageTV.text = "Flowering"
                binding.stageIV.setImageResource(R.drawable.ic_flowering)
            }
            "Germination" -> {
                binding.stageTV.text = "Germination"
                binding.stageIV.setImageResource(R.drawable.ic_germination)
            }
            "Seedling" -> {
                binding.stageTV.text = "Seedling"
                binding.stageIV.setImageResource(R.drawable.ic_seedling)
            }
            "Vegetative" -> {
                binding.stageTV.text = "Vegetative"
                binding.stageIV.setImageResource(R.drawable.ic_vegetative)
            }
        }

        val mCurrentDate = SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()).format(Date())
        val sdf = SimpleDateFormat("dd/MM/yyyy")
        val dateString=convertTimestampInDate(mHarvestPlantModel.creation_date.toString())
        val date1 = sdf.parse(dateString)
        val date2 = sdf.parse(mCurrentDate)
        val diff: Long = date2.time - date1.time
        val seconds = diff / 1000
        val minutes = seconds / 60
        val hours = minutes / 60
        var days = hours / 24
        days += 1


        if(days.toInt()==1) {
            binding.daysTV.text = "$days day"
        }else{
            binding.daysTV.text = "$days days"
        }
        val db = DBQuery(requireContext())
        db.open()
        val initPlantStage=db.getInitPlantStageFromPlantList(mHarvestPlantModel.item_id!!)
        db.close()

        when (initPlantStage!![0]) {
            "Flowering" -> {
                binding.stageHeadingTV.text = "Flowering stage started"
            }
            "Germination" -> {
                binding.stageHeadingTV.text = "Germination stage started"
            }
            "Seedling" -> {
                binding.stageHeadingTV.text = "Seedling stage started"
            }
            "Vegetative" -> {
                binding.stageHeadingTV.text = "Vegetative stage started"
            }
        }

        when (mHarvestPlantModel.plant_env) {
            "Outdoor" -> {
                binding.imgEnvIV.setImageResource(R.drawable.ic_outdoor)
                binding.envHeadingTV.text = "Outdoor"
            }
            "Indoor" -> {
                binding.imgEnvIV.setImageResource(R.drawable.ic_indoor)
                binding.envHeadingTV.text = "Indoor"
            }
        }
    }

}