package com.stronglifts.sharedViewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.stronglifts.model.GrowingPlantsModel


class EditGrowingPlantSharedViewModel : ViewModel() {
    private val selected = MutableLiveData< ArrayList<GrowingPlantsModel>>()


    fun setSelected(item: ArrayList<GrowingPlantsModel>) {
        selected.value = item
    }


    fun getSelected(): LiveData< ArrayList<GrowingPlantsModel>> {
        return selected
    }
}