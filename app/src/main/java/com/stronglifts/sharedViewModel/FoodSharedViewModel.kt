package com.stronglifts.sharedViewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.stronglifts.model.ActionModel
import com.stronglifts.model.FoodModel
import com.stronglifts.model.GrowingPlantsModel


class FoodSharedViewModel : ViewModel() {
    private val selected = MutableLiveData< ArrayList<FoodModel>>()


    fun setSelected(item: ArrayList<FoodModel>) {
        selected.value = item
    }


    fun getSelected(): LiveData< ArrayList<FoodModel>> {
        return selected
    }
}