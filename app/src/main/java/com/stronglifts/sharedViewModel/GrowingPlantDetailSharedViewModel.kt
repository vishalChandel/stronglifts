package com.stronglifts.sharedViewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.stronglifts.model.GrowingPlantsModel
import com.stronglifts.model.TreeLogModel


class GrowingPlantDetailSharedViewModel : ViewModel() {
    private val selected = MutableLiveData<ArrayList<TreeLogModel>>()


    fun setSelected(item: ArrayList<TreeLogModel>) {
        selected.value = item
    }


    fun getSelected(): LiveData< ArrayList<TreeLogModel>> {
        return selected
    }
}