package com.stronglifts.sharedViewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.stronglifts.model.ActionModel
import com.stronglifts.model.GrowingPlantsModel


class EditGrowingPlantActionSharedViewModel : ViewModel() {
    private val selected = MutableLiveData< ArrayList<ActionModel>>()


    fun setSelected(item: ArrayList<ActionModel>) {
        selected.value = item
    }


    fun getSelected(): LiveData< ArrayList<ActionModel>> {
        return selected
    }
}