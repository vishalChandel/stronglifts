package com.stronglifts.sharedViewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.stronglifts.model.ActionModel
import com.stronglifts.model.EnvLogModel
import com.stronglifts.model.FoodModel
import com.stronglifts.model.GrowingPlantsModel


class EnvLogSharedViewModel : ViewModel() {
    private val selected = MutableLiveData< ArrayList<EnvLogModel>>()


    fun setSelected(item: ArrayList<EnvLogModel>) {
        selected.value = item
    }


    fun getSelected(): LiveData< ArrayList<EnvLogModel>> {
        return selected
    }
}