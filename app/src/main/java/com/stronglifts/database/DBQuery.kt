package com.stronglifts.database

import android.content.Context
import android.database.SQLException
import android.database.sqlite.SQLiteDatabase
import com.stronglifts.model.*
import java.io.IOException

class DBQuery(context: Context?) {
    private val dbHelper: DBHelper = DBHelper(context!!)
    private var db: SQLiteDatabase? = null

    @Throws(SQLException::class)
    fun createDatabase(): DBQuery {
        try {
            dbHelper.createDataBase()
        } catch (ignored: IOException) {
        }
        return this
    }

    @Throws(SQLException::class)
    fun open(): DBQuery {
        try {
            dbHelper.openDataBase()
            dbHelper.close()
            db = dbHelper.readableDatabase
        } catch (ignored: SQLException) {
        }
        return this
    }

    fun close() {
        dbHelper.close()
    }

    //______________________________________________________________________________________________
    //__________________________________      QUERIES      ________________________________________
    //______________________________________________________________________________________________
    fun getPlantsList(): ArrayList<PlantsModel>? {
        val sql = "SELECT * FROM plants"
        val cursor = db!!.rawQuery(sql, null)
        try {
            val mList = ArrayList<PlantsModel>()
            if (cursor.moveToFirst()) {
                do {
                    // on below line we are adding the data from cursor to our array list.
                    mList.add(
                        PlantsModel(
                            cursor.getString(0),
                            cursor.getString(1),
                            cursor.getString(2),
                            cursor.getString(3),
                            cursor.getString(4),
                            cursor.getString(5),
                            cursor.getString(6),
                            cursor.getString(7),
                            cursor.getString(8),
                            cursor.getString(9),
                            cursor.getString(10),
                            cursor.getString(11),
                            cursor.getString(12),
                            cursor.getString(13),
                            cursor.getString(14),
                            cursor.getString(15),
                            cursor.getString(16),
                            cursor.getString(17),
                            cursor.getString(18),
                            cursor.getString(19),
                            cursor.getString(19)
                        )
                    )
                } while (cursor.moveToNext())
            }
            return mList
        } catch (ignored: Exception) {
        } finally {
            cursor.close()
        }
        return null
    }

    fun getPlantsCategoryList(): ArrayList<String>? {
        val sql = "SELECT category FROM plants"
        val cursor = db!!.rawQuery(sql, null)
        try {
            val categoriesName = ArrayList<String>()
            if (cursor.moveToFirst()) {
                do {
                    categoriesName.add(cursor.getString(0))
                } while (cursor.moveToNext())
            }
            return categoriesName
        } catch (ignored: java.lang.Exception) {
        } finally {
            cursor.close()
        }
        return null
    }

    fun getInitPlantStageFromPlantList(itemId:String): ArrayList<String>? {
        val sql = "SELECT plant_init_stage FROM plant_list WHERE item_id='$itemId'"
        val cursor = db!!.rawQuery(sql, null)
        try {
            val initPlantStage = ArrayList<String>()
            if (cursor.moveToFirst()) {
                do {
                    initPlantStage.add(cursor.getString(0))
                } while (cursor.moveToNext())
            }
            return initPlantStage
        } catch (ignored: java.lang.Exception) {
        } finally {
            cursor.close()
        }
        return null
    }

    fun getPlantsListByCategory(mCategory: String): ArrayList<PlantsModel>? {
        val sql = "SELECT * FROM plants WHERE category='$mCategory'"
        val cursor = db!!.rawQuery(sql, null)
        try {
            val mList = ArrayList<PlantsModel>()
            if (cursor.moveToFirst()) {
                do {
                    mList.add(
                        PlantsModel(
                            cursor.getString(0),
                            cursor.getString(1),
                            cursor.getString(2),
                            cursor.getString(3),
                            cursor.getString(4),
                            cursor.getString(5),
                            cursor.getString(6),
                            cursor.getString(7),
                            cursor.getString(8),
                            cursor.getString(9),
                            cursor.getString(10),
                            cursor.getString(11),
                            cursor.getString(12),
                            cursor.getString(13),
                            cursor.getString(14),
                            cursor.getString(15),
                            cursor.getString(16),
                            cursor.getString(17),
                            cursor.getString(18),
                            cursor.getString(19),
                            cursor.getString(19)
                        )
                    )
                } while (cursor.moveToNext())
            }
            return mList
        } catch (ignored: Exception) {
        } finally {
            cursor.close()
        }
        return null
    }

    fun getAddedPlantsListByCategory(mCategory:String): ArrayList<PlantsModel>? {
        val mSecondSQL = "SELECT plants.*,plant_list.item_id FROM plants,plant_list where plants.plant_id=plant_list.plant_id and plant_list.plant_type=0 and plants.category='$mCategory'"
        val cursor = db!!.rawQuery(mSecondSQL, null)
        try {
            val mList = ArrayList<PlantsModel>()
            if (cursor.moveToFirst()) {
                do {
                    // on below line we are adding the data from cursor to our array list.
                    mList.add(
                        PlantsModel(
                            cursor.getString(0),
                            cursor.getString(1),
                            cursor.getString(2),
                            cursor.getString(3),
                            cursor.getString(4),
                            cursor.getString(5),
                            cursor.getString(6),
                            cursor.getString(7),
                            cursor.getString(8),
                            cursor.getString(9),
                            cursor.getString(10),
                            cursor.getString(11),
                            cursor.getString(12),
                            cursor.getString(13),
                            cursor.getString(14),
                            cursor.getString(15),
                            cursor.getString(16),
                            cursor.getString(17),
                            cursor.getString(18),
                            cursor.getString(19),
                            cursor.getString(20)
                        )
                    )
                } while (cursor.moveToNext())
            }
            return mList
        } catch (ignored: Exception) {
        } finally {
            cursor.close()
        }
        return null
    }

    fun getAddedPlantsList(): ArrayList<PlantsModel>? {
        val mSecondSQL = "SELECT plants.*,plant_list.item_id FROM plants,plant_list where plants.plant_id=plant_list.plant_id and plant_list.plant_type=0"
        val cursor = db!!.rawQuery(mSecondSQL, null)
        try {
            val mList = ArrayList<PlantsModel>()
            if (cursor.moveToFirst()) {
                do {
                    // on below line we are adding the data from cursor to our array list.
                    mList.add(
                        PlantsModel(
                            cursor.getString(0),
                            cursor.getString(1),
                            cursor.getString(2),
                            cursor.getString(3),
                            cursor.getString(4),
                            cursor.getString(5),
                            cursor.getString(6),
                            cursor.getString(7),
                            cursor.getString(8),
                            cursor.getString(9),
                            cursor.getString(10),
                            cursor.getString(11),
                            cursor.getString(12),
                            cursor.getString(13),
                            cursor.getString(14),
                            cursor.getString(15),
                            cursor.getString(16),
                            cursor.getString(17),
                            cursor.getString(18),
                            cursor.getString(19),
                            cursor.getString(20)
                        )
                    )
                } while (cursor.moveToNext())
            }
            return mList
        } catch (ignored: Exception) {
        } finally {
            cursor.close()
        }
        return null
    }

    fun getFoodList(): ArrayList<FoodModel>? {
        val sql = "SELECT * FROM foods"
        val cursor = db!!.rawQuery(sql, null)
        try {
            val mList = ArrayList<FoodModel>()
            if (cursor.moveToFirst()) {
                do {
                    // on below line we are adding the data from cursor to our array list.
                    mList.add(
                        FoodModel(
                            cursor.getString(0),
                            cursor.getString(1),
                            cursor.getString(2),
                            cursor.getString(3)
                        )
                    )
                } while (cursor.moveToNext())
            }
            return mList
        } catch (ignored: Exception) {
        } finally {
            cursor.close()
        }
        return null
    }

    fun getGrowingPlantsList(): ArrayList<GrowingPlantsModel>? {
        val mSQL = "SELECT plants.plant_id,plants.name,plants.thumbnailImage,plant_list.item_id,plant_list.plant_alt_name,plant_list.plant_stage,plant_list.plant_env,plant_list.creation_date FROM plants,plant_list where plants.plant_id=plant_list.plant_id and plant_list.plant_type=1"
        val cursor = db!!.rawQuery(mSQL, null)
        try {
            val mList = ArrayList<GrowingPlantsModel>()
            if (cursor.moveToFirst()) {
                do {
                    // on below line we are adding the data from cursor to our array list.
                    mList.add(
                        GrowingPlantsModel(
                            cursor.getString(0),
                            cursor.getString(1),
                            cursor.getString(2),
                            cursor.getString(3),
                            cursor.getString(4),
                            cursor.getString(5),
                            cursor.getString(6),
                            cursor.getString(7)
                        )
                    )
                } while (cursor.moveToNext())
            }
            return mList
        } catch (ignored: Exception) {
        } finally {
            cursor.close()
        }
        return null
    }

    fun getGrowingHarvestingPlantsList(): ArrayList<GrowingPlantsModel>? {
        val mSQL = "SELECT plants.plant_id,plants.name,plants.thumbnailImage,plant_list.item_id,plant_list.plant_alt_name,plant_list.plant_stage,plant_list.plant_env,plant_list.creation_date FROM plants,plant_list where plants.plant_id=plant_list.plant_id "
        val cursor = db!!.rawQuery(mSQL, null)
        try {
            val mList = ArrayList<GrowingPlantsModel>()
            if (cursor.moveToFirst()) {
                do {
                    // on below line we are adding the data from cursor to our array list.
                    mList.add(
                        GrowingPlantsModel(
                            cursor.getString(0),
                            cursor.getString(1),
                            cursor.getString(2),
                            cursor.getString(3),
                            cursor.getString(4),
                            cursor.getString(5),
                            cursor.getString(6),
                            cursor.getString(7)
                        )
                    )
                } while (cursor.moveToNext())
            }
            return mList
        } catch (ignored: Exception) {
        } finally {
            cursor.close()
        }
        return null
    }

    fun getHarvestPlantsList(): ArrayList<GrowingPlantsModel>? {
        val mSQL = "SELECT plants.plant_id,plants.name,plants.thumbnailImage,plant_list.item_id,plant_list.plant_alt_name,plant_list.plant_stage,plant_list.plant_env,plant_list.creation_date FROM plants,plant_list where plants.plant_id=plant_list.plant_id and plant_list.plant_type=2"
        val cursor = db!!.rawQuery(mSQL, null)
        try {
            val mList = ArrayList<GrowingPlantsModel>()
            if (cursor.moveToFirst()) {
                do {
                    // on below line we are adding the data from cursor to our array list.
                    mList.add(
                        GrowingPlantsModel(
                            cursor.getString(0),
                            cursor.getString(1),
                            cursor.getString(2),
                            cursor.getString(3),
                            cursor.getString(4),
                            cursor.getString(5),
                            cursor.getString(6),
                            cursor.getString(7)
                        )
                    )
                } while (cursor.moveToNext())
            }
            return mList
        } catch (ignored: Exception) {
        } finally {
            cursor.close()
        }
        return null
    }

    fun getTreeLogList(mSelectedDate:String): ArrayList<TreeLogModel>? {
        val sql = "SELECT tree_log.* FROM tree_log,plant_list where tree_log.log_date ='$mSelectedDate' and plant_list.plant_type=1 and tree_log.plant_id=plant_list.item_id"
        val cursor = db!!.rawQuery(sql, null)
        try {
            val mList = ArrayList<TreeLogModel>()
            if (cursor.moveToFirst()) {
                do {
                    // on below line we are adding the data from cursor to our array list.
                    mList.add(
                        TreeLogModel(
                            cursor.getString(0),
                            cursor.getString(1),
                            cursor.getString(2),
                            cursor.getString(3)
                        )
                    )
                } while (cursor.moveToNext())
            }
            return mList
        } catch (ignored: Exception) {
        } finally {
            cursor.close()
        }
        return null
    }

    fun getEnvLogList(mSelectedDate:String): ArrayList<EnvLogModel>? {
        val sql = "SELECT * FROM environment_log where env_log_date  ='$mSelectedDate'"
        val cursor = db!!.rawQuery(sql, null)
        try {
            val mList = ArrayList<EnvLogModel>()
            if (cursor.moveToFirst()) {
                do {
                    // on below line we are adding the data from cursor to our array list.
                    mList.add(
                        EnvLogModel(
                            cursor.getString(0),
                            cursor.getString(1),
                            cursor.getString(2),
                            cursor.getString(3)
                        )
                    )
                } while (cursor.moveToNext())
            }
            return mList
        } catch (ignored: Exception) {
        } finally {
            cursor.close()
        }
        return null
    }

    fun getTreeLogListByGrowingPlant(mSelectedDate:String,mGrowingPlantId:String): ArrayList<TreeLogModel>? {
        val sql = "SELECT tree_log.* FROM  tree_log,plant_list where tree_log.log_date ='$mSelectedDate' and plant_list.plant_type=1 and tree_log.plant_id='$mGrowingPlantId'  and tree_log.plant_id=plant_list.item_id"
        val cursor = db!!.rawQuery(sql, null)
        try {
            val mList = ArrayList<TreeLogModel>()
            if (cursor.moveToFirst()) {
                do {
                    // on below line we are adding the data from cursor to our array list.
                    mList.add(
                        TreeLogModel(
                            cursor.getString(0),
                            cursor.getString(1),
                            cursor.getString(2),
                            cursor.getString(3)
                        )
                    )
                } while (cursor.moveToNext())
            }
            return mList
        } catch (ignored: Exception) {
        } finally {
            cursor.close()
        }
        return null
    }

    fun getEnvLogListByEnvironment(mSelectedDate:String,mEnv:String): ArrayList<EnvLogModel>? {
        val sql = "SELECT * FROM environment_log where env_log_date  ='$mSelectedDate' and env_name='$mEnv'"
        val cursor = db!!.rawQuery(sql, null)
        try {
            val mList = ArrayList<EnvLogModel>()
            if (cursor.moveToFirst()) {
                do {
                    // on below line we are adding the data from cursor to our array list.
                    mList.add(
                        EnvLogModel(
                            cursor.getString(0),
                            cursor.getString(1),
                            cursor.getString(2),
                            cursor.getString(3)
                        )
                    )
                } while (cursor.moveToNext())
            }
            return mList
        } catch (ignored: Exception) {
        } finally {
            cursor.close()
        }
        return null
    }

    fun getGrowingPlantNameList(mGrowingPlantId: String): ArrayList<GrowingPlantsModel>? {
        val mSQL = "SELECT plants.plant_id,plants.name,plants.thumbnailImage,plant_list.item_id,plant_list.plant_alt_name,plant_list.plant_stage,plant_list.plant_env,plant_list.creation_date FROM plants,plant_list where plants.plant_id=plant_list.plant_id and plant_list.item_id='$mGrowingPlantId'"
        val cursor = db!!.rawQuery(mSQL, null)
        try {
            val mList = ArrayList<GrowingPlantsModel>()
            if (cursor.moveToFirst()) {
                do {
                    // on below line we are adding the data from cursor to our array list.
                    mList.add(
                        GrowingPlantsModel(
                            cursor.getString(0),
                            cursor.getString(1),
                            cursor.getString(2),
                            cursor.getString(3),
                            cursor.getString(4),
                            cursor.getString(5),
                            cursor.getString(6),
                            cursor.getString(7)
                        )
                    )
                } while (cursor.moveToNext())
            }
            return mList
        } catch (ignored: Exception) {
        } finally {
            cursor.close()
        }
        return null
    }

    fun getActionList(mSelectedDate:String): ArrayList<ActionModel>? {
        val sql = "SELECT actions.* FROM actions,plant_list where actions.ac_date ='$mSelectedDate' and actions.ac_repeat_till='' and actions.ac_repeat='' and plant_list.plant_type=1 and actions.ac_plant_id =plant_list.item_id"
        val cursor = db!!.rawQuery(sql, null)
        try {
            val mList = ArrayList<ActionModel>()
            if (cursor.moveToFirst()) {
                do {
                    // on below line we are adding the data from cursor to our array list.
                    mList.add(
                        ActionModel(
                            cursor.getString(0),
                            cursor.getString(1),
                            cursor.getString(2),
                            cursor.getString(3),
                            cursor.getString(4),
                            cursor.getString(5),
                            cursor.getString(6),
                            cursor.getString(7),
                            cursor.getString(8),
                            cursor.getString(9),
                            cursor.getString(10),
                            cursor.getString(11),
                            cursor.getString(12),
                            cursor.getString(13)
                        )
                    )
                } while (cursor.moveToNext())
            }
            return mList
        } catch (ignored: Exception) {
        } finally {
            cursor.close()
        }
        return null
    }

    fun getActionListByPlant(mSelectedDate:String,mGrowingPlantId:String): ArrayList<ActionModel>? {
        val sql = "SELECT actions.* FROM actions,plant_list where actions.ac_date ='$mSelectedDate' and actions.ac_plant_id='$mGrowingPlantId' and actions.ac_repeat_till ='' and actions.ac_repeat='' and plant_list.plant_type=1  and actions.ac_plant_id =plant_list.item_id"
        val cursor = db!!.rawQuery(sql, null)
        try {
            val mList = ArrayList<ActionModel>()
            if (cursor.moveToFirst()) {
                do {
                    // on below line we are adding the data from cursor to our array list.
                    mList.add(
                        ActionModel(
                            cursor.getString(0),
                            cursor.getString(1),
                            cursor.getString(2),
                            cursor.getString(3),
                            cursor.getString(4),
                            cursor.getString(5),
                            cursor.getString(6),
                            cursor.getString(7),
                            cursor.getString(8),
                            cursor.getString(9),
                            cursor.getString(10),
                            cursor.getString(11),
                            cursor.getString(12),
                            cursor.getString(13)
                        )
                    )
                } while (cursor.moveToNext())
            }
            return mList
        } catch (ignored: Exception) {
        } finally {
            cursor.close()
        }
        return null
    }

    fun getFoodListByFoodId(food_id:String): ArrayList<FoodModel>? {
        val sql = "SELECT * FROM foods where food_id='$food_id'"
        val cursor = db!!.rawQuery(sql, null)
        try {
            val mList = ArrayList<FoodModel>()
            if (cursor.moveToFirst()) {
                do {
                    // on below line we are adding the data from cursor to our array list.
                    mList.add(
                        FoodModel(
                            cursor.getString(0),
                            cursor.getString(1),
                            cursor.getString(2),
                            cursor.getString(3)
                        )
                    )
                } while (cursor.moveToNext())
            }
            return mList
        } catch (ignored: Exception) {
        } finally {
            cursor.close()
        }
        return null
    }

    fun getActionListByRepeatType(): ArrayList<ActionModel>? {
        val sql = "SELECT actions.* FROM actions,plant_list where actions.ac_repeat ='1' and actions.ac_repeat_type='0' and actions.ac_repeat_days='1' and actions.ac_repeat_till ='' and plant_list.plant_type=1  and actions.ac_plant_id =plant_list.item_id"
        val cursor = db!!.rawQuery(sql, null)
        try {
            val mList = ArrayList<ActionModel>()
            if (cursor.moveToFirst()) {
                do {
                    // on below line we are adding the data from cursor to our array list.
                    mList.add(
                        ActionModel(
                            cursor.getString(0),
                            cursor.getString(1),
                            cursor.getString(2),
                            cursor.getString(3),
                            cursor.getString(4),
                            cursor.getString(5),
                            cursor.getString(6),
                            cursor.getString(7),
                            cursor.getString(8),
                            cursor.getString(9),
                            cursor.getString(10),
                            cursor.getString(11),
                            cursor.getString(12),
                            cursor.getString(13)
                        )
                    )
                } while (cursor.moveToNext())
            }
            return mList
        } catch (ignored: Exception) {
        } finally {
            cursor.close()
        }
        return null
    }

    fun getActionListByRepeatType2(mGrowingPlantId:String): ArrayList<ActionModel>? {
        val sql = "SELECT actions.* FROM actions,plant_list where actions.ac_repeat ='1' and actions.ac_repeat_type='0' and actions.ac_repeat_days='1'  and actions.ac_plant_id='$mGrowingPlantId' and actions.ac_repeat_till ='' and plant_list.plant_type=1  and actions.ac_plant_id =plant_list.item_id"
        val cursor = db!!.rawQuery(sql, null)
        try {
            val mList = ArrayList<ActionModel>()
            if (cursor.moveToFirst()) {
                do {
                    // on below line we are adding the data from cursor to our array list.
                    mList.add(
                        ActionModel(
                            cursor.getString(0),
                            cursor.getString(1),
                            cursor.getString(2),
                            cursor.getString(3),
                            cursor.getString(4),
                            cursor.getString(5),
                            cursor.getString(6),
                            cursor.getString(7),
                            cursor.getString(8),
                            cursor.getString(9),
                            cursor.getString(10),
                            cursor.getString(11),
                            cursor.getString(12),
                            cursor.getString(13)
                        )
                    )
                } while (cursor.moveToNext())
            }
            return mList
        } catch (ignored: Exception) {
        } finally {
            cursor.close()
        }
        return null
    }

    fun getActionListByRepeatTypeUntilDate(): ArrayList<ActionModel>? {
        val sql = "SELECT actions.* FROM actions,plant_list where actions.ac_repeat ='1' and actions.ac_repeat_type='1' and actions.ac_repeat_days='1' and plant_list.plant_type=1  and actions.ac_plant_id =plant_list.item_id "
        val cursor = db!!.rawQuery(sql, null)
        try {
            val mList = ArrayList<ActionModel>()
            if (cursor.moveToFirst()) {
                do {
                    // on below line we are adding the data from cursor to our array list.
                    mList.add(
                        ActionModel(
                            cursor.getString(0),
                            cursor.getString(1),
                            cursor.getString(2),
                            cursor.getString(3),
                            cursor.getString(4),
                            cursor.getString(5),
                            cursor.getString(6),
                            cursor.getString(7),
                            cursor.getString(8),
                            cursor.getString(9),
                            cursor.getString(10),
                            cursor.getString(11),
                            cursor.getString(12),
                            cursor.getString(13)
                        )
                    )
                } while (cursor.moveToNext())
            }
            return mList
        } catch (ignored: Exception) {
        } finally {
            cursor.close()
        }
        return null
    }

    fun getActionListByRepeatTypeUntilDate2(mGrowingPlantId:String): ArrayList<ActionModel>? {
        val sql = "SELECT actions.* FROM actions,plant_list where actions.ac_repeat ='1' and actions.ac_repeat_type='1' and actions.ac_repeat_days='1'  and actions.ac_plant_id='$mGrowingPlantId' and plant_list.plant_type=1  and actions.ac_plant_id =plant_list.item_id"
        val cursor = db!!.rawQuery(sql, null)
        try {
            val mList = ArrayList<ActionModel>()
            if (cursor.moveToFirst()) {
                do {
                    // on below line we are adding the data from cursor to our array list.
                    mList.add(
                        ActionModel(
                            cursor.getString(0),
                            cursor.getString(1),
                            cursor.getString(2),
                            cursor.getString(3),
                            cursor.getString(4),
                            cursor.getString(5),
                            cursor.getString(6),
                            cursor.getString(7),
                            cursor.getString(8),
                            cursor.getString(9),
                            cursor.getString(10),
                            cursor.getString(11),
                            cursor.getString(12),
                            cursor.getString(13)
                        )
                    )
                } while (cursor.moveToNext())
            }
            return mList
        } catch (ignored: Exception) {
        } finally {
            cursor.close()
        }
        return null
    }


    fun getActionListAll(): ArrayList<ActionModel>? {
        val sql = "SELECT actions.* FROM actions,plant_list WHERE plant_list.plant_type=1  and actions.ac_plant_id =plant_list.item_id"
        val cursor = db!!.rawQuery(sql, null)
        try {
            val mList = ArrayList<ActionModel>()
            if (cursor.moveToFirst()) {
                do {
                    // on below line we are adding the data from cursor to our array list.
                    mList.add(
                        ActionModel(
                            cursor.getString(0),
                            cursor.getString(1),
                            cursor.getString(2),
                            cursor.getString(3),
                            cursor.getString(4),
                            cursor.getString(5),
                            cursor.getString(6),
                            cursor.getString(7),
                            cursor.getString(8),
                            cursor.getString(9),
                            cursor.getString(10),
                            cursor.getString(11),
                            cursor.getString(12),
                            cursor.getString(13)
                        )
                    )
                } while (cursor.moveToNext())
            }
            return mList
        } catch (ignored: Exception) {
        } finally {
            cursor.close()
        }
        return null
    }

    fun getActionListAll2(mGrowingPlantId:String): ArrayList<ActionModel>? {
        val sql = "SELECT actions.* FROM actions,plant_list WHERE actions.ac_plant_id='$mGrowingPlantId' and plant_list.plant_type=1  and actions.ac_plant_id =plant_list.item_id"
        val cursor = db!!.rawQuery(sql, null)
        try {
            val mList = ArrayList<ActionModel>()
            if (cursor.moveToFirst()) {
                do {
                    // on below line we are adding the data from cursor to our array list.
                    mList.add(
                        ActionModel(
                            cursor.getString(0),
                            cursor.getString(1),
                            cursor.getString(2),
                            cursor.getString(3),
                            cursor.getString(4),
                            cursor.getString(5),
                            cursor.getString(6),
                            cursor.getString(7),
                            cursor.getString(8),
                            cursor.getString(9),
                            cursor.getString(10),
                            cursor.getString(11),
                            cursor.getString(12),
                            cursor.getString(13)
                        )
                    )
                } while (cursor.moveToNext())
            }
            return mList
        } catch (ignored: Exception) {
        } finally {
            cursor.close()
        }
        return null
    }


    fun getActionListAllByCurrentDate(mCurrentDate:String): ArrayList<ActionModel>? {
        val sql = "SELECT actions.* FROM actions,plant_list where actions.ac_date <='$mCurrentDate' and actions.ac_type = 'Harvest' and plant_list.plant_type=1  and actions.ac_plant_id =plant_list.item_id or actions.ac_date <='$mCurrentDate' and actions.ac_type = 'Declare Death' and plant_list.plant_type=1  and actions.ac_plant_id =plant_list.item_id"
        val cursor = db!!.rawQuery(sql, null)
        try {
            val mList = ArrayList<ActionModel>()
            if (cursor.moveToFirst()) {
                do {
                    // on below line we are adding the data from cursor to our array list.
                    mList.add(
                        ActionModel(
                            cursor.getString(0),
                            cursor.getString(1),
                            cursor.getString(2),
                            cursor.getString(3),
                            cursor.getString(4),
                            cursor.getString(5),
                            cursor.getString(6),
                            cursor.getString(7),
                            cursor.getString(8),
                            cursor.getString(9),
                            cursor.getString(10),
                            cursor.getString(11),
                            cursor.getString(12),
                            cursor.getString(13)
                        )
                    )
                } while (cursor.moveToNext())
            }
            return mList
        } catch (ignored: Exception) {
        } finally {
            cursor.close()
        }
        return null
    }

    fun getChangeStateActionList(mCurrentDate:String): ArrayList<ActionModel>? {
        val sql = "SELECT actions.* FROM actions,plant_list where actions.ac_date <='$mCurrentDate' and actions.ac_type = 'Change State' and plant_list.plant_type=1  and actions.ac_plant_id =plant_list.item_id"
        val cursor = db!!.rawQuery(sql, null)
        try {
            val mList = ArrayList<ActionModel>()
            if (cursor.moveToFirst()) {
                do {
                    // on below line we are adding the data from cursor to our array list.
                    mList.add(
                        ActionModel(
                            cursor.getString(0),
                            cursor.getString(1),
                            cursor.getString(2),
                            cursor.getString(3),
                            cursor.getString(4),
                            cursor.getString(5),
                            cursor.getString(6),
                            cursor.getString(7),
                            cursor.getString(8),
                            cursor.getString(9),
                            cursor.getString(10),
                            cursor.getString(11),
                            cursor.getString(12),
                            cursor.getString(13)
                        )
                    )
                } while (cursor.moveToNext())
            }
            return mList
        } catch (ignored: Exception) {
        } finally {
            cursor.close()
        }
        return null
    }

    fun getActionListForHarvestPlant(mGrowingPlantId:String): ArrayList<ActionModel>? {
        val sql = "SELECT * FROM actions WHERE ac_plant_id='$mGrowingPlantId'"
        val cursor = db!!.rawQuery(sql, null)
        try {
            val mList = ArrayList<ActionModel>()
            if (cursor.moveToFirst()) {
                do {
                    // on below line we are adding the data from cursor to our array list.
                    mList.add(
                        ActionModel(
                            cursor.getString(0),
                            cursor.getString(1),
                            cursor.getString(2),
                            cursor.getString(3),
                            cursor.getString(4),
                            cursor.getString(5),
                            cursor.getString(6),
                            cursor.getString(7),
                            cursor.getString(8),
                            cursor.getString(9),
                            cursor.getString(10),
                            cursor.getString(11),
                            cursor.getString(12),
                            cursor.getString(13)
                        )
                    )
                } while (cursor.moveToNext())
            }
            return mList
        } catch (ignored: Exception) {
        } finally {
            cursor.close()
        }
        return null
    }

    fun getActionListForHarvestPlantByHarvestDate(mGrowingPlantId:String,ac_date:String): ArrayList<ActionModel>? {
        val sql = "SELECT * FROM actions WHERE ac_plant_id='$mGrowingPlantId' and ac_date <= '$ac_date'"
        val cursor = db!!.rawQuery(sql, null)
        try {
            val mList = ArrayList<ActionModel>()
            if (cursor.moveToFirst()) {
                do {
                    // on below line we are adding the data from cursor to our array list.
                    mList.add(
                        ActionModel(
                            cursor.getString(0),
                            cursor.getString(1),
                            cursor.getString(2),
                            cursor.getString(3),
                            cursor.getString(4),
                            cursor.getString(5),
                            cursor.getString(6),
                            cursor.getString(7),
                            cursor.getString(8),
                            cursor.getString(9),
                            cursor.getString(10),
                            cursor.getString(11),
                            cursor.getString(12),
                            cursor.getString(13)
                        )
                    )
                } while (cursor.moveToNext())
            }
            return mList
        } catch (ignored: Exception) {
        } finally {
            cursor.close()
        }
        return null
    }

    fun getEnvLogListForHarvestPlant(mEnv:String,env_log_date:String): ArrayList<EnvLogModel>? {
        val sql = "SELECT * FROM environment_log where env_name='$mEnv' and env_log_date <='$env_log_date'"
        val cursor = db!!.rawQuery(sql, null)
        try {
            val mList = ArrayList<EnvLogModel>()
            if (cursor.moveToFirst()) {
                do {
                    // on below line we are adding the data from cursor to our array list.
                    mList.add(
                        EnvLogModel(
                            cursor.getString(0),
                            cursor.getString(1),
                            cursor.getString(2),
                            cursor.getString(3)
                        )
                    )
                } while (cursor.moveToNext())
            }
            return mList
        } catch (ignored: Exception) {
        } finally {
            cursor.close()
        }
        return null
    }

    fun getTreeLogListForHarvestPlant(mGrowingPlantId:String,log_date:String): ArrayList<TreeLogModel>? {
        val sql = "SELECT * FROM  tree_log where plant_id='$mGrowingPlantId' and log_date <='$log_date'"
        val cursor = db!!.rawQuery(sql, null)
        try {
            val mList = ArrayList<TreeLogModel>()
            if (cursor.moveToFirst()) {
                do {
                    // on below line we are adding the data from cursor to our array list.
                    mList.add(
                        TreeLogModel(
                            cursor.getString(0),
                            cursor.getString(1),
                            cursor.getString(2),
                            cursor.getString(3)
                        )
                    )
                } while (cursor.moveToNext())
            }
            return mList
        } catch (ignored: Exception) {
        } finally {
            cursor.close()
        }
        return null
    }

    fun getActionStatus(ac_id:String,ac_status_date:String): ArrayList<ActionStatusModel>? {
        val sql = "SELECT * FROM action_status where ac_id='$ac_id' and ac_status_date='$ac_status_date'"
        val cursor = db!!.rawQuery(sql, null)
        try {
            val mList = ArrayList<ActionStatusModel>()
            if (cursor.moveToFirst()) {
                do {
                    // on below line we are adding the data from cursor to our array list.
                    mList.add(
                        ActionStatusModel(
                            cursor.getString(0),
                            cursor.getString(1),
                            cursor.getString(2)
                        )
                    )
                } while (cursor.moveToNext())
            }
            return mList
        } catch (ignored: Exception) {
        } finally {
            cursor.close()
        }
        return null
    }
}
