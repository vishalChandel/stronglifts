package com.stronglifts.database

import android.content.ContentValues
import android.content.Context
import android.database.SQLException
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.io.OutputStream


class DBHelper(context: Context) :
    SQLiteOpenHelper(context, DB_NAME, null, DB_VERSION) {
    private var mDataBase: SQLiteDatabase? = null
    private val ctx: Context

    @Throws(IOException::class)
    fun createDataBase() {
        if (!checkDataBase()) {
            this.readableDatabase
            copyDataBase()
            close()
        }
    }

    private fun checkDataBase(): Boolean {
        val DbFile = File(DB_PATH + DB_NAME)
        return DbFile.exists()
    }

    @Throws(SQLException::class)
    fun openDataBase(): Boolean {
        mDataBase = SQLiteDatabase.openDatabase(DB_PATH, null, SQLiteDatabase.CREATE_IF_NECESSARY)
        return mDataBase != null
    }

    @Synchronized
    override fun close() {
        if (mDataBase != null) mDataBase!!.close()
        SQLiteDatabase.releaseMemory()
        super.close()
    }

    override fun onCreate(db: SQLiteDatabase) {
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
    }

    @Throws(IOException::class)
    private fun copyDataBase() {
        val mInput = ctx.assets.open(DB_NAME)
        val outfileName = DB_PATH
        val mOutput: OutputStream = FileOutputStream(outfileName)
        val buffer = ByteArray(1024)
        var mLength: Int
        while (mInput.read(buffer).also { mLength = it } > 0) {
            mOutput.write(buffer, 0, mLength)
        }
        mOutput.flush()
        mInput.close()
        mOutput.close()
    }

    companion object {
        private var DB_PATH = ""
        private const val DB_NAME = "gardenLife.sqlite"
        private const val DB_VERSION = 1
    }

    init {
        DB_PATH = context.getDatabasePath(DB_NAME).path
        ctx = context
    }

    // - - Create Tables for database
    fun createTables() {
        val mDataBase = this.writableDatabase

        val query0 = "CREATE TABLE `plant_list` (\n" +
                "  `item_id` INTEGER PRIMARY KEY AUTOINCREMENT,\n" +
                "  `plant_id` TEXT,\n" +
                "  `plant_type` TEXT,\n" +
                "  `plant_alt_name` TEXT ,\n" +
                "  `plant_init_stage` TEXT,\n" +
                "  `plant_stage` TEXT,\n" +
                "  `plant_env` TEXT,\n" +
                "  `creation_date` TEXT \n" +
                ")"

        val query1 = "CREATE TABLE `foods` (\n" +
                "  `food_id` INTEGER PRIMARY KEY AUTOINCREMENT,\n" +
                "  `nutrient` TEXT,\n" +
                "  `amount` TEXT,\n" +
                "  `solvent` TEXT \n" +
                ")"

        val query2 = "CREATE TABLE `action_status` (\n" +
                "  `action_status_id` INTEGER PRIMARY KEY AUTOINCREMENT,\n" +
                "  `ac_id` TEXT,\n" +
                "  `ac_status_date` TEXT\n" +
                ")"

        val query3 = "CREATE TABLE `actions` (\n" +
                "  `ac_id` INTEGER PRIMARY KEY AUTOINCREMENT,\n" +
                "  `ac_plant_id` INTEGER,\n" +
                "  `ac_type` TEXT,\n" +
                "  `ac_date` TEXT,\n" +
                "  `ac_desc` TEXT,\n" +
                "  `ac_type_method` TEXT,\n" +
                "  `ac_type_water_quantity` TEXT,\n" +
                "  `ac_type_water_amount` TEXT,\n" +
                "  `ac_type_nutrients_list` TEXT,\n" +
                "  `ac_repeat` INTEGER,\n" +
                "  `ac_repeat_type` INTEGER,\n" +
                "  `ac_repeat_till` TEXT,\n" +
                "  `ac_repeat_days` INTEGER,\n" +
                "  `ac_repeat_status` TEXT\n" +
                ")"

        val query4 = "CREATE TABLE `tree_log` (\n" +
                "  `tl_id` INTEGER PRIMARY KEY AUTOINCREMENT,\n" +
                "  `plant_id` INTEGER,\n" +
                "  `log_date` TEXT,\n" +
                "  `log_desc` TEXT\n" +
                ")"

        val query5 = "CREATE TABLE `environment_log` (\n" +
                "  `ev_id` INTEGER PRIMARY KEY AUTOINCREMENT,\n" +
                "  `env_name` TEXT,\n" +
                "  `env_log_date` TEXT,\n" +
                "  `env_log_desc` TEXT\n" +
                ")"

        val query6 = "CREATE TABLE `videos` (\n" +
                "  `video_id` INTEGER PRIMARY KEY AUTOINCREMENT,\n" +
                "  `video_title` varchar(255) NOT NULL,\n" +
                "  `video_desc` TEXT,\n" +
                "  `video_url` TEXT,\n" +
                "  `video_thumbnail` TEXT\n" +
                ")"

        val query7 = "CREATE TABLE `other_resource` (\n" +
                "  `res_id` INTEGER PRIMARY KEY AUTOINCREMENT,\n" +
                "  `res_title` TEXT,\n" +
                "  `res_url` TEXT,\n" +
                "  `res_desc` TEXT\n" +
                ")"

        mDataBase.execSQL(query0)
        mDataBase.execSQL(query1)
        mDataBase.execSQL(query2)
        mDataBase.execSQL(query3)
        mDataBase.execSQL(query4)
        mDataBase.execSQL(query5)
        mDataBase.execSQL(query6)
        mDataBase.execSQL(query7)
        mDataBase.close()
    }

    // - - Add Plant in 'plant_list' Table
    fun addPlant(
        plantId: String,
        plantType: String,
    ) {
        val mDataBase = this.writableDatabase
        val values = ContentValues()
        values.put("plant_id", plantId)
        values.put("plant_type", plantType)
        mDataBase.insert("plant_list", null, values)
        mDataBase.close()
    }

    // - - Add Food in 'foods' Table
    fun addFood(
        nutrient: String,
        amount: String,
        solvent: String
    ) {
        val mDataBase = this.writableDatabase
        val values = ContentValues()
        values.put("nutrient", nutrient)
        values.put("amount", amount)
        values.put("solvent", solvent)
        mDataBase.insert("foods", null, values)
        mDataBase.close()
    }

    // - - Add Action_Status in 'action_status' Table
    fun addActionStatus(
        ac_id: String,
        ac_status_date: String
    ) {
        val mDataBase = this.writableDatabase
        val values = ContentValues()
        values.put("ac_id", ac_id)
        values.put("ac_status_date", ac_status_date)
        mDataBase.insert("action_status", null, values)
        mDataBase.close()
    }

    // - - Delete Action_Status in 'action_status' Table
    fun deleteActionStatus(
        ac_id: String,
        ac_status_date: String
    ) {
        val mDataBase = this.writableDatabase
        val query = "DELETE FROM action_status WHERE ac_id='$ac_id' AND ac_status_date='$ac_status_date'"
        mDataBase.execSQL(query)
        mDataBase.close()
    }

    // - - Delete Plant from 'plant_list' Table
    fun deletePlantFromPlantList(mItemId: String, mPlantType: String) {
        val mDataBase = this.writableDatabase
        val query = "DELETE FROM plant_list WHERE item_id='$mItemId' AND plant_type='$mPlantType'"
        mDataBase.execSQL(query)
        mDataBase.close()
    }

    // - - Add Growing Plant in 'plant_list' Table
    fun addGrowingPlant(
        plantId: String,
        plantType: String,
        plantStage: String,
        plantEnv: String,
        creationDate: String
    ) {
        val mDataBase = this.writableDatabase
        val values = ContentValues()
        values.put("plant_id", plantId)
        values.put("plant_type", plantType)
        values.put("plant_init_stage", plantStage)
        values.put("plant_stage",plantStage)
        values.put("plant_env",plantEnv)
        values.put("creation_date",creationDate)
        mDataBase.insert("plant_list", null, values)
        mDataBase.close()
    }

    fun updateGrowingPlant(growingPlantId: String,
                           growingPlantAltName: String,
                           growingPlantStage: String,
                           growingPlantEnv: String){
        val mDataBase = this.writableDatabase
        mDataBase.execSQL("UPDATE plant_list SET plant_alt_name = '$growingPlantAltName', plant_stage = '$growingPlantStage', plant_env = '$growingPlantEnv' WHERE item_id = '$growingPlantId' ")
        mDataBase.close()
    }

    fun updateHarvestingPlant(growingPlantId: String, plant_type: String){
        val mDataBase = this.writableDatabase
        mDataBase.execSQL("UPDATE plant_list SET plant_type = '$plant_type' WHERE item_id = '$growingPlantId' ")
        mDataBase.close()
    }

    fun updateGrowingPlantState(growingPlantId: String, stage: String){
        val mDataBase = this.writableDatabase
        mDataBase.execSQL("UPDATE plant_list SET plant_stage = '$stage' WHERE item_id = '$growingPlantId' ")
        mDataBase.close()
    }

    // - - Add Tree Log in 'tree_log' Table
    fun addTreeLog(
        growingPlantId: String,
        logDate: String,
        logDesc: String,
    ) {
        val mDataBase = this.writableDatabase
        val values = ContentValues()
        values.put("plant_id", growingPlantId)
        values.put("log_date", logDate)
        values.put("log_desc",logDesc)
        mDataBase.insert("tree_log", null, values)
        mDataBase.close()
    }

    // - - Add Environment Log in 'environment_log' Table
    fun addEnvironmentLog(
        envName: String,
        envDate: String,
        envDesc: String,
    ) {
        val mDataBase = this.writableDatabase
        val values = ContentValues()
        values.put("env_name", envName)
        values.put("env_log_date", envDate)
        values.put("env_log_desc",envDesc)
        mDataBase.insert("environment_log", null, values)
        mDataBase.close()
    }

    // - - Delete Tree Log from 'tree_log' Table
    fun deleteTreeLog(mItemId: String) {
        val mDataBase = this.writableDatabase
        val query = "DELETE FROM tree_log WHERE tl_id ='$mItemId'"
        mDataBase.execSQL(query)
        mDataBase.close()
    }

    // - - Delete Environment Log from 'environment_log' Table
    fun deleteEnvLog(mItemId: String) {
        val mDataBase = this.writableDatabase
        val query = "DELETE FROM environment_log WHERE ev_id ='$mItemId'"
        mDataBase.execSQL(query)
        mDataBase.close()
    }

    // - - Delete Action from 'actions' Table
    fun deleteAction(ac_plant_id: String) {
        val mDataBase = this.writableDatabase
        val query = "DELETE FROM actions WHERE ac_plant_id ='$ac_plant_id'"
        mDataBase.execSQL(query)
        mDataBase.close()
    }

    fun updateTreeLog(tl_id: String, plant_id: String, log_date: String, log_desc: String){
        val mDataBase = this.writableDatabase
        mDataBase.execSQL("UPDATE tree_log SET plant_id = '$plant_id', log_date = '$log_date', log_desc = '$log_desc' WHERE tl_id = '$tl_id' ")
        mDataBase.close()
    }

    fun updateEnvLog(envId: String, envName: String, envDate: String, envDesc: String){
        val mDataBase = this.writableDatabase
        mDataBase.execSQL("UPDATE environment_log SET env_name = '$envName', env_log_date = '$envDate', env_log_desc = '$envDesc' WHERE ev_id = '$envId' ")
        mDataBase.close()
    }

    // - - Add Action in 'actions' Table
    fun addAction(
        ac_plant_id: String,
        ac_type: String,
        ac_date: String,
        ac_desc: String,
        ac_type_method: String,
        ac_type_water_quantity: String,
        ac_type_water_amount:String,
        ac_type_nutrients_list: String,
        ac_repeat: String,
        ac_repeat_type: String,
        ac_repeat_till: String,
        ac_repeat_days: String,
        ac_repeat_status:String
    ) {
        val mDataBase = this.writableDatabase
        val values = ContentValues()
        values.put("ac_plant_id", ac_plant_id)
        values.put("ac_type", ac_type)
        values.put("ac_date", ac_date)
        values.put("ac_desc", ac_desc)
        values.put("ac_type_method", ac_type_method)
        values.put("ac_type_water_quantity", ac_type_water_quantity)
        values.put("ac_type_water_amount", ac_type_water_amount)
        values.put("ac_type_nutrients_list", ac_type_nutrients_list)
        values.put("ac_repeat", ac_repeat)
        values.put("ac_repeat_type", ac_repeat_type)
        values.put("ac_repeat_till", ac_repeat_till)
        values.put("ac_repeat_days", ac_repeat_days)
        values.put("ac_repeat_status", ac_repeat_status)
        mDataBase.insert("actions", null, values)
        mDataBase.close()
    }

    fun updateAction(ac_id: String, ac_plant_id: String, ac_type:String, ac_date: String, ac_desc: String,ac_type_method: String, ac_type_water_quantity: String, ac_type_water_amount: String, ac_type_nutrients_list: String,
                     ac_repeat: String, ac_repeat_type: String, ac_repeat_till: String, ac_repeat_days: String,ac_repeat_status: String){
        val mDataBase = this.writableDatabase
        mDataBase.execSQL("UPDATE actions SET ac_plant_id = '$ac_plant_id',ac_type='$ac_type', ac_date = '$ac_date', ac_desc = '$ac_desc',ac_type_method = '$ac_type_method', ac_type_water_quantity = '$ac_type_water_quantity', ac_type_water_amount = '$ac_type_water_amount', ac_type_nutrients_list = '$ac_type_nutrients_list'," +
                "ac_repeat = '$ac_repeat', ac_repeat_type = '$ac_repeat_type', ac_repeat_till = '$ac_repeat_till',ac_repeat_days = '$ac_repeat_days',ac_repeat_status = '$ac_repeat_status' WHERE ac_id = '$ac_id' ")
        mDataBase.close()
    }
}

