package com.stronglifts.Interface

import com.stronglifts.model.TreeLogModel

interface DeleteTreeLogClickListenerInterface {
    fun mClickListenerInterface(mModel: TreeLogModel)
}