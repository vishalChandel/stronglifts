package com.stronglifts.Interface

import com.stronglifts.model.EnvLogModel

interface EditEnvLogClickListenerInterface {
    fun mClickListenerInterface(mModel: EnvLogModel)
}