package com.stronglifts.Interface

import com.stronglifts.model.GrowingPlantsModel

interface ClickListenerInterfacewithGrowingPlantsData {
    fun mClickListenerInterface(mPos: Int,mTag:String,mGrowingPlantsList: ArrayList<GrowingPlantsModel>?)
}