package com.stronglifts.Interface

import com.stronglifts.model.EnvLogModel

interface DeleteEnvLogClickListenerInterface {
    fun mClickListenerInterface(mModel: EnvLogModel)
}