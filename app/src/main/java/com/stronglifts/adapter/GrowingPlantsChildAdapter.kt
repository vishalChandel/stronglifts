package com.stronglifts.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.goserve.utils.PLANT_IMAGES
import com.stronglifts.Interface.ClickListenerInterface
import com.stronglifts.Interface.ClickListenerInterfacewithGrowingPlantsData
import com.stronglifts.R
import com.stronglifts.model.GrowingPlantsModel
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

class GrowingPlantsChildAdapter(
    var mActivity: Activity?,
    var mInterface: ClickListenerInterface,
    var mInterfaceWithData: ClickListenerInterfacewithGrowingPlantsData,
    var mUpdatedGrowingPlantList: ArrayList<GrowingPlantsModel>?,
) :
    RecyclerView.Adapter<GrowingPlantsChildAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View = LayoutInflater.from(mActivity).inflate(R.layout.item_growing_plant_child, parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.txtPlantNameTV.text = mUpdatedGrowingPlantList!![position].name
        if(mUpdatedGrowingPlantList!![position].plant_alt_name.isNullOrEmpty()){
            holder.txtPlantAltNameTV.text=""
        }else{
            holder.txtPlantAltNameTV.text=" ("+ mUpdatedGrowingPlantList!![position].plant_alt_name + ")"
        }

        if (mUpdatedGrowingPlantList!![position].plant_env == "Outdoor") {
            holder.imgEnvIV.setImageResource(R.drawable.ic_outdoor)
        } else if (mUpdatedGrowingPlantList!![position].plant_env == "Indoor") {
            holder.imgEnvIV.setImageResource(R.drawable.ic_indoor)
        }

        for (i in PLANT_IMAGES.indices) {
            val mStaticImagesArray =
                mActivity!!.resources.getResourceEntryName(PLANT_IMAGES[i]) + ".png"
            if (mUpdatedGrowingPlantList!![position].thumbnailImage == mStaticImagesArray) {
                holder.imgPlantIV.setImageResource(PLANT_IMAGES[i])
                break
            }
        }

        val mCurrentDate = SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()).format(Date())
        val sdf = SimpleDateFormat("dd/MM/yyyy")
        val dateString=convertTimestampInDate(mUpdatedGrowingPlantList!![position].creation_date.toString())
        val date1 = sdf.parse(dateString)
        val date2 = sdf.parse(mCurrentDate)
        val diff: Long = date2.time - date1.time
        val seconds = diff / 1000
        val minutes = seconds / 60
        val hours = minutes / 60
        var days = hours / 24
        days += 1
        holder.txtDaysTV.text = "Day $days"


        holder.itemView.setOnClickListener {
            mInterfaceWithData.mClickListenerInterface(position, "GrowingPlantsDetailFragment", mUpdatedGrowingPlantList)
        }

        holder.imgEditIV.setOnClickListener {
            mInterfaceWithData.mClickListenerInterface(position, "EditGrowingPlantFragment", mUpdatedGrowingPlantList)
        }
    }

    override fun getItemCount(): Int {
        return if (mUpdatedGrowingPlantList.isNullOrEmpty()) {
            0
        } else {
            mUpdatedGrowingPlantList!!.size
        }
    }

    class MyViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var imgEditIV: ImageView = itemView.findViewById(R.id.imgEditIV)
        var txtPlantNameTV: TextView = itemView.findViewById(R.id.txtPlantNameTV)
        var txtPlantAltNameTV: TextView = itemView.findViewById(R.id.txtPlantAltNameTV)
        var imgPlantIV: ImageView = itemView.findViewById(R.id.imgPlantIV)
        var imgEnvIV: ImageView = itemView.findViewById(R.id.imgEnvIV)
        var txtDaysTV: TextView = itemView.findViewById(R.id.txtDaysTV)
    }

     private fun convertTimestampInDate(timestamp: String): String {
         val formatter: DateFormat = SimpleDateFormat("dd/MM/yyyy")
         val date = Date(timestamp.toLong())
         return formatter.format(date)
     }
}