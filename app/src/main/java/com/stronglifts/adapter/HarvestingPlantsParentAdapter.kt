package com.stronglifts.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.stronglifts.R
import com.stronglifts.model.ActionModel
import com.stronglifts.model.EnvLogModel
import com.stronglifts.model.TreeLogModel
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class HarvestingPlantsParentAdapter(
    var mActivity: Activity?,
    var mHarvestingPlantDates: ArrayList<String?>,
    var mHarvestingPlantUpdatedActionList: ArrayList<ActionModel>,
    var mHarvestingPlantTreeLogList: ArrayList<TreeLogModel>,
    var mHarvestingPlantEnvLogList: ArrayList<EnvLogModel>,
    var creationDate: String?,
) : RecyclerView.Adapter<HarvestingPlantsParentAdapter.MyViewHolder>() {

    // Initialize Objects
    private var mHarvestingPlantsChildActionAdapter: HarvestingPlantsChildActionAdapter? = null
    private var mHarvestingPlantsChildTreeLogAdapter: HarvestingPlantsChildTreeLogAdapter? = null
    private var mHarvestingPlantsChildEnvLogAdapter: HarvestingPlantsChildEnvLogAdapter? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View =
            LayoutInflater.from(mActivity).inflate(R.layout.item_harvest_detail, parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        val format1 = SimpleDateFormat("dd/MM/yyyy")
        val format2 = SimpleDateFormat("MMM dd, yyyy")
        val harvestingPlantDateString=convertTimestampInDate(mHarvestingPlantDates[position].toString())
        val date = format1.parse(harvestingPlantDateString)
        holder.dateTV.text = format2.format(date)
        val creationDateString=convertTimestampInDate(creationDate.toString())
        val sdf = SimpleDateFormat("dd/MM/yyyy")
        val date1 = sdf.parse(creationDateString)
        val date2 = sdf.parse(harvestingPlantDateString)
        val diff: Long = date2.time - date1.time
        val seconds = diff / 1000
        val minutes = seconds / 60
        val hours = minutes / 60
        var days = hours / 24
        days += 1
            if(days.toString()=="1"){
                holder.daysTV.text = "$days day"
            }else {
                holder.daysTV.text = "$days days"
            }

        val mUpdatedActionList = ArrayList<ActionModel>()
        for (i in 0 until mHarvestingPlantUpdatedActionList!!.size) {
            if (mHarvestingPlantUpdatedActionList!![i].ac_date == mHarvestingPlantDates[position]) {
                mUpdatedActionList.add(mHarvestingPlantUpdatedActionList!![i])
            }
            mUpdatedActionList.reverse()
            setActionChildAdapter(holder, mUpdatedActionList)
        }

        val mUpdatedTreeLogList = ArrayList<TreeLogModel>()
        for (i in 0 until mHarvestingPlantTreeLogList!!.size) {
            if (mHarvestingPlantTreeLogList!![i].log_date == mHarvestingPlantDates[position]) {
                mUpdatedTreeLogList.add(mHarvestingPlantTreeLogList!![i])
            }
            setTreeLogChildAdapter(holder, mUpdatedTreeLogList)
        }

        val mUpdatedEnvLogList = ArrayList<EnvLogModel>()
        for (i in 0 until mHarvestingPlantEnvLogList!!.size) {
            if (mHarvestingPlantEnvLogList!![i].env_log_date == mHarvestingPlantDates[position]) {
                mUpdatedEnvLogList.add(mHarvestingPlantEnvLogList!![i])
            }
            setEnvLogChildAdapter(holder, mUpdatedEnvLogList)
        }
    }

    override fun getItemCount(): Int {
        return if (mHarvestingPlantDates.isNullOrEmpty()) {
            0
        } else {
            mHarvestingPlantDates.size
        }
    }

    class MyViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var actionRV: RecyclerView = itemView.findViewById(R.id.actionRV)
        var treeLogRV: RecyclerView = itemView.findViewById(R.id.treeLogRV)
        var envLogRV: RecyclerView = itemView.findViewById(R.id.envLogRV)
        var dateTV: TextView = itemView.findViewById(R.id.dateTV)
        var daysTV: TextView = itemView.findViewById(R.id.daysTV)
    }


    private fun setActionChildAdapter(
        holder: MyViewHolder,
        mUpdatedActionList: ArrayList<ActionModel>
    ) {
        mHarvestingPlantsChildActionAdapter =
            HarvestingPlantsChildActionAdapter(mActivity as FragmentActivity?, mUpdatedActionList)
        holder.actionRV.adapter = mHarvestingPlantsChildActionAdapter
    }

    private fun setTreeLogChildAdapter(
        holder: MyViewHolder,
        mUpdatedActionList: ArrayList<TreeLogModel>
    ) {
        mHarvestingPlantsChildTreeLogAdapter =
            HarvestingPlantsChildTreeLogAdapter(mActivity as FragmentActivity?, mUpdatedActionList)
        holder.treeLogRV.adapter = mHarvestingPlantsChildTreeLogAdapter
    }

    private fun setEnvLogChildAdapter(
        holder: MyViewHolder,
        mUpdatedActionList: ArrayList<EnvLogModel>
    ) {
        mHarvestingPlantsChildEnvLogAdapter =
            HarvestingPlantsChildEnvLogAdapter(mActivity as FragmentActivity?, mUpdatedActionList)
        holder.envLogRV.adapter = mHarvestingPlantsChildEnvLogAdapter
    }

    private fun convertTimestampInDate(timestamp: String): String {
        val formatter: DateFormat = SimpleDateFormat("dd/MM/yyyy")
        val date = Date(timestamp.toLong())
        return formatter.format(date)
    }
}