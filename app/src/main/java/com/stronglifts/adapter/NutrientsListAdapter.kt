package com.stronglifts.adapter

import android.app.Activity
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.SeekBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.stronglifts.Interface.ClickListenerInterface
import com.stronglifts.R
import com.stronglifts.model.FoodModel

class NutrientsListAdapter(
    var mActivity: Activity?,
    var mNutrientList: ArrayList<FoodModel>?,
) :
    RecyclerView.Adapter<NutrientsListAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View = LayoutInflater.from(mActivity).inflate(R.layout.item_nutrients, parent,false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.txtNutrientTV.text=mNutrientList!![position]?.nutrient
        holder.txtFoodAmountTV.text= mNutrientList!![position]?.amount
        holder.txtSolventTV.text=mNutrientList!![position]?.solvent
        holder.cancelIV.setOnClickListener {
            mNutrientList!!.removeAt(position)
            notifyDataSetChanged()
        }
    }

    override fun getItemCount(): Int {
        return if (mNutrientList.isNullOrEmpty()) {
            0
        } else {
            mNutrientList!!.size
        }
    }

    class MyViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var txtNutrientTV: TextView = itemView.findViewById(R.id.txtNutrientTV)
        var txtFoodAmountTV: TextView = itemView.findViewById(R.id.txtTotalAmountTV)
        var txtSolventTV: TextView = itemView.findViewById(R.id.txtSolventTV)
        var cancelIV: ImageView = itemView.findViewById(R.id.cancelIV)
    }
}