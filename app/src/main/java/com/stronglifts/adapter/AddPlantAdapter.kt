package com.stronglifts.adapter

import android.app.Activity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.goserve.utils.PLANT_IMAGES
import com.stronglifts.Interface.ClickListenerInterface
import com.stronglifts.Interface.ClickListenerInterfacewithData
import com.stronglifts.R
import com.stronglifts.model.PlantsModel
import com.stronglifts.view.fragment.ChoosePlantStageFragment


class AddPlantAdapter(
    var mActivity: Activity?,
    var mFragmentTag: String,
    var mPlantsList: ArrayList<PlantsModel>?,
    var mInterface: ClickListenerInterfacewithData,
    var mHomeFragment2Interface: ClickListenerInterface
) : RecyclerView.Adapter<AddPlantAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View = LayoutInflater.from(mActivity).inflate(R.layout.item_add_plant, parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        if (mFragmentTag == "HomeFragment2") {
            holder.imgForwardIV.setImageResource(R.drawable.ic_add)
            holder.itemView.setOnClickListener {
                mHomeFragment2Interface.mClickListenerInterface(mPlantsList!![position]?.id.toInt(), "HomeFragment2")
            }
        } else {
            holder.itemView.setOnClickListener {
                mInterface.mClickListenerInterface(position, "AddPlant", mPlantsList)
            }
        }

        holder.txtPlantTV.text = mPlantsList!![position]!!.name

        for (i in 0 until PLANT_IMAGES!!.size) {
            val mStaticImagesArray =
                mActivity!!.resources.getResourceEntryName(PLANT_IMAGES[i]) + ".png"
            if (mPlantsList!![position]!!.thumbnailImage == mStaticImagesArray) {
                holder.imgPlantIV.setImageResource(PLANT_IMAGES[i])
                break
            }
        }
    }

    override fun getItemCount(): Int {
        return mPlantsList!!.size
    }

    class MyViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var imgPlantIV: ImageView = itemView.findViewById(R.id.imgPlantIV)
        var imgForwardIV: ImageView = itemView.findViewById(R.id.imgForwardIV)
        var txtPlantTV: TextView = itemView.findViewById(R.id.txtPlantTV)
    }

    fun updateList(list: ArrayList<PlantsModel>?) {
        mPlantsList = list
        notifyDataSetChanged()
    }


}