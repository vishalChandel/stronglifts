package com.stronglifts.adapter

import android.app.Activity
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.stronglifts.Interface.FilterClickListenerInterface
import com.stronglifts.R


class FilterAdapter(
    val mActivity: Activity?,
    var mPlantsCategoryList: ArrayList<String>?,
    var mFilterClickListenerInterface: FilterClickListenerInterface,
    var mTag: String,
) :
    RecyclerView.Adapter<FilterAdapter.ViewHolder>() {

    private var selectedItem = 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(mActivity)
        val view: View = layoutInflater.inflate(R.layout.item_filter, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.txtTitleTV.text = mPlantsCategoryList!![position]
        holder.txtTitleTV.setBackgroundResource(R.drawable.bg_filter_unsel)
        holder.txtTitleTV.setTextColor(Color.parseColor("#464543"))
        if (selectedItem == position) {
            holder.txtTitleTV.setBackgroundResource(R.drawable.bg_continue)
            holder.txtTitleTV.setTextColor(Color.parseColor("#ffffff"))
        }

        holder.itemView.setOnClickListener {
            val previousItem = selectedItem
            selectedItem = position
            notifyItemChanged(previousItem)
            notifyItemChanged(position)
            mFilterClickListenerInterface.mClickListenerInterface(mPlantsCategoryList!![position], mTag)
        }
    }

    override fun getItemCount(): Int {
        return if (mPlantsCategoryList.isNullOrEmpty()) {
            0
        } else {
            mPlantsCategoryList!!.size
        }
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var txtTitleTV: TextView = itemView.findViewById<View>(R.id.txtTitleTV) as TextView

    }

}
