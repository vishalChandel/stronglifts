package com.stronglifts.adapter

import android.app.Activity
import android.database.sqlite.SQLiteDatabase
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.goserve.utils.PLANT_IMAGES
import com.stronglifts.Interface.DeleteTreeLogClickListenerInterface
import com.stronglifts.Interface.EditTreeLogClickListenerInterface
import com.stronglifts.database.DBQuery
import com.stronglifts.databinding.ItemHarvestTreeLogBinding
import com.stronglifts.databinding.ItemTreeLogBinding
import com.stronglifts.model.GrowingPlantsModel
import com.stronglifts.model.TreeLogModel

class HarvestingPlantsChildTreeLogAdapter(
    var mActivity: Activity?,
    var mTreeLogArraylist: ArrayList<TreeLogModel>?
) : RecyclerView.Adapter<HarvestingPlantsChildTreeLogAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val binding = ItemHarvestTreeLogBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return MyViewHolder(binding)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        with(holder) {

            val mDB = DBQuery(mActivity)
            mDB.open()
            val mGrowingPlantListName = mDB.getGrowingPlantNameList(mTreeLogArraylist!![position].plant_id)
            mDB.close()

            if (mGrowingPlantListName!![0].plant_alt_name.isNullOrEmpty()) {
                binding.txtPlantNameTV.text = mGrowingPlantListName!![0].name
            } else {
                binding.txtPlantNameTV.text = mGrowingPlantListName!![0].name + "(" + mGrowingPlantListName!![0].plant_alt_name + ")"
            }
            binding.descriptionTV.text = mTreeLogArraylist!![position].log_desc


        }
    }

    override fun getItemCount(): Int {
        return if (mTreeLogArraylist.isNullOrEmpty()) {
            0
        } else {
            mTreeLogArraylist!!.size
        }
    }

    inner class MyViewHolder(val binding: ItemHarvestTreeLogBinding) :
        RecyclerView.ViewHolder(binding.root)


}