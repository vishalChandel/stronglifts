package com.stronglifts.adapter

import android.app.Activity
import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.stronglifts.R
import com.stronglifts.model.ActionTypeModel

class ActionTypeSpinnerAdapter(
    val context: Activity?,
    private var dataSource: ArrayList<ActionTypeModel>
) : BaseAdapter() {

    private val inflater: LayoutInflater = context!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val view: View
        val vh: ItemHolder
        if (convertView == null) {
            view = inflater.inflate(R.layout.layout_spinner, parent, false)
            vh = ItemHolder(view)
            view?.tag = vh
        } else {
            view = convertView
            vh = view.tag as ItemHolder
        }
        if(position==0){
            vh.label.setTextColor(Color.GRAY)
            vh.img.visibility=View.GONE
        }else{
            vh.label.setTextColor(Color.BLACK)
            vh.img.visibility=View.VISIBLE
        }
        vh.label.text = dataSource[position].actionName
        Glide.with(context!!).load(dataSource[position].actionImage).into(vh.img)
        return view
    }

    override fun getItem(position: Int): Any? {
        return dataSource[position]
    }

    override fun getCount(): Int {
        return dataSource.size
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    private class ItemHolder(row: View?) {
        val label: TextView = row?.findViewById(R.id.itemTV) as TextView
        val img: ImageView = row?.findViewById(R.id.itemIV) as ImageView
    }
}