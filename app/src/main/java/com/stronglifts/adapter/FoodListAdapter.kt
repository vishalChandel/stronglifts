package com.stronglifts.adapter

import android.app.Activity
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.SeekBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.goserve.utils.AppPreference
import com.goserve.utils.FOOD_ID
import com.goserve.utils.IS_LOGIN
import com.stronglifts.Interface.ClickListenerInterface
import com.stronglifts.Interface.ClickListenerInterfaceFoodListwithData
import com.stronglifts.R
import com.stronglifts.model.FoodModel

class FoodListAdapter(
    var mActivity: Activity?,
    var mFoodList: ArrayList<FoodModel>?,
    var mClickListenerInterfaceFoodListwithData: ClickListenerInterfaceFoodListwithData,
) :
    RecyclerView.Adapter<FoodListAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View = LayoutInflater.from(mActivity).inflate(R.layout.item_food_list, parent,false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.txtNutrientTV.text=mFoodList!![position]?.nutrient
        holder.txtFoodAmountTV.text= mFoodList!![position]?.amount
        holder.txtSolventTV.text=mFoodList!![position]?.solvent
        holder.itemView.setOnClickListener {
            mClickListenerInterfaceFoodListwithData.mClickListenerInterface(mFoodList!![position])
        }
    }

    override fun getItemCount(): Int {
        return if (mFoodList.isNullOrEmpty()) {
            0
        } else {
            mFoodList!!.size
        }
    }

    class MyViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var txtNutrientTV: TextView = itemView.findViewById(R.id.txtNutrientTV)
        var txtFoodAmountTV: TextView = itemView.findViewById(R.id.txtTotalAmountTV)
        var txtSolventTV: TextView = itemView.findViewById(R.id.txtSolventTV)
    }
}