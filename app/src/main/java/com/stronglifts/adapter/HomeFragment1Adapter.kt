package com.stronglifts.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.goserve.utils.PLANT_IMAGES
import com.stronglifts.Interface.ClickListenerInterfacewithData
import com.stronglifts.Interface.LongClickListenerInterface
import com.stronglifts.R
import com.stronglifts.model.PlantsModel

class HomeFragment1Adapter(
    var mActivity: Activity?,
    var mPlantsList: ArrayList<PlantsModel>?,
    var mInterface: ClickListenerInterfacewithData?,
    var mLongClickInterface: LongClickListenerInterface,
    var isLongPressed: Boolean
) : RecyclerView.Adapter<HomeFragment1Adapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View =
            LayoutInflater.from(mActivity).inflate(R.layout.item_home_fragment1, parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        if (isLongPressed) {
            holder.imgCrossIV.visibility = View.VISIBLE
            holder.imgCrossIV.setOnClickListener {
                mLongClickInterface.mClickListenerInterface(
                    mPlantsList!![position].item_id, "HomeFragment1"
                )
            }
        }else{
            holder.itemView.setOnClickListener {
                mInterface!!.mClickListenerInterface(position, "HomeFragment1", mPlantsList)
            }
        }

        holder.txtPlantTV.text = mPlantsList!![position].name

        for (i in PLANT_IMAGES.indices) {
            val mStaticImagesArray =
                mActivity!!.resources.getResourceEntryName(PLANT_IMAGES[i]) + ".png"
            if (mPlantsList!![position].thumbnailImage == mStaticImagesArray) {
                holder.imgPlantIV.setImageResource(PLANT_IMAGES[i])
                break
            }
        }
    }

    override fun getItemCount(): Int {
        return if (mPlantsList.isNullOrEmpty()) {
            0
        } else {
            mPlantsList!!.size
        }
    }

    class MyViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var imgPlantIV: ImageView = itemView.findViewById(R.id.imgPlantIV)
        var imgCrossIV: ImageView = itemView.findViewById(R.id.imgCrossIV)
        var txtPlantTV: TextView = itemView.findViewById(R.id.txtPlantTV)
    }

    fun updateList(list: ArrayList<PlantsModel>?) {
        mPlantsList = list
        notifyDataSetChanged()
    }
}