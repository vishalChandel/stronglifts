package com.stronglifts.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import com.goserve.utils.PLANT_IMAGES
import com.stronglifts.Interface.ClickListenerInterfaceHarvestModel
import com.stronglifts.R
import com.stronglifts.Interface.ClickListenerInterfacewithData
import com.stronglifts.Interface.ClickListenerInterfacewithGrowingPlantsData
import com.stronglifts.Interface.DeleteClickListenerInterfaceHarvestModel
import com.stronglifts.database.DBHelper
import com.stronglifts.database.DBQuery
import com.stronglifts.model.GrowingPlantsModel
import com.stronglifts.model.PlantsModel


class GrowingHarvestAdapter(
    var mActivity: Activity?,
    var mGrowingHarvestList: ArrayList<GrowingPlantsModel>?,
    var mInterface: ClickListenerInterfaceHarvestModel,
    var mDeleteClickListenerInterfaceHarvestModel: DeleteClickListenerInterfaceHarvestModel
) :
    RecyclerView.Adapter<GrowingHarvestAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View = LayoutInflater.from(mActivity).inflate(R.layout.item_growing_harvest, parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.txtPlantNameTV.text = mGrowingHarvestList!![position].name
        if(mGrowingHarvestList!![position].plant_alt_name.isNullOrEmpty()){
            holder.txtPlantAltNameTV.text=""
        }else{
            holder.txtPlantAltNameTV.text=" ("+ mGrowingHarvestList!![position].plant_alt_name + ")"
        }


        for (i in PLANT_IMAGES.indices) {
            val mStaticImagesArray =
                mActivity!!.resources.getResourceEntryName(PLANT_IMAGES[i]) + ".png"
            if (mGrowingHarvestList!![position].thumbnailImage == mStaticImagesArray) {
                holder.imgPlantIV.setImageResource(PLANT_IMAGES[i])
                break
            }
        }

        holder.deleteIV.setOnClickListener {
            mDeleteClickListenerInterfaceHarvestModel.mClickListenerInterface(mGrowingHarvestList!![position])
        }

        holder.itemView.setOnClickListener {
            mInterface.mClickListenerInterface(mGrowingHarvestList!![position])
        }
    }

    override fun getItemCount(): Int {
        return if (mGrowingHarvestList.isNullOrEmpty()) {
            0
        } else {
            mGrowingHarvestList!!.size
        }
    }

    class MyViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var txtPlantNameTV: TextView = itemView.findViewById(R.id.txtPlantNameTV)
        var txtPlantAltNameTV: TextView = itemView.findViewById(R.id.txtPlantAltNameTV)
        var imgPlantIV: ImageView = itemView.findViewById(R.id.imgPlantIV)
        var deleteIV:ImageView= itemView.findViewById(R.id.deleteIV)
    }
}