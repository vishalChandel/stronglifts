package com.stronglifts.adapter

import android.app.Activity
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.stronglifts.Interface.ClickListenerInterface
import com.stronglifts.R

class OtherResourcesAdapter(
    var mActivity: Activity?,
) :
    RecyclerView.Adapter<OtherResourcesAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View = LayoutInflater.from(mActivity).inflate(R.layout.item_other_resources, parent,false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

    }

    override fun getItemCount(): Int {
        return 3
    }

    class MyViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
    }
}