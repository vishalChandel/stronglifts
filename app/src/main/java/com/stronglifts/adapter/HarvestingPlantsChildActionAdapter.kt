package com.stronglifts.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.goserve.utils.PLANT_IMAGES
import com.stronglifts.Interface.EditActionClickListenerInterface
import com.stronglifts.R
import com.stronglifts.database.DBQuery
import com.stronglifts.databinding.ItemActionBinding
import com.stronglifts.databinding.ItemHarvestActionBinding
import com.stronglifts.model.ActionModel
import com.stronglifts.model.FoodModel

class HarvestingPlantsChildActionAdapter(
    var mActivity: Activity?,
    var mActionArrayList: ArrayList<ActionModel>?
) : RecyclerView.Adapter<HarvestingPlantsChildActionAdapter.MyViewHolder>() {
    // Initialize Objects
    var mActionFoodListAdapter: ActionFoodListAdapter? = null
    var mActionFoodList = ArrayList<FoodModel>()
    private var mWaterQtyArray = arrayOf(
        "ml",
        "cc",
        "m3",
        "in3",
        "ft3",
        "gal",
        "mm3",
        "l",
        "cups",
        "ft. oz",
        "pint",
        "quart",
        "tbsp"
    )
    private var mNutrientArray = arrayOf("Liquid", "Spray", "Solid")
    private var mRepellentArray = arrayOf("Liquid", "Spray", "Solid")
    private var mTrainingArray =
        arrayOf("FIM", "LST(Low stress training)", "SCROG", "Super cropping", "Topping")
    private var mTrimArray =
        arrayOf("Topping", "FIM", "Defoliation", "Lollipopping", "Lower branches")
    private var mStateArray = arrayOf("Flowering", "Germination", "Seedling", "Vegetative")


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val binding = ItemHarvestActionBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return MyViewHolder(binding)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        with(holder) {

            binding.txtActionNameTV.text = mActionArrayList!![position].ac_type

            binding.repeatStatusTV.text = mActionArrayList!![position].ac_repeat_status

            binding.descriptionTV.text = mActionArrayList!![position].ac_desc

            when (mActionArrayList!![position].ac_type) {
                "Water" -> {
                    binding.applicationMethodTV.text =
                        mActionArrayList!![position].ac_type_water_amount + "(" + mWaterQtyArray[mActionArrayList!![position].ac_type_water_quantity.toInt()] + ")"
                    binding.actionImage.setImageResource(R.drawable.ic_water)
                    binding.methodIV.setImageResource(R.drawable.ic_water)
                }
                "Nutrients" -> {
                    binding.applicationMethodTV.text =
                        mNutrientArray[mActionArrayList!![position].ac_type_method.toInt()]
                    binding.actionImage.setImageResource(R.drawable.ic_nutrients)
                    binding.methodIV.setImageResource(R.drawable.ic_nutrients)
                    binding.nutrientListRV.visibility = View.VISIBLE
                    if (mActionArrayList!![position].ac_type_nutrients_list.isNotEmpty()) {
                        val mFoodIdList =
                            mActionArrayList!![position].ac_type_nutrients_list.split(",")
                        val db1 = DBQuery(mActivity)
                        db1.open()
                        if (!mActionFoodList.isNullOrEmpty()) {
                            mActionFoodList.clear()
                        }
                        for (i in 0 until mFoodIdList!!.size) {
                            val mList = db1.getFoodListByFoodId(mFoodIdList!![i])
                            mActionFoodList.add(0, mList!![0])
                        }
                        db1.close()
                        setChildAdapter(binding.nutrientListRV, mActionFoodList)
                    }
                }
                "Repellent" -> {
                    binding.applicationMethodTV.text =
                        mRepellentArray[mActionArrayList!![position].ac_type_method.toInt()]
                    binding.actionImage.setImageResource(R.drawable.ic_repellent)
                    binding.methodIV.setImageResource(R.drawable.ic_repellent)
                }
                "Transplant" -> {
                    binding.applicationMethodLL.visibility = View.GONE
                    binding.actionImage.setImageResource(R.drawable.ic_transplant)
                }
                "Trim" -> {
                    binding.applicationMethodTV.text =
                        mTrimArray[mActionArrayList!![position].ac_type_method.toInt()]
                    binding.actionImage.setImageResource(R.drawable.ic_trim)
                    binding.methodIV.setImageResource(R.drawable.ic_trim)
                }
                "Training" -> {
                    binding.applicationMethodTV.text =
                        mTrainingArray[mActionArrayList!![position].ac_type_method.toInt()]
                    binding.actionImage.setImageResource(R.drawable.ic_training)
                    binding.methodIV.setImageResource(R.drawable.ic_training)
                }
                "Change Environment" -> {
                    binding.applicationMethodLL.visibility = View.GONE
                    binding.actionImage.setImageResource(R.drawable.ic_change_env)
                }
                "Flush" -> {
                    binding.applicationMethodLL.visibility = View.GONE
                    binding.actionImage.setImageResource(R.drawable.ic_flush)
                }
                "Harvest" -> {
                    binding.applicationMethodLL.visibility = View.GONE
                    binding.actionImage.setImageResource(R.drawable.ic_harvest)
                }
                "Declare Death" -> {
                    binding.applicationMethodLL.visibility = View.GONE
                    binding.actionImage.setImageResource(R.drawable.ic_death)
                    binding.repeatLL.visibility=View.GONE
                }
                "Change State" -> {
                    binding.applicationMethodTV.text = mStateArray[mActionArrayList!![position].ac_type_method.toInt()]
                    binding.actionImage.setImageResource(R.drawable.ic_change_state)
                    when (mActionArrayList!![position].ac_type_method) {
                        "0" -> {
                            binding.methodIV.setImageResource(R.drawable.ic_flowering)
                        }
                        "1" -> {
                            binding.methodIV.setImageResource(R.drawable.ic_germination)
                        }
                        "2" -> {
                            binding.methodIV.setImageResource(R.drawable.ic_seedling)
                        }
                        "3" -> {
                            binding.methodIV.setImageResource(R.drawable.ic_vegetative)
                        }
                    }
                    binding.repeatLL.visibility=View.GONE
                }
                "Other" -> {
                    binding.applicationMethodLL.visibility = View.GONE
                    binding.actionImage.setImageResource(R.drawable.ic_other)
                }
            }
        }
    }

    override fun getItemCount(): Int {
        return if (mActionArrayList.isNullOrEmpty()) {
            0
        } else {
            mActionArrayList!!.size
        }
    }

    inner class MyViewHolder(val binding: ItemHarvestActionBinding) :
        RecyclerView.ViewHolder(binding.root)


    private fun setChildAdapter(nutrientListRV: RecyclerView, mArrayList: ArrayList<FoodModel>) {
        mActionFoodListAdapter = ActionFoodListAdapter(mActivity as FragmentActivity?, mArrayList)
        nutrientListRV.adapter = mActionFoodListAdapter
    }

}