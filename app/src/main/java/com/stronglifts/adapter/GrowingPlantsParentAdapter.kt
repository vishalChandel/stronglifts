package com.stronglifts.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.TextureView
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.stronglifts.Interface.ClickListenerInterface
import com.stronglifts.Interface.ClickListenerInterfacewithData
import com.stronglifts.Interface.ClickListenerInterfacewithGrowingPlantsData
import com.stronglifts.R
import com.stronglifts.model.GrowingPlantsModel
import com.stronglifts.model.PlantsModel

class GrowingPlantsParentAdapter(
    var mActivity: Activity?,
    var mGrowingPlantStages: ArrayList<String?>,
    var mGrowingPlantsList: ArrayList<GrowingPlantsModel>?,
    var mInterface: ClickListenerInterface,
    var mInterfaceWithData: ClickListenerInterfacewithGrowingPlantsData
) : RecyclerView.Adapter<GrowingPlantsParentAdapter.MyViewHolder>() {

    // Initialize Objects
    var mGrowingPlantsChildAdapter: GrowingPlantsChildAdapter? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View = LayoutInflater.from(mActivity)
            .inflate(R.layout.item_growing_plant_parent, parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        holder.txtGrowingPlantStageTV.text = mGrowingPlantStages[position]

        val mUpdatedGrowingPlantList = ArrayList<GrowingPlantsModel>()
        for (i in 0 until mGrowingPlantsList!!.size) {
            if (mGrowingPlantsList!![i].plant_stage == mGrowingPlantStages[position]) {
                mUpdatedGrowingPlantList.add(mGrowingPlantsList!![i])
            }
            setChildAdapter(holder, mInterface, mInterfaceWithData, mUpdatedGrowingPlantList)
        }
    }

    override fun getItemCount(): Int {
        return if (mGrowingPlantStages.isNullOrEmpty()) {
            0
        } else {
            mGrowingPlantStages.size
        }
    }

    class MyViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var mGrowingPlantsChildRV: RecyclerView = itemView.findViewById(R.id.growingPlantsChildRV)
        var txtGrowingPlantStageTV: TextView = itemView.findViewById(R.id.txtGrowingPlantStageTV)
    }


    private fun setChildAdapter(
        holder: MyViewHolder,
        mInterface: ClickListenerInterface,
        mInterfaceWithData: ClickListenerInterfacewithGrowingPlantsData,
        mUpdatedGrowingPlantList: ArrayList<GrowingPlantsModel>?
    ) {
        val layoutManager: RecyclerView.LayoutManager =
            LinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false)
        holder.mGrowingPlantsChildRV.layoutManager = layoutManager
        mGrowingPlantsChildAdapter = GrowingPlantsChildAdapter(
            mActivity as FragmentActivity?,
            mInterface,
            mInterfaceWithData,
            mUpdatedGrowingPlantList
        )
        holder.mGrowingPlantsChildRV.adapter = mGrowingPlantsChildAdapter
    }
}