package com.stronglifts.adapter

import android.app.Activity
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.stronglifts.Interface.ClickListenerInterface
import com.stronglifts.R
import com.stronglifts.model.PlantsModel

class HomeFragment4Adapter(
    var mActivity: Activity?,
    var mInterface: ClickListenerInterface?
) :
    RecyclerView.Adapter<HomeFragment4Adapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View = LayoutInflater.from(mActivity).inflate(R.layout.item_home_fragment4, parent,false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.itemView.setOnClickListener {
            var mPlantsList: ArrayList<PlantsModel>?=null
            mInterface!!.mClickListenerInterface(position,"HomeFragment4")
        }

    }

    override fun getItemCount(): Int {
        return 4
    }

    class MyViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
    }
}