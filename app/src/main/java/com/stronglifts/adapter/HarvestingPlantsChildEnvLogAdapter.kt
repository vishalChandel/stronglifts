package com.stronglifts.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.stronglifts.Interface.DeleteEnvLogClickListenerInterface
import com.stronglifts.Interface.EditEnvLogClickListenerInterface
import com.stronglifts.R
import com.stronglifts.databinding.ItemEnvLogBinding
import com.stronglifts.databinding.ItemHarvestEnvLogBinding
import com.stronglifts.model.EnvLogModel

class HarvestingPlantsChildEnvLogAdapter(
    var mActivity: Activity?,
    var mEnvLogArraylist: ArrayList<EnvLogModel>?
) : RecyclerView.Adapter<HarvestingPlantsChildEnvLogAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val binding = ItemHarvestEnvLogBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return MyViewHolder(binding)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        with(holder) {
            binding.txtPlantStageTV.text = mEnvLogArraylist!![position].env_name
            binding.descriptionTV.text = mEnvLogArraylist!![position].env_log_desc
        }
    }

    override fun getItemCount(): Int {
        return if (mEnvLogArraylist.isNullOrEmpty()) {
            0
        } else {
            mEnvLogArraylist!!.size
        }
    }

    inner class MyViewHolder(val binding: ItemHarvestEnvLogBinding) : RecyclerView.ViewHolder(binding.root)
}