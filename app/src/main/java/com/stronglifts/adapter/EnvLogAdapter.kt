package com.stronglifts.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.stronglifts.Interface.DeleteEnvLogClickListenerInterface
import com.stronglifts.Interface.EditEnvLogClickListenerInterface
import com.stronglifts.R
import com.stronglifts.databinding.ItemEnvLogBinding
import com.stronglifts.model.EnvLogModel

class EnvLogAdapter(
    var mActivity: Activity?,
    var mEnvLogArraylist: ArrayList<EnvLogModel>?,
    var mEditEnvLogClickListenerInterface: EditEnvLogClickListenerInterface,
    var mDeleteEnvLogClickListenerInterface: DeleteEnvLogClickListenerInterface
) : RecyclerView.Adapter<EnvLogAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val binding = ItemEnvLogBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return MyViewHolder(binding)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        with(holder) {
            binding.txtPlantStageTV.text = mEnvLogArraylist!![position].env_name

            binding.descriptionTV.text = mEnvLogArraylist!![position].env_log_desc

            binding.editTV.setOnClickListener {
                mEditEnvLogClickListenerInterface.mClickListenerInterface(mEnvLogArraylist!![position])
            }

            binding.removeTV.setOnClickListener {
                mDeleteEnvLogClickListenerInterface.mClickListenerInterface(mEnvLogArraylist!![position])
            }

            if(mEnvLogArraylist!![position].env_name=="Indoor") {
                binding.envIV.setImageResource(R.drawable.ic_indoor)
            }else if(mEnvLogArraylist!![position].env_name=="Outdoor"){
                binding.envIV.setImageResource(R.drawable.ic_outdoor)
            }
        }
    }

    override fun getItemCount(): Int {
        return if (mEnvLogArraylist.isNullOrEmpty()) {
            0
        } else {
            mEnvLogArraylist!!.size
        }
    }

    inner class MyViewHolder(val binding: ItemEnvLogBinding) : RecyclerView.ViewHolder(binding.root)
}