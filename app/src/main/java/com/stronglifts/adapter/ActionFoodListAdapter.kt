package com.stronglifts.adapter

import android.app.Activity
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.SeekBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.goserve.utils.AppPreference
import com.goserve.utils.FOOD_ID
import com.goserve.utils.IS_LOGIN
import com.stronglifts.Interface.ClickListenerInterface
import com.stronglifts.Interface.ClickListenerInterfaceFoodListwithData
import com.stronglifts.R
import com.stronglifts.model.FoodModel

class ActionFoodListAdapter(
    var mActivity: Activity?,
    var mActionFoodList: ArrayList<FoodModel>,
    ) :
    RecyclerView.Adapter<ActionFoodListAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View = LayoutInflater.from(mActivity).inflate(R.layout.item_nutrientlist_home, parent,false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.foodTV.text=mActionFoodList!![position]?.nutrient +" - "+ mActionFoodList!![position]?.amount +" x " + mActionFoodList!![position]?.solvent
    }

    override fun getItemCount(): Int {
        return if (mActionFoodList.isNullOrEmpty()) {
            0
        } else {
            mActionFoodList!!.size
        }
    }

    class MyViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var foodTV: TextView = itemView.findViewById(R.id.foodTV)
    }
}